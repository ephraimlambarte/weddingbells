var mysql = require('mysql');
var connection;
var sessionid;
var fs1 = require('fs');
const path = require('path');
//var remote = require('remote');
function connectMysql(){
   connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'ephraim',
      password : '11587496', // or the original password : 'apaswword'
      database : 'dbweddingbells'
  });
  connection.connect(function(err) {
    if(err){
        console.log(err.code);
        console.log(err.fatal);
    }
  });
}
function connectMysql2(callback){
   connection = mysql.createConnection({
      host     : 'User-PC',
      user     : 'ephraim',
      password : '11587496', // or the original password : 'apaswword'
      database : 'dbweddingbells',
      multipleStatements: true
  });
  //console.log(connection);
  connection.connect(function(err) {
    //console.log(connection);
    if(err){
        console.log(err);
        console.log(err.code);
        console.log(err.fatal);
    }else{
      //console.log(connection);
      callback(connection)
    }
  });
}
function closeConnection(){
  try {
    connection.end();
  } catch (e) {
    console.log(e.message);
  }
}
function testPrepare(){
  var sql ="SELECT * FROM ?? WHERE ?? = ?";
  var insert = ['tbluser', 'userID', '1'];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, rows, fields){
    if(err){
        console.log("An error ocurred performing the query.");
        console.log(err);
        return;
    }

    console.log("Query succesfully executed", rows);
  });
}
function setSessionUsername(username, callback){
  var sql  = "SELECT userID FROM tbluser WHERE username = ?";
  var insert = [username];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, rows, fields){
    if(err){
      return "false";
    }else{
      sessionStorage.setItem('id',rows[0]["userID"]);
      callback(rows[0]["userID"]);
    }
  });
}
function loader(element){
  $("#"+element).html("<div class='cssload-loader'>"+
	"<div class='cssload-inner cssload-one'></div>"+
	"<div class='cssload-inner cssload-two'></div>"+
	"<div class='cssload-inner cssload-three'></div>"+
  "</div>");
}
function tableLoader(element, colspan){
  $(element).html("<tr><td colspan = '"+colspan+"'><div class='cssload-loader'>"+
	"<div class='cssload-inner cssload-one'></div>"+
	"<div class='cssload-inner cssload-two'></div>"+
	"<div class='cssload-inner cssload-three'></div>"+
  "</div></td></tr>");
}
function getMaxRows(callback, table, condition){
  sql = "SELECT * FROM "+table+" "+condition;
  connection.query(sql,function(err, rows, fields){
    if(err){
      console.log(err);
    }else {
      callback(rows.length);
    }
  });
}
function getTransactionRows(callback){
  sql = "SELECT tblcustomer.firstname, tblcustomer.lastname, tbltransaction.transID, "+
					"tbltransaction.time_transacted, tbltransaction.date FROM tbltransaction INNER JOIN "+
					"tblcustomer ON tblcustomer.custID = tbltransaction.custID ORDER BY "+
					"tbltransaction.transID DESC";
  connection.query(sql,function(err, rows, fields){
    if(err){
      console.log(err);
    }else {
      callback(rows.length);
    }
  });
}
function getUserType(connection,userid, callback){
  var sql = "SELECT type FROM tbluser WHERE userID = ?";
  var insert = [userid];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, rows){
    if(err){
      console.log(err);
    }else{
      callback(rows[0].type);
    }
    //connection.end();
  });
}
function generateItemCode(id){
  var date = new Date();
  var mydate = date.getFullYear();
  if(id<10){
    return mydate +"-"+"000000"+id;
  }else if (id<100 && id>=10) {
    return mydate +"-"+"00000"+id;
  }else if (id<1000 && id>=100) {
    return mydate +"-"+"0000"+id;
  }else if (id<10000 && id>=1000) {
    return mydate +"-"+"000"+id;
  }else if (id<100000 && id>=10000) {
    return mydate +"-"+"00"+id;
  }else if (id<1000000 && id>=100000) {
    return mydate +"-"+"0"+id;
  }else if (id>=1000000) {
    return mydate +"-"+id;
  }
}
function getImageInventory(id, callback){
  //console.log("Hello");
  connectMysql2(function(connection){
    sql = "SELECT img FROM tblinventory WHERE itemcode = '"+id+"'";
    connection.query(sql, function(err, result){
      if(err){
        alert("An error occured loading the image.");
      }else{
        callback(result[0]);
      }
    });
  });
}
function getImageTheme(id, callback){
  connectMysql2(function(connection){
    sql = "SELECT img FROM tbltheme WHERE themeID = '"+id+"'";
    connection.query(sql, function(err, result){
      if(err){
        alert("An error occured loading the image.");
      }else{
        callback(result[0]);
      }
    });
  });
}
function getImageCake(id, callback){
  connectMysql2(function(connection){
    sql = "SELECT img FROM tblcake WHERE cakeID = '"+id+"'";
    connection.query(sql, function(err, result){
      if(err){
        alert("An error occured loading the image.");
      }else{
        callback(result[0]);
      }
    });
  });
}
function readWriteFile(req, imgno,callback) {

  var fs = require('fs');
    //console.log(req);
      var data =   Buffer.from(req.img);
      fs.writeFile("./public/temp/out"+imgno+".jpg", data, 'binary', function (err) {
          if (err) {
            console.log(err)
              console.log("There was an error writing the image")
          }
          else {
              console.log("The sheel file was written")
          }
          callback("./public/temp/out.jpg");
  });

}
function base64ToBlob(base64) {
  /*var contentType = 'image/jpeg';
  var byteCharacters = atob(base64);
  var byteNumbers = new Array(byteCharacters.length);
  for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  var byteArray = new Uint8Array(byteNumbers);
  var blob = new Blob([byteArray], {type: contentType});*/
  var buf = Buffer.from(base64, 'base64');
  return buf;
}
function deleteImages(callback){
  directory = "./public/temp/";
  fs1.readdir(directory, (err, files) => {
    if (err) throw error;
    for (const file of files) {
      fs1.unlink(path.join(directory, file), err => {
        if (err) throw error;
      });
    }
    callback("hello");
  });
  }
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
function getMonthName(monthnum){
  if(monthnum == 1){
    return "January";
  }else if (monthnum == 2) {
    return "February";
  }else if (monthnum == 3) {
    return "March";
  }else if (monthnum == 4) {
    return "April";
  }else if (monthnum == 5) {
    return "May";
  }else if (monthnum == 6) {
    return "June";
  }else if (monthnum == 7) {
    return "July";
  }else if (monthnum == 8) {
    return "August";
  }else if (monthnum == 9) {
    return "September";
  }else if (monthnum == 10) {
    return "October";
  }else if (monthnum == 11) {
    return "November";
  }else if (monthnum == 12) {
    return "December";
  }
}
function getTime(time, ampm){
  if(ampm=="PM"){
    var myval = parseFloat(time.split(":")[0])+parseFloat(12);
    return myval+":"+time.split(":")[1]+":00";
  }else{
    return time+":00";
  }
}
function get12Hour(time){
  if(time.split(":")[0]>12){
    return (time.split(":")[0]-12)+":"+time.split(":")[1]+"PM";
  }else{
      return (time.split(":")[0])+":"+time.split(":")[1]+"AM";
  }
}
function checkIfTransHasTheme(connection, transid, locationid,callback){
  var sql = "SELECT tbltheme.themeID FROM tbltrans_location INNER JOIN "+
  "tbltrans_theme ON tbltrans_location.trans_locationid = tbltrans_theme.locationID"+
  " INNER JOIN tbltheme ON tbltheme.themeID = tbltrans_theme.themeID INNER "+
  "JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID "+
  "WHERE tbltransaction.transID = '"+transid+"'  AND tbltrans_location.trans_locationid = '"+locationid+"'";
  //console.log(sql);
  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
    }else{
      if(rows.length== 0){
        callback(false, 0);
      }else{
        callback(true, rows[0].themeID);
      }
    }
  });
}
function getImageMaleTailor(id, callback){
  connectMysql2(function(connection){
    sql = "SELECT img FROM tbltrans_tailor_boy WHERE id = '"+id+"'";
    connection.query(sql, function(err, result){
      if(err){
        alert("An error occured loading the image.");
      }else{
        callback(result[0]);
      }
    });
  });
}
function getImageFemaleTailor(id, callback){
  connectMysql2(function(connection){
    sql = "SELECT img FROM tbltrans_tailor_girl WHERE id = '"+id+"'";
    connection.query(sql, function(err, result){
      if(err){
        alert("An error occured loading the image.");
      }else{
        callback(result[0]);
      }
    });
  });
}
function logoutuser(){
  window.location.href = "./index.html";
}
function generateCSV(data){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  
  var yyyy = today.getFullYear();
  if(dd<10){
      dd='0'+dd;
  } 
  if(mm<10){
      mm='0'+mm;
  } 
  var today = dd+'-'+mm+'-'+yyyy;
  
  fs1.writeFile('csv/'+today+'.csv', data, 'utf8', function (err) {
    if (err) {
      console.log('Some error occured - file either not saved or corrupted file saved.');
    } else{
      console.log('It\'s saved!');
    }
  });
}
