const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');
var mainWindow;

require('electron-context-menu')({
   prepend: (params, browserWindow) => [{
	   label: 'Rainbow',
	   // Only show it when right-clicking images 
	   visible: params.mediaType === 'image'
   }]
});

let win;

app.on('ready', () => {
	mainWindow=  new BrowserWindow({width:1024, height:800, backgroundColor:'white'});
	mainWindow.loadURL(url.format({
		  pathname: path.join('./views/', 'index.html'),
		  protocol: 'file:',
		  slashes: true
	  }))
});