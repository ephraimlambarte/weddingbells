var rowid = "";
var myimage = 0;
var cakename = "";
var myimage = 0;
var photographername = "";
$(document).ready(function(){
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();

	});
  tableLoader("#loadPhotographerHere", "5");
  connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadPhotographerHere").html("");
				return;
			}
			$('#photographerPagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));
									loadPhotographer(connection, mypage);
							}
			 });
		}, "tblphotographer", "");
	});
  $("#addPhotoButton").on('click', function(){
  	var orgname = $('#orgName').val();
  	var contact = $('#photoContact').val();
    var firstname = $("#photoFirstName").val();
    var lastname = $("#photoLastName").val();
    var middlename  = $("#photoMiddleName").val();
    var fee = $("#photoFee").val();
    if(firstname == "" || lastname == ""||middlename == "" ||fee == ""||orgname == ""||contact == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    var photo = {
      firstname:firstname,
      lastname:lastname,
      middlename:middlename,
      fee:fee,
      orgname:orgname,
      contact:contact
    };
    connectMysql2(function(connection){
      insertPhotographer(connection, photo);
    });
  });
  $("#savePhotoButton").on('click', function(){
  	var orgname = $("#orgName").val();
  	var contact = $("#photoContact").val();
    var firstname = $("#photoFirstName").val();
    var lastname = $("#photoMiddleName").val();
    var middlename  = $("#photoMiddleName").val();
    var fee = $("#photoFee").val();
    if(rowid == ""){
      alert("Please select a row to update!");
      return;
    }
    if(firstname == "" || lastname == ""||middlename == "" ||fee == ""||orgname == ""||contact == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    var photo = {
      firstname:firstname,
      lastname:lastname,
      middlename:middlename,
      fee:fee,
      orgname:orgname,
      contact:contact,
      photoID:rowid
    };
    connectMysql2(function(connection){
      updatePhotographer(connection, photo);
    });
  });
  $("#loadPhotographerHere").on('click', 'tr', function(){
    rowid = $(this).prop("id").split("-")[1];
    var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
    photographername = product[1]+" "+product[2];
    $("#photoFirstName").val(product[1]);
    $("#photoLastName").val(product[2]);
    $("#photoMiddleName").val(product[3]);
    $("#photoFee").val(product[5]);
    $("#orgName").val(product[0]);
    $("#photoContact").val(product[4]);
    $(".userLabel").addClass("active");
  });
  $("#loadPhotographerHere").on('click', '.delete', function(){
    rowid = $(this).prop("id").split("-")[1];
    $("#deleteTitle").text("Are you sure you want to delete photographer "+photographername);
    $("#deleteModal").modal("open");
  });
  $(".mymodal").on('click', '#deleteConfirmButton', function(){
    connectMysql2(function(connection){
      deletePhotographer(connection, rowid);
    });
  });
	$("#searchPhotographer").on('keyup', function(){
		connectMysql2(function(connection){
			searchPhotographer(connection, '%'+$("#searchPhotographer").val()+'%');
		});
	});
});
function loadPhotographer(connection, page){
  var sql = "SELECT photoID, lastname, firstname, middlename, fee, orgName, contact FROM tblphotographer ORDER BY photoID DESC LIMIT "+page+", 5";
  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong, please try again later!");
    }else{
      var html = "";
      rows.forEach(function(row){
        html += "<tr id='photo-"+row.photoID+"'>";
          html += "<td>"+row.orgName+"</td>";
          html += "<td>"+row.firstname+"</td>";
          html += "<td>"+row.lastname+"</td>";
          html += "<td>"+row.middlename+"</td>";
          html += "<td>"+row.contact+"</td>";
          html += "<td>"+row.fee+"</td>";
          html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.photoID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadPhotographerHere").html(html);
    }
  });
}
function searchPhotographer(connection, search){
	var sql = "SELECT photoID, lastname, firstname, middlename, fee, orgName, contact FROM tblphotographer "+
	"WHERE lastname LIKE ? OR firstname LIKE ? OR middlename LIKE ? OR fee LIKE ? OR orgName LIKE ? OR contact LIKE ? ORDER BY photoID";
	var insert = [search,search,search,search,search,search];
	sql  =  mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong, please try again later!");
    }else{
      var html = "";
      rows.forEach(function(row){
        html += "<tr id='photo-"+row.photoID+"'>";
          html += "<td>"+row.orgname+"</td>";
          html += "<td>"+row.firstname+"</td>";
          html += "<td>"+row.lastname+"</td>";
          html += "<td>"+row.middlename+"</td>";
          html += "<td>"+row.contact+"</td>";
          html += "<td>"+row.fee+"</td>";
          html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.photoID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadPhotographerHere").html(html);
    }
  });
}
function updatePhotographer(connection, photo){
  var sql = "UPDATE tblphotographer SET firstname = ?, lastname = ?, middlename = ?, fee = ?, orgName = ?, contact = ? WHERE photoID = ?";
  var insert = [photo.firstname, photo.lastname, photo.middlename, photo.fee, photo.orgname, photo.contact, photo.photoID];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later!");
    }else{
      var html = "<td>"+photo.orgname+"</td>"; 
      html += "<td>"+photo.firstname+"</td>";
      html += "<td>"+photo.lastname+"</td>";
      html += "<td>"+photo.middlename+"</td>";
      html += "<td>"+photo.fee+"</td>";
      html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+photo.photoID+"'>Delete</a></td>";
      $("#photo-"+photo.photoID).html(html);
      alert("Successfully updated!");
    }
    connection.end();
  });
}
function deletePhotographer(connection, photoid){
  var sql = "DELETE FROM tblphotographer WHERE photoID = '"+photoid+"'";
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("This photographer is in another transactions and cannot be deleted!");
    }else{
      $("#photo-"+photoid).remove();
      alert("Successfully deleted!");
    }
    connection.end();
    rowid = "";
  });
}
function insertPhotographer(connection, photo){
  var sql = "INSERT INTO tblphotographer (firstname, lastname, middlename, fee, orgName, contact) VALUES(?,?,?,?,?,?)";
  var insert = [photo.firstname, photo.lastname, photo.middlename, photo.fee, photo.orgname, photo.contact];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, result){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later.");
    }else {
      var html = "<tr id = 'photo-"+result.insertId+"'>";
      	html += "<td>"+photo.orgname+"</td>";
        html += "<td>"+photo.firstname+"</td>";
        html += "<td>"+photo.lastname+"</td>";
        html += "<td>"+photo.middlename+"</td>";
        html += "<td>"+photo.contact+"</td>";
        html += "<td>"+photo.fee+"</td>";
        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+result.insertId+"'>Delete</a></td>";
        html += "</tr>";
        $("#loadPhotographerHere").prepend(html);
        alert("Successfully saved!");
    }
    connection.end();
    rowid = "";
  });
}
