$(document).ready(function(){
	var rowid = "";
	var myimage = 0;
	var themename = "";
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();
	});
	tableLoader("#loadThemeHere", "3");
	connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadThemeHere").html("");
				return;
			}
			$('#themePagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));
									loadTheme(connection, mypage);
							}
			 });
		}, "tbltheme", "");
	});
	$("#uploadImageTheme").on('change', function(){
		if(this.files[0].size>5000000){
			alert("Image size must not be greater than 5mb.");
			$("#uploadImageTheme").val("");
			return;
		}
		readURL(this);
	});
	$("#addThemeButton").on("click", function(){
		var themename = $("#themeName").val();
		var description = $("#themeDesc").val();
		if( document.getElementById("uploadImageTheme").files.length == 0 ){
			alert("Please upload an image for the theme!");
			return;
		}
		if(themename == "" || description == ""){
			alert("Please provide all the necessary details!");
			return;
		}
		var image = base64ToBlob($("#invImage").attr('src').replace(/^data:image\/(png|jpeg);base64,/,''));
		var theme = {
			themename: themename,
			description: description,
			image: image
		};
		connectMysql2(function(connection){
			
			connectMysql2(function(connection){
				new Promise((resolve, reject)=>{
					checkTheme(connection, themename, function(check){
						if(check){
							reject(true);
						}else{
							//console.log(check);
							resolve(true);
						}
					});		
				}).then((res)=>{
					insertTheme(connection, theme);
				}).catch((err)=>{
					alert("Theme's name already exist!");
				});
			});
		});
	});
	$("#loadThemeHere").on('click', 'tr', function(){
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		rowid = $(this).prop("id").split("-")[1];
		themename = product[0];
		$("#themeName").val(product[0]);
		$("#themeDesc").val(product[1]);
		$(".userLabel").addClass("active");
		deleteImages(function(){
			getImageTheme(rowid, function(img){
				//console.log(img);
				readWriteFile(img, myimage,function(address){
					$('#invImage').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
	});
	$("#saveThemeButton").on('click', function(){
		var themenames = $("#themeName").val();
		var description = $("#themeDesc").val();
		if(themenames == "" || description == ""){
			alert("Please provide all the necessary details!");
			return;
		}
		if(rowid == ""){
			alert("Please select a row to update!");
			return;
		}
		var checkimage = $("#uploadImageTheme").val();
		var image = base64ToBlob($("#invImage").attr('src').replace(/^data:image\/(png|jpeg);base64,/,''));
		var theme = {
			themename: themenames,
			description: description,
			image: image,
			rowid:rowid,
			checkimage:checkimage
		};
		connectMysql2(function(connection){
			new Promise((resolve, reject)=>{
				if(themename == themenames){
					resolve(true);
				}else{
					checkTheme(connection, themenames, function(check){
						if(check){
							reject(true);
						}else{
							//console.log(check);
							resolve(true);
						}
					});		
				}
			}).then((res)=>{
				updateTheme(connection, theme);
			}).catch((err)=>{
				alert("Theme's name already exist!");
			});
		});
	});
	$("#loadThemeHere").on('click', '.delete', function(){
		rowid = $(this).prop("id")[1];
		$("#deleteModal").modal("open");
		$("#deleteTitle").text("Are you sure you want to delete theme "+themename+"?");
	});
	$(".mymodal").on('click', '#deleteConfirmButton', function(){
		connectMysql2(function(connection){
			deleteTheme(connection, rowid);
		});
	});
	$("#searchTheme").on('keyup', function(){
		connectMysql2(function(connection){
			searchTheme(connection, '%'+$("#searchTheme").val()+'%');
		});
	});
});
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#invImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function insertTheme(connection, theme){
	var sql = "INSERT INTO tbltheme (themename, description, img) VALUES(?,?,?)";
	var insert = [theme.themename, theme.description, theme.image];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, result){
		if(err){
			console.log(err);
			alert("Something went wrong! please try again later!");
		}else{
			var html = "<tr id = 'theme-"+result.insertId+"'>";
			html += "<td>"+theme.themename+"</td>";
			html += "<td>"+theme.description+"</td>";
			html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+result.insertId+"'>Delete</a></td>";
			html += "</tr>";
			$("#loadThemeHere").prepend(html);
			alert("Successfully added!");
			connection.end();
		}
	});
}
function loadTheme(connection, page){
	var sql = "SELECT themeID, themename, description FROM tbltheme ORDER BY themeID DESC LIMIT "+page+", 5";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong. Please try again later.");
		}else{
			//console.log(rows);
			var html = "";
			rows.forEach(function(row){
				html += "<tr id = 'theme-"+row.themeID+"'>";
				html += "<td>"+row.themename+"</td>";
				html += "<td>"+row.description+"</td>";
				html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.themeID+"'>Delete</a></td></tr>";
			});
			$("#loadThemeHere").html(html);
		}
	});
}
function searchTheme(connection, search){
	var sql = "SELECT themeID, themename, description FROM tbltheme "+
	" WHERE themename LIKE ? OR description LIKE ? ORDER BY themeID DESC";
	var insert = [search, search];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong. Please try again later.");
		}else{
			//console.log(rows);
			var html = "";
			rows.forEach(function(row){
				html += "<tr id = 'theme-"+row.themeID+"'>";
				html += "<td>"+row.themename+"</td>";
				html += "<td>"+row.description+"</td>";
				html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.themeID+"'>Delete</a></td></tr>";
			});
			$("#loadThemeHere").html(html);
		}
	});
}
function updateTheme(connection, theme){
	var sql = "UPDATE tbltheme SET themename = ?, description = ?, img = ? WHERE themeID = ?";	
	if(theme.checkimage == ""){
		sql = "UPDATE tbltheme SET themename = ?, description = ? WHERE themeID = ?";
	}
	var insert = [theme.themename, theme.description, theme.image, theme.rowid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html = "";
			html += "<td>"+theme.themename+"</td>";
			html += "<td>"+theme.description+"</td>";
			html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+theme.rowid+"'>Delete</a></td>";
			$("#theme-"+theme.rowid).html(html);
			alert("Successfully updated!");
		}
		connection.end();
	});
}
function deleteTheme(connection, rowid){
	var sql = "DELETE FROM tbltheme WHERE themeID = '"+rowid+"'";
	connection.query(sql, function(err){
		if(err){
			console.log(err);
			alert("This theme is in another transactions so it cannot be deleted!");
		}else{
			$("#theme-"+rowid).remove();
			alert("Successfully deleted!");
		}
		connection.end();
	});
}
function checkTheme(connection, themename, callback){
	var sql = "SELECT themename FROM tbltheme WHERE themename = ?";
	var insert = [themename];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			callback(true);
		}else{
			if(rows.length > 0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
