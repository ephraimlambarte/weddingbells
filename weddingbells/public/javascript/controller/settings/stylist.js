var rowid = "";
var totalpages= "";
var visiblepages = "";
var stylistname = "";
$(document).ready(function(){
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();
	});
  tableLoader("#loadStylistHere", "5");
  $("#loadStylistHere").on('click', 'tr', function(){
    rowid = $(this).prop("id").split("-")[1];
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    stylistname = product[2]+" "+product[3];
    $("#orgName").val(product[0]);
    $("#stylistFirstName").val(product[1]);
    $("#stylistLastName").val(product[2]);
    $("#stylistMiddleName").val(product[3]);
    $("#stylistContact").val(product[4]);
    $("#stylistFee").val(product[5]);
    $(".userLabel").addClass("active");
  });
  connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadStylistHere").html("");
				return;
			}
			$('#stylistPagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));
									loadStylist(connection, mypage);
							}
			 });
		}, "tblstylist", "");
	});
  $("#addStylistButton").on('click', function(){
    var orgname = $("#orgName").val();
    var contact = $("#stylistContact").val();
    var firstname = $("#stylistFirstName").val();
    var lastname = $("#stylistLastName").val();
    var middlename = $("#stylistMiddleName").val();
    var fee = $("#stylistFee").val();
    if(firstname == "" || lastname == "" || middlename == "" || fee == ""|| orgname == ""|| contact == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    stylist = {
      lastname:lastname,
      firstname:firstname,
      middlename:middlename,
      fee:fee,
      orgname:orgname,
      contact:contact
    };
    connectMysql2(function(connection){
      insertStylist(connection, stylist);
    });
  });
  $("#saveStylistButton").on('click', function(){
    var orgname = $("#orgName").val();
    var contact = $("#photoContact").val();
    var firstname = $("#stylistFirstName").val();
    var lastname = $("#stylistLastName").val();
    var middlename = $("#stylistMiddleName").val();
    var fee = $("#stylistFee").val();
    if(rowid == ""){
      alert("Please select a row to update!");
      return;
    }
    if(firstname == "" || lastname == "" || middlename == "" || fee == ""|| orgname == ""|| contact == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    stylist = {
      lastname:lastname,
      firstname:firstname,
      middlename:middlename,
      fee:fee,
      orgname:orgname,
      contact:contact,
      stylistID:rowid
    };
    connectMysql2(function(connection){
      updateStylist(connection, stylist);
    });
  });
  $("#loadStylistHere").on('click', '.delete', function(){
    rowid = $(this).prop("id").split("-")[1];
    $("#deleteTitle").text("Are you sure you want to delete stylist "+stylistname);
    $("#deleteModal").modal("open");
  });
  $(".mymodal").on('click', '#deleteConfirmButton', function(){
    connectMysql2(function(connection){
      deleteStylist(connection, rowid);
    });
  });
	$('#searchStylist').on('keyup', function(){
		connectMysql2(function(connection){
			searchStylist(connection, '%'+$('#searchStylist').val()+'%')
		});
	});
});
function loadStylist(connection, page){
  var sql = "SELECT stylistID, lastname, firstname, middlename, fee, orgName, contact FROM tblstylist ORDER BY stylistID DESC LIMIT "+page+", 5";
  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later.");
    }else {
      var html = "";
      rows.forEach(function(row){
        html += "<tr id = 'stylist-"+row.stylistID+"'>";
        html += "<td>"+row.orgName+"</td>";
        html += "<td>"+row.firstname+"</td>";
        html += "<td>"+row.lastname+"</td>";
        html += "<td>"+row.middlename+"</td>";
        html += "<td>"+row.contact+"</td>";
        html += "<td>"+row.fee+"</td>";
        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.stylistID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadStylistHere").html(html);
    }
  });
}
function searchStylist(connection, search){
	var sql = "SELECT stylistID, lastname, firstname, middlename, fee, orgName, contact FROM tblstylist "+
	"WHERE lastname LIKE ? OR firstname LIKE ? or middlename LIKE ? OR fee LIKE ? OR orgName LIKE ? OR contact LIKE ? ORDER BY stylistID DESC";
	var insert = [search, search,search,search,search,search];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later.");
    }else {
      var html = "";
      rows.forEach(function(row){
        html += "<tr id = 'stylist-"+row.stylistID+"'>";
        html += "<td>"+row.orgName+"</td>";
        html += "<td>"+row.firstname+"</td>";
        html += "<td>"+row.lastname+"</td>";
        html += "<td>"+row.middlename+"</td>";
        html += "<td>"+row.contact+"</td>";
        html += "<td>"+row.fee+"</td>";
        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.stylistID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadStylistHere").html(html);
    }
  });
}
function updateStylist(connection, stylist){
  var sql = "UPDATE tblstylist SET lastname = ?, firstname = ?, middlename = ?, fee = ?, orgName = ?, contact = ? WHERE stylistID = ?";
  var insert = [stylist.lastname, stylist.firstname, stylist.middlename, stylist.fee, stylist.orgname, stylist.contact, stylist.stylistID];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later.");
    }else {
      var html = "<td>"+stylist.orgName+"</td>";
      html += "<td>"+stylist.firstname+"</td>";
      html += "<td>"+stylist.lastname+"</td>";
      html += "<td>"+stylist.middlename+"</td>";
      html += "<td>"+stylist.contact+"</td>";
      html += "<td>"+stylist.fee+"</td>";
      html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+stylist.stylistID+"'>Delete</a></td>";
      $("#stylist-"+stylist.stylistID).html(html);
      alert("succesfully updated!");
    }
  });
}
function insertStylist(connection, stylist){
  var sql = "INSERT INTO tblstylist(lastname, firstname, middlename, fee, orgName, contact) VALUES(?,?,?,?,?,?)";
  var insert = [stylist.lastname, stylist.firstname, stylist.middlename, stylist.fee, stylist.orgname, stylist.contact];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, result){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later.");
    }else {
      var html = "<tr id = 'stylist-"+result.insertId+"'>";
        html += "<td>"+stylist.orgname+"</td>";
        html += "<td>"+stylist.firstname+"</td>";
        html += "<td>"+stylist.lastname+"</td>";
        html += "<td>"+stylist.middlename+"</td>";
        html += "<td>"+stylist.contact+"</td>";
        html += "<td>"+stylist.fee+"</td>";
        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+result.insertId+"'>Delete</a></td>";
        html += "</tr>";
        $("#loadStylistHere").prepend(html);
        alert("succesfully added!");
    }
    rowid = "";
  });
}
function deleteStylist(connection, stylistid){
  var sql = "DELETE FROM tblstylist WHERE stylistID = '"+stylistid+"'";
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("This data is in another transactions and cannot be deleted!");
    }else{
      $("#stylist-"+stylistid).remove();
      alert("Successfully deleted!");
    }
    rowid = "";
  });
}
