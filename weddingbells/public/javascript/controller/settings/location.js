var totalpages;
var visiblepages;
var rowid="";
var mypage;
$(document).ready(function(){
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();
	});
	tableLoader("#loadLocationHere", "4");
	connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		//console.log(connection);
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadLocationHere").html("");
				return;
			}
			$('#paginationLocation').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));

								loadLocation(connection, mypage);
							}
			 });
		}, "tbllocation", "");
	});
	$("#addLocationButton").on('click', function(){
		var locationname = $("#locationName").val();
		var description = $("#descriptionLocation").val();
		if(locationname == "" || description == ""){
			alert("Please provide all the necessary details!");
			return;
		}
		var loc  = {
			locationname:locationname,
			Description:description
		};
		connectMysql2(function(connection){
			insertLocation(connection, loc);
		});
	});
	$("#loadLocationHere").on('click', '.change', function(){
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		rowid = $(this).prop("id").split("-")[1];
		$("#locationName").val(product[0]);
		$("#descriptionLocation").val(product[1]);
		$(".userLabel").addClass("active");
	});
	$("#saveLocationButton").on('click', function(){
		if(rowid == ""){
			alert("Please select a row to update!");
			return;
		}
		var locationname = $("#locationName").val();
		var description = $("#descriptionLocation").val();
		if(locationname == "" || description == ""){
			alert("Please provide all the necessary details!");
			return;
		}
		var loc  = {
			locationname:locationname,
			Description:description
		};
		connectMysql2(function(connection){
			updateLocation(connection, rowid, loc);
		});
	});
	$("#loadLocationHere").on('click', '.delete', function(){
		rowid = $(this).prop("id").split("-")[1];
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		$("#deleteTitle").text("Are you sure you want to delete "+product[0]);
		$("#deleteModal").modal("open");
	});
	$(".mymodal").on('click', '#deleteConfirmButton', function(){
		connectMysql2(function(connection){
			deleteLocation(connection, rowid);
		});
	});
	$("#searchLocation").on('keyup', function(){
		connectMysql2(function(connection){
			searchLocation(connection, '%'+$("#searchLocation").val()+'%')
		});
	});
});
function loadLocation(connection, page){

	var sql = "SELECT locationID, locationname, Description FROM tbllocation LIMIT "+page+", 5";
	connection.query(sql, function(err, rows, fields){
		if(err){
			alert("Something went wrong. please try again later!");
			console.log(err);
		}else{
			var html;
			rows.forEach(function(row){
				html += "<tr id ='loc-"+row.locationID+"'>";
					html += "<td>"+row.locationname+"</td>";
					html += "<td>"+row.Description+"</td>";
					html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.locationID+"'>Edit</a></td>";
					html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.locationID+"'>Delete</a></td>";
				html += "</tr>";
			});
			$("#loadLocationHere").html(html);
		}
	});
}
function searchLocation(connection, search){
	var sql = "SELECT locationID, locationname, Description FROM tbllocation "+
	"WHERE locationname LIKE ? OR Description LIKE ?";
	var insert  = [search, search];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			alert("Something went wrong. please try again later!");
		}else{
			var html = "";
			rows.forEach(function(row){
				html += "<tr id ='loc-"+row.locationID+"'>";
					html += "<td>"+row.locationname+"</td>";
					html += "<td>"+row.Description+"</td>";
					html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.locationID+"'>Edit</a></td>";
					html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.locationID+"'>Delete</a></td>";
				html += "</tr>";
			});
			$("#loadLocationHere").html(html);
		}
		//connection.end();
	});
}
function deleteLocation(connection, rowid){
	var sql = "DELETE FROM tbllocation WHERE locationID = '"+rowid+"'";
	connection.query(sql, function(err){
		if(err){
			alert("You cant delete this data because it is include in other transactions!");
		}else{
			$("#loc-"+rowid).remove();
			alert("succesfully deleted!");
			connection.end();
		}
	});
}
function insertLocation(connection, loc){
	var sql = "INSERT INTO tbllocation(locationname, Description) VALUES(?, ?)";
	var insert = [loc.locationname, loc.Description];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, result){
		if(err){
			console.log(err);
			alert("Location name already exists!");
		}else{
			var html = "<tr id = 'loc-"+result.insertId+"'>";
				html += "<td>"+loc.locationname+"</td>";
				html += "<td>"+loc.Description+"</td>";
				html+="<td><a class='waves-effect waves-light btn change' id='edit-"+result.insertId+"'>Edit</a></td>";
				html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+result.insertId+"'>Delete</a></td>";
			html += "</tr>";
			$("#loadLocationHere").prepend(html);
			alert("succesfully added!");
			connection.end();
		}
	});
}
function updateLocation(connection, myrowid, loc){
	var sql = "UPDATE tbllocation SET locationname=?, Description=? WHERE locationID = '"+rowid+"'";
	var insert = [loc.locationname, loc.Description];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err){
		if(err){
			alert("Something went wrong. Please try again later.");
		}else{
			html = "<td>"+loc.locationname+"</td>";
			html += "<td>"+loc.Description+"</td>";
			html+="<td><a class='waves-effect waves-light btn change' id='edit-"+rowid+"'>Edit</a></td>";
			html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+rowid+"'>Delete</a></td>";
			$("#loc-"+rowid).html(html);
			rowid = "";
			alert("succesfully saved!");
			connection.end();
		}
	});
}
