$(document).ready(function(){
	var rowid = "";
	var myimage = 0;
	var cakename = "";
  var myimage = 0;
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();
	});
  tableLoader("#loadCakeHere", "4");
  connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadCakeHere").html("");
				return;
			}
			$('#cakePagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));
									loadCake(connection, mypage);
							}
			 });
		}, "tblcake", "");
	});
  $("#addCakeButton").on('click', function(){
    var cakename = $("#cakeName").val();
    var cakedesc = $("#cakeDesc").val();
    var price = $("#cakePrice").val();
    var img =  base64ToBlob($("#invImage").attr('src').replace(/^data:image\/(png|jpeg);base64,/,''));
    if( document.getElementById("uploadImageCake").files.length == 0 ){
			alert("Please upload an image for the theme!");
			return;
		}
    if(cakename == "" || cakedesc == "" || price == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    var cake = {
      cakename: cakename,
      descrip:cakedesc,
      price:price,
      img:img
    }
    connectMysql2(function(connection){
      insertCake(connection, cake);
    });
  });
  $("#uploadImageCake").on('change', function(){
    if(this.files[0].size>5000000){
			alert("Image size must not be greater than 5mb.");
			$("#uploadImageCake").val("");
			return;
		}
		readURL(this);
  });
  $("#loadCakeHere").on('click', 'tr', function(){
    var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
    $("#cakeName").val(product[0]);
    $("#cakeDesc").val(product[1]);
    $("#cakePrice").val(product[2]);
    rowid = $(this).prop("id").split("-")[1];
    cakename = product[0];
    $(".userLabel").addClass("active");
    deleteImages(function(){
			getImageCake(rowid, function(img){
				readWriteFile(img, myimage,function(address){
					$('#invImage').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
  });
  $("#saveCakeButton").on('click', function(){
    var cakename = $("#cakeName").val();
    var cakedesc = $("#cakeDesc").val();
    var price = $("#cakePrice").val();
    var img =  base64ToBlob($("#invImage").attr('src').replace(/^data:image\/(png|jpeg);base64,/,''));
    if(rowid==""){
      alert("Please select a row to update!");
      return;
    }
    if(cakename == "" || cakedesc == "" || price == ""){
      alert("Please provide all the necessary details!");
      return;
    }
    var cake = {
      cakename: cakename,
      descrip:cakedesc,
      price:price,
      img:img,
      cakeID:rowid
    };
    connectMysql2(function(connection){
      updateCake(connection, cake);
    });
  });
  $("#loadCakeHere").on('click', '.delete', function(){
		rowid = $(this).prop("id")[1];
		$("#deleteModal").modal("open");
		$("#deleteTitle").text("Are you sure you want to delete cake "+cakename+"?");
	});
  $(".mymodal").on('click', '#deleteConfirmButton', function(){
		connectMysql2(function(connection){
			deleteCake(connection, rowid);
		});
	});
	$("#searchCake").on('keyup', function(){
		connectMysql2(function(connection){
			searchCake(connection, '%'+$("#searchCake").val()+'%');
		});
	});
});
function loadCake(connection, page){
	//console.log(connection);
  var sql = "SELECT cakeID, cakename, descrip, price, img FROM tblcake ORDER BY cakeID DESC LIMIT "+page+", 5";
  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong please try again later!");
    }else{
      var html = "";
      rows.forEach(function(row){
        html += "<tr id = 'cake-"+row.cakeID+"'>";
          html += "<td>"+row.cakename+"</td>";
          html += "<td>"+row.descrip+"</td>";
          html += "<td>"+row.price+"</td>";
          html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.cakeID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadCakeHere").html(html);
    }
  });
}
function searchCake(connection, search){
	var sql = "SELECT cakeID, cakename, descrip, price, img FROM tblcake WHERE cakename LIKE ? OR"+
	" descrip LIKE ? OR price LIKE ? ORDER BY cakeID";
	var insert = [search, search, search];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err);
      alert("Something went wrong please try again later!");
    }else{
      var html = "";
      rows.forEach(function(row){
        html += "<tr id = 'cake-"+row.cakeID+"'>";
          html += "<td>"+row.cakename+"</td>";
          html += "<td>"+row.descrip+"</td>";
          html += "<td>"+row.price+"</td>";
          html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.cakeID+"'>Delete</a></td>";
        html += "</tr>";
      });
      $("#loadCakeHere").html(html);
    }
		//connection.end();
  });
}
function insertCake(connection, cake){
  var sql = "INSERT INTO tblcake(cakename, descrip, price, img) VALUES(?,?,?,?)";
  var insert = [cake.cakename, cake.descrip, cake.price, cake.img];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, result){
    if(err){
      console.log(err);
      alert("Something went wrong please try again later.");
    }else{
      var html = "<tr id = 'cake-"+result.insertId+"'>";
        html += "<td>"+cake.cakename+"</td>";
        html += "<td>"+cake.descrip+"</td>";
        html += "<td>"+cake.price+"</td>";
        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+result.insertId+"'>Delete</a></td>";
      html += "</tr>";
      $("#loadCakeHere").prepend(html);
      alert("Successfully added!");
    }
    connection.end();
  });
}
function updateCake(connection, cake){
  var sql = "UPDATE tblcake SET cakename = ?, descrip = ?, price = ?, img = ? WHERE cakeID = ?";
  var insert = [cake.cakename, cake.descrip, cake.price, cake.img, cake.cakeID];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("Something went wrong, please try again later!");
    }else{
      var html = "<td>"+cake.cakename+"</td>";
      html += "<td>"+cake.descrip+"</td>";
      html += "<td>"+cake.price+"</td>";
      html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+cake.cakeID+"'>Delete</a></td>";
      $("#cake-"+cake.cakeID).html(html);
      alert("Successfully saved!");
    }
    connection.end();
  });
}
function deleteCake(connection, cakeid){
  var sql = "DELETE FROM tblcake WHERE cakeID = '"+cakeid+"'";
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("This data is included in other transactions and cannot be deleted!");
    }else{
      $("#cake-"+cakeid).remove();
      alert("Successfully deleted!");
    }
    connection.end();
  });
}
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#invImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
