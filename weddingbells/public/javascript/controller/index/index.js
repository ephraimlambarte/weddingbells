$(document).ready(function(){
  //connectMysql();
  $("#loginButton").on('click', function(){
    var sql = "SELECT * FROM tbluser WHERE username = ? AND password =? AND status = 'Active'";
    var insert = [$("#username").val(), $("#Password").val()];
    sql = mysql.format(sql, insert);
    connectMysql2(function(connection){
      connection.query(sql, function(err, rows, fields){
        if(err){
          console.log("An error occured");
          console.log(err);
          return;
        }
        if(rows.length==0){
          alert("Incorrect Username or Password!");
        }else{
          setSessionUsername($("#username").val(), function(userid){
              getUserType(connection, userid, function(usertype){
                connection.end();
                if(usertype=='Admin'){
                  window.location.href = "./users.html";
                }else{
                  window.location.href = "./customer.html";
                }

              });
          });
        }

      });
    });

  });

});
