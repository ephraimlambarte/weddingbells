$(document).ready(function(){
  w3.includeHTML(function(){
    $("div .dropdown-button").dropdown();
    $(".button-collapse").sideNav();
    $('select').material_select();
    $('.modal').modal();
  });
  var totalyr = 0;
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  var mydate =  new Date;
  var dat = {
    labels: ["January", "February", "March", "April", "May", "June", "July", "August","September","October","November","December"],
    datasets: [
        {
            label: "Monthly Earnings",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: []
        }
    ]
  };
  var csv = "Month,Earnings\n";
  connectMysql2(function(connection){
    getData(connection, mydate.getFullYear(), 1, function(data){
      totalyr = data;
      dat.datasets[0].data.push(data);
      csv = csv + "January,"+data+"\n";
      getData(connection, mydate.getFullYear(), 2, function(data){
        totalyr += data;
        dat.datasets[0].data.push(data);
        csv = csv + "February,"+data+"\n";
        getData(connection, mydate.getFullYear(), 3, function(data){
          totalyr += data;
          dat.datasets[0].data.push(data);
          csv = csv + "March,"+data+"\n";
          getData(connection, mydate.getFullYear(), 4, function(data){
            totalyr += data;
            dat.datasets[0].data.push(data);
            csv = csv + "April,"+data+"\n";
            getData(connection, mydate.getFullYear(), 5, function(data){
              totalyr += data;
              dat.datasets[0].data.push(data);
              csv = csv + "May,"+data+"\n";
              getData(connection, mydate.getFullYear(), 6, function(data){
                totalyr += data;
                dat.datasets[0].data.push(data);
                csv = csv + "June,"+data+"\n";
                getData(connection, mydate.getFullYear(), 7, function(data){
                  totalyr += data;
                  dat.datasets[0].data.push(data);
                  csv = csv + "July,"+data+"\n";
                  getData(connection, mydate.getFullYear(), 8, function(data){
                    totalyr += data;
                    dat.datasets[0].data.push(data);
                    csv = csv + "August,"+data+"\n";
                    getData(connection, mydate.getFullYear(), 9, function(data){
                      totalyr += data;
                      dat.datasets[0].data.push(data);
                      csv = csv + "September,"+data+"\n";
                      getData(connection, mydate.getFullYear(), 10, function(data){
                        totalyr += data;
                        dat.datasets[0].data.push(data);
                        csv = csv + "October,"+data+"\n";
                        getData(connection, mydate.getFullYear(), 11, function(data){
                          totalyr += data;
                          dat.datasets[0].data.push(data);
                          csv = csv + "November,"+data+"\n";
                          getData(connection, mydate.getFullYear(), 12, function(data){
                            totalyr += data;
                            dat.datasets[0].data.push(data);
                            console.log(dat);
                            csv = csv + "December,"+data+"\n";
                            csv = csv + "Total Earnings, "+totalyr+"\n";
                            var myNewChart = new Chart(ctx , {
                              type: "line",
                              data: dat,
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
  $("#generateCSV").on('click', function(){
    generateCSV(csv);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
   
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    var today = dd+'-'+mm+'-'+yyyy;
    $("#download").attr("href", "../csv/"+today+".csv");
    $("#download").show();
    });
});
function getData(connection, year, month, callback){
  checkIfNullDeposit(connection, year, month, function(data){
    //console.log(data);
    if(data){
      var sql = "SELECT SUM(payment) +(SELECT SUM(deposit) FROM tbltrans_location WHERE "+
      "YEAR(date) = ? AND MONTH(date) = ? AND depositReturned = 'false') AS payment "+
      "FROM tbltrans_location WHERE YEAR(date) = ? AND MONTH(date) = ?";
      var insert = [year, month, year, month];
      sql = mysql.format(sql, insert);
      checkIfNullDeposit(connection, year, month);
      connection.query(sql, function(err, rows){
        if(err){
          console.log(err);
          alert("Something went wrong. Please try again later!");
          callback(0);
        }else{
          if(rows[0].payment=== null){
            callback(0);
          }else{
            callback(rows[0].payment);
          }
        }
      });
    }else{
      var sql = "SELECT SUM(payment) AS payment "+
      "FROM tbltrans_location WHERE YEAR(date) = ? AND MONTH(date) = ?";
      var insert = [year, month, year, month];
      sql = mysql.format(sql, insert);
      checkIfNullDeposit(connection, year, month);
      connection.query(sql, function(err, rows){
        if(err){
          console.log(err);
          alert("Something went wrong. Please try again later!");
          callback(0);
        }else{
          if(rows[0].payment=== null){
            callback(0);
          }else{
            callback(rows[0].payment);
          }
        }
      });
    }
  });
 
}
function checkIfNullDeposit(connection, year, month, callbacks){
  var sql = "SELECT SUM(deposit) as mypayment FROM tbltrans_location WHERE "+
  "YEAR(date) = ? AND MONTH(date) = ? AND depositReturned = 'false'";
  var insert = [year, month, year, month];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, rows){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later!");
      callbacks(false);
    }else{
      var check;
      if(rows[0].mypayment){
        callbacks(true);
      }else{
        callbacks(false);
      }
    }
  });
}