var totalpages;
var visiblepages;
var rowid;
var action;
var myimage = 0;
var itemcode;
var edipackage =false;
var packageid= 0;
var tempoitemid;
var mypage;
$(document).ready(function(){
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();

	});
	//$('.modal').modal();
	tableLoader("#loadInventoryHere", "9");
	connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadInventoryHere").html("");
				return;
			}
			$('#inventoryPagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								mypage = (5*(page-1));
									loadInventory(connection, mypage, 'off');	
							}
			 });
			 $('#addpackagePagination').twbsPagination({
				totalPages: totalpages,
				visiblePages: visiblepages,
				onPageClick: function (event, page){
					mypage = (5*(page-1));
					loadInventoryOnPackages(connection, mypage);	
				}
 			});

		}, "tblinventory", "");
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}

			 $('#packagePagination').twbsPagination({
				totalPages: totalpages,
				visiblePages: visiblepages,
				onPageClick: function (event, page){
					mypage = (5*(page-1));
					loadActualPackages(connection, mypage);	
				}
 			});

		}, "package", "");
	});
	$("#addInventoryButton").on('click', function(){
		$("#invGenderMale").prop("disabled", false);
		$("#invGenderFemale").prop("disabled", false);
		$(".userLabel").removeClass("active");
		$("#itemQty").val("");
		$("#itemPrice").val("");
		$("#itemCode").val("");
		$("#inventoryModal").modal("open");
		$('#invImage').prop('src', '../public/images/default.jpg');
		action = "add";
	});
	$("#addPackageButton").on('click', function(){
		$('#packageModal').modal('open');
	});
	$(".mymodal").on('click', '#saveItemButton',function(){
		var itemQty = $("#itemQty").val();
		var itemPrice = $("#itemPrice").val();
		var category = $("#category").val();
		var type = $("#type").val();
		var color = $("#color").val();
		var gender = getGender();
		var image = base64ToBlob($("#invImage").attr('src').replace(/^data:image\/(png|jpeg);base64,/,''));
		var damagedItems = $("#damagedItems").val();
		//console.log(image);

		if(itemQty == "" || itemPrice == ""|| category == ""||type=="" ||color=="" || gender == ""
		 || damagedItems == ""){
			alert("Please provide all the necessary details!");
			return;
		}

		var inv = {
			gender:gender,
			type:type,
			category:category,
			color:color,
			quantity:itemQty,
			price:itemPrice,
			img:image,
			damagedItems:damagedItems
		};
		if(action =="add"){
			if( document.getElementById("uploadImageInventory").files.length == 0 ){
				alert("Please upload an image for the item!");
				return;
			}
			connectMysql2(function(connection){
				insertInventory(connection, inv)
			});
		}else if (action == "edit") {
			if( document.getElementById("uploadImageInventory").files.length == 0 ){
				inv.img = "";
			}
			connectMysql2(function(connection){
				updateInventory(connection, rowid, inv, itemcode);
			});
		}
	});
	$(".mymodal").on('change', '#uploadImageInventory', function(){
		if(this.files[0].size>5000000){
			alert("Image size must not be greater than 5mb.");
			$("#uploadImageInventory").val("")
			return;
		}
		readURL(this);
	});
	$(".mymodal").on('click', '.changeEditPackage', function(){
		var id  =  $(this).prop("id").split("-")[1];
		if(edipackage == false){
			alert("Please select a package to add this item to.");
			return;
		}
		deleteImages(function(){
			getImageInventory(id, function(img){
				//console.log(img);
				readWriteFile(img, myimage,function(address){
					$('#packageInventoryImage').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
		var items = [];
		var c = 0;
		var cproduct = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				cproduct[c]=$(this).html();
				c++;
			});
		});
		var quantity = cproduct[1];
		if (quantity == 0){
			alert("No more items!");
			return;
		}
		var html;
		html = ""
		+"<td>"+cproduct[0]+"</td>"
		+"<td>"+(quantity-1)+"</td>"
		+"<td>"+cproduct[2]+"</td>"
		+"<td><a class='waves-effect waves-light btn changeEditPackage' id='editPackage-"+id+"'>Add</a></td>"+
		"";
		$("#package-"+id).html(html);
		$('#loaditemsofpackagehere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			items.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});
		});
		
		if(items.length>0){
			var exists = false;
			var quantity=0;
			var price;
			var itemcode2;
			items.forEach(function(item){
				if(id == item.itemcode){
					exists = true;
					quantity = item.quantity;
					price = item.price;
					itemcode2 = item.itemcode2;
				}
			});
			if(exists){
				html = ""+
				"<td>"+itemcode2+"</td>"+
				"<td>"+(parseInt(quantity) + parseInt(1))+"</td>"+
				"<td>"+price+"</td>"+
				"<td><a class='waves-effect waves-light btn removeItemsOfPackage' id='removeItemsOfPackage-"+id+"'>Remove</a></td>"+
				"";
				$("#itemsOfPackage-"+id).html(html);
			}else{
				html = "<tr id = 'itemsOfPackage-"+id+"'>"+
				"<td>"+cproduct[0]+"</td>"+
				"<td>1</td>"+
				"<td>"+cproduct[2]+"</td>"+
				"<td><a class='waves-effect waves-light btn removeItemsOfPackage' id='removeItemsOfPackage-"+id+"'>Remove</a></td>"+
				"</tr>";
				$("#loaditemsofpackagehere").append(html);
			}
		}else{
			html = "<tr id = 'itemsOfPackage-"+id+"'>"+
			"<td>"+cproduct[0]+"</td>"+
			"<td>1</td>"+
			"<td>"+cproduct[2]+"</td>"+
			"<td><a class='waves-effect waves-light btn removeItemsOfPackage' id='removeItemsOfPackage-"+id+"'>Remove</a></td>"+
			"</tr>";
			$("#loaditemsofpackagehere").append(html);
		}
	});
	$(".mymodal").on('click', '.removeItemsOfPackage', function(){
		var id  =  $(this).prop("id").split("-")[1];
		deleteImages(function(){
			getImageInventory(id, function(img){
				//console.log(img);
				readWriteFile(img, myimage,function(address){
					$('#packageInventoryImage').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
		var items = [];
		var c = 0;
		var cproduct = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				cproduct[c]=$(this).html();
				c++;
			});
		});
		var quantity = cproduct[1];
		if (quantity == 1){
			$("#itemsOfPackage-"+id).remove();
			
		}
		var html;
		html = ""
		+"<td>"+cproduct[0]+"</td>"
		+"<td>"+(quantity-1)+"</td>"
		+"<td>"+cproduct[2]+"</td>"
		+"<td><a class='waves-effect waves-light btn removeItemsOfPackage' id='removeItemsOfPackage-"+id+"'>Remove</a></td>"+
		"";
		$("#itemsOfPackage-"+id).html(html);
		$('#loadPackageItemsHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			items.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});			
		});
		var myquantity = 0;
		items.forEach(function(item){
			if(item.itemcode == id){
				myquantity = item.quantity;
			}
		});
		html = ""
		+"<td>"+cproduct[0]+"</td>"
		+"<td>"+(parseInt(myquantity)+parseInt(1))+"</td>"
		+"<td>"+cproduct[2]+"</td>"
		+"<td><a class='waves-effect waves-light btn changeEditPackage' id='editPackage-"+id+"'>Add</a></td>"+
		"";
		$("#package-"+id).html(html);
	});
	$(".mymodal").on('click', '.changeEditActualPackage', function(){
		edipackage = true;
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		$("#packageName").val(product[0]);
		$("#packagePrice").val(product[1]);
		$(".userLabel").addClass("active");
		 packageid = $(this).prop("id").split("-")[1];
		 connectMysql2(function(connection){
			loadItemsOfPackages(connection, packageid);
			loadInventoryOnPackages(connection, 0);
			getDescriptionPackage(connection, packageid, function(description){
				$("#packageDescription").val(description);
			});
		 });
		 
	});
	$("#loadInventoryHere").on('click', '.change', function(){
		$("#invGenderMale").prop("disabled", true);
		$("#invGenderFemale").prop("disabled", true);
		rowid = $(this).prop("id").split("-")[1];
		$('#inventoryModal').modal("open");
		deleteImages(function(){
			getImageInventory(rowid, function(img){
				//console.log(img);
				readWriteFile(img, myimage,function(address){
					$('#invImage').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		itemcode = product[0];
		setGender(product[1]);
		$("#itemCode").val(product[0]);
		$("#itemQty").val(product[5]);
		$("#itemPrice").val(product[6]);
		$("#category").val(product[3]).material_select();
		$("#type").val(product[2]).material_select();
		$("#color").val(product[4]).material_select();	
		$("#damagedItems").val(product[8]);
		$(".userLabel").addClass("active");
		action = "edit";
	});
	$("#loadInventoryHere").on('click', '.delete', function(){
		rowid = $(this).prop("id").split("-")[1];
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		$("#deleteTitle").text("Are you sure you want to delete item "+product[0]+"?");
		$("#deleteModal").modal("open");
	});
	$(".mymodal").on('click', '.viewImage', function(){
		$("#imagemodal").modal("open");
		rowid = $(this).prop("id").split("-")[1];
		//$('#inventoryModal').modal("open");
		deleteImages(function(){
			getImageInventory(rowid, function(img){
				console.log(rowid);
				readWriteFile(img, myimage,function(address){
					$('#itemImageModal').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
	});
	$("#loadInventoryHere").on('click', '.viewImage', function(){
		$("#imagemodal").modal("open");
		rowid = $(this).prop("id").split("-")[1];
		//$('#inventoryModal').modal("open");
		deleteImages(function(){
			getImageInventory(rowid, function(img){
				console.log(rowid);
				readWriteFile(img, myimage,function(address){
					$('#itemImageModal').prop('src', '../public/temp/out'+myimage+'.jpg');
					myimage++;
				});
			});
		});
	});
	$(".mymodal").on('click', '#deleteConfirmButton', function(){
		connectMysql2(function(connection){
			deleteInventory(connection, rowid);
		});
	});
	$("#searchItem").on('keyup', function(){
		connectMysql2(function(connection){
				searchInventory(connection, "%"+$("#searchItem").val()+"%");
		});
	});
	$(".mymodal").on('click', '#invGenderMale', function(){

		var html = "<option value='coat'>Coat</option>"+
			"<option value='slacks'>Slacks</option>"+
			"<option value='polo'>Polo</option>"+
			"<option value='vest'>Vest</option>"+
			"<option value='others'>Others</option>";
		$("#category").html(html);
		$("#category").material_select();
	});
	$(".mymodal").on('click', '#invGenderFemale', function(){
		var html = "<option value='cocktail'>Cocktail</option>"+
			"<option value='ballgown'>Ballgown</option>"+
			"<option value='longdress'>Long Dress</option>"+
			"<option value='others'>Others</option>";
		$("#category").html(html);
		$("#category").material_select();
	});
	$(".mymodal").on('click', '#addPackageButton', function(){
		var packageName = $("#packageName").val();
		var packagePrice = $("#packagePrice").val();
		var description = 	$("#packageDescription").val();
		if(packageName == "" || packagePrice == "" || description == ""){
			alert("Please fill up all the necessary fields.");
			return;
		}
		pack = {
			packageName:packageName,
			packagePrice:packagePrice,
			description:description
		};
		connectMysql2(function(connection){
			new Promise((resolve, reject)=>{
				checkIfPackageAlreadyExist(connection, packageName, function(check){
					if(check){
						reject(check);
					}else{
						resolve(true);
					}
				});
			}).then((res)=>{
				addPackage(connection, pack);
			}).catch((err)=>{
				console.log(err);
				alert("Package name already exists!");
			});
		});
	});
	$(".mymodal").on('click', '#editPackageButton', function(){
		var packageName = $('#packageName').val();
		var packagePrice = $("#packagePrice").val();
		var description = 	$("#packageDescription").val();
		if(packageName == "" || packagePrice == "" || description == ""){
			alert("Please fill up all the necessary fields!");
			return;
		}
		var items=[];
		$('#loaditemsofpackagehere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			items.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});
		});
		if(items.length == 0){
			alert("A package must contain atleast 1 item!");
			return;
		}
		
		var package = {
			packageName:packageName,
			packagePrice:packagePrice,
			items:items, 
			packageID:packageid,
			description:description
		}
		connectMysql2(function(connection){
			insertItemsToPackage(connection, package);	
		
		});
	});
	$("#viewTempoItems").on('click', function(){
		$("#viewTempoItemsModal").modal("open");
		connectMysql2(function(connection){
			loadTemporaryItems(connection);
		});
	});
	$(".mymodal").on('click', '.addToInventory', function(){
		$("#confirmAddToInventoryModal").modal("open");
		tempoitemid = $(this).prop("id").split("-")[1];
	});
	$(".mymodal").on('click', '#confirmItemAddToInventory', function(){
		connectMysql2(function(){
			transferItemsToInventory(connection, tempoitemid);
		});
		
	});
	$("#filterTrigger").on('click', function(){
		if($("#filterTrigger").val() == "on"){
			$("#filterTrigger").val("off")
		}else{
			$("#filterTrigger").val("on")
		}
		if($("#filterTrigger").val()=="off"){
			loadInventory(connection, mypage, "off");
			$("#filterMale").prop("disabled", true);
			$("#filterFemale").prop("disabled", true);
			$("#filterCategory").prop("disabled", true);
			$("#filterType").prop("disabled", true);
			$("#filterColor").prop("disabled", true);
		}else{
			loadInventory(connection, mypage, "on");
			$("#filterMale").prop("disabled", false);
			$("#filterFemale").prop("disabled", false);
			$("#filterCategory").removeAttr("disabled");
			$("#filterType").removeAttr("disabled");
			$("#filterColor").removeAttr("disabled");
		}
		$("#filterCategory, #filterType, #filterColor").material_select();
		
	});
	$("#filterMale").on('click', function(){
		var html = "<option value=''>--</option>"+
		"<option value='coat'>Coat</option>"+
			"<option value='slacks'>Slacks</option>"+
			"<option value='polo'>Polo</option>"+
			"<option value='vest'>Vest</option>"+
			"<option value='others'>Others</option>";
		$("#filterCategory").html(html);
		$("#filterCategory").material_select();
		loadInventory(connection, mypage, "on");
	});
	$("#filterFemale").on('click', function(){
		var html = "<option value=''>--</option>"+
		"<option value='cocktail'>Cocktail</option>"+
				"<option value='ballgown'>Ballgown</option>"+
				"<option value='longdress'>Long Dress</option>"+
				"<option value='others'>Others</option>";
		$("#filterCategory").html(html);
		$("#filterCategory").material_select();
		loadInventory(connection, mypage, "on");
	});
	$("#filterCategory").on('change', function(){
		loadInventory(connection, mypage, "on");
	});
	$("#filterType").on('change', function(){
		loadInventory(connection, mypage, "on");
	});	
	$("#filterColor").on('change', function(){
		loadInventory(connection, mypage, "on");
	});
});
function loadInventoryOnPackages(connection, page){
 var sql = "SELECT itemcode, itemcode2, quantity, price, damagedItems FROM tblinventory LIMIT "+page+", 5";
 connection.query(sql, function(err, rows, fields){
	if(err){
		console.log(err);
		alert("Something went wrong, please try again later!");
	}else{
		var html;
		if(rows.length == 0){
			 html = "";
			$("#loadPackageItemsHere").html(html);
		}else{
			$("#loadPackageItemsHere").html("");
			rows.forEach(function(row){
				getReservedItem(connection, row.itemcode, function(qty){
					html+="<tr id = 'package-"+row.itemcode+"'>";
					html +="<td>"+row.itemcode2+"</td>";
					html += "<td>"+(returnAbs(row.quantity, qty, row.damagedItems))+"</td>";
					html +="<td>"+row.price+"</td>";
					html+="<td><a class='waves-effect waves-light btn changeEditPackage' id='editPackage-"+row.itemcode+"'>Add</a></td>";
					html+="<td><a class='waves-effect waves-light btn viewImage' id='viewImage-"+row.itemcode+"'><i class='material-icons'>pageview</i></a></td>";
					html +="<tr/>"
				$("#loadPackageItemsHere").append(html);
				html = "";
				});
			});
			
		}
	}
	});
}
function loadInventory(connection, page, filter){
	var sql = "";
	if(filter == "off"){
		sql = "SELECT itemcode, itemcode2, gender, type, category, damagedItems, color, quantity, price, availableItems FROM tblinventory LIMIT "+page+", 5";
	}else{
		sql = "SELECT itemcode, itemcode2, gender, type, category, damagedItems, availableItems,"+
		" color, quantity, price FROM tblinventory WHERE gender = ? AND category LIKE ? "+
		"AND type LIKE ? AND color LIKE ? "+
		" LIMIT "+page+", 5";
		var insert = [getFilterGender(), '%'+$("#filterCategory").val()+'%',
		'%'+$("#filterType").val()+'%', '%'+$("#filterColor").val()+'%'];
		sql = mysql.format(sql, insert);
	}
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html;
			if(rows.length == 0){
				 html = "";
				$("#loadInventoryHere").html(html);
			}else{
				$("#loadInventoryHere").html("");
				rows.forEach(function(row){
					getReservedItem(connection, row.itemcode, function(qty){
						html+="<tr id = 'inv-"+row.itemcode+"'>";
						html +="<td>"+row.itemcode2+"</td>";
						html +="<td>"+row.gender+"</td>";
						html +="<td>"+row.type+"</td>";
						html +="<td>"+row.category+"</td>";
						html +="<td>"+row.color+"</td>";
						html +="<td>"+row.quantity+"</td>";
						html +="<td>"+row.price+"</td>";
						html += "<td>"+qty+"</td><td>"+row.damagedItems+"</td><td>"+row.availableItems+"</td>";
						html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.itemcode+"'>Edit</a></td>";
						html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.itemcode+"'>Delete</a></td>";
						html+="<td><a class='waves-effect waves-light btn viewImage' id = 'viewImage-"+row.itemcode+"'><i class='material-icons'>pageview</i></a></td>";
					html +="<tr/>"
					$("#loadInventoryHere").append(html);
					html = "";
					});
				});
				
			}
		}
	});
}
function searchInventory(connection, search){
	var sql = "SELECT itemcode, itemcode2, gender, type, category, color, quantity, "+
	"price FROM tblinventory WHERE itemcode2 LIKE ? OR gender LIKE ?  OR gender LIKE "+
	"? OR type LIKE ? OR color LIKE ? OR price LIKE ?";
	var insert = [search,search,search,search,search,search];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html;
			if(rows.length == 0){
				 html = "";
				$("#loadInventoryHere").html(html);
			}else{
				rows.forEach(function(row){
					html+="<tr id = 'inv-"+row.itemcode+"'>";
						html +="<td>"+row.itemcode2+"</td>";
						html +="<td>"+row.gender+"</td>";
						html +="<td>"+row.type+"</td>";
						html +="<td>"+row.category+"</td>";
						html +="<td>"+row.color+"</td>";
						html +="<td>"+row.quantity+"</td>";
						html +="<td>"+row.price+"</td>";
						html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.itemcode+"'>Edit</a></td>";
						html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.itemcode+"'>Delete</a></td>";
					html +="<tr/>"
				});
				$("#loadInventoryHere").html(html);
			}
		}
	});
}
function returnAbs(firstvalue, secvalue, thirdvalue){
	var myval = (firstvalue-secvalue)-thirdvalue;
	if(myval<0){
		return 0;
	}else{
		return myval;
	}
}
function insertInventory(connection, inv){
	var sql  = "INSERT INTO tblinventory(gender, type, category, color, quantity, price, img, damagedItems, availableItems) VALUES(?, ?, ?, ?, ?, ?, ?, ?,?)";
	var available = inv.quantity - inv.damagedItems;
	if(available <0 ){
		available = 0;
	}
	var insert = [inv.gender, inv.type, inv.category, inv.color, inv.quantity, inv.price, inv.img, inv.damagedItems, available];

	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, result){
		if(err){
			//console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var itemcode = generateItemCode(result.insertId);
			//console.log(itemcode);
			updateItemCode(connection, result.insertId, itemcode, inv);
		}
	});
}
function updateItemCode(connection, id, itemcode, inv){
	var sql = "UPDATE tblinventory SET itemcode2 ='"+itemcode+"' WHERE itemcode = '"+id+"'";
	connection.query(sql, function(err){
		if(err){
			console.log("Something went wrong generating the itemcode.");
			deleteItemCodeErr(id, connection);
		}else{
			if(inv != null){
				var html = "<tr id = 'inv-"+id+"'>";
				html +="<td>"+itemcode+"</td>";
				html +="<td>"+inv.gender+"</td>";
				html +="<td>"+inv.type+"</td>";
				html +="<td>"+inv.category+"</td>";
				html +="<td>"+inv.color+"</td>";
				html +="<td>"+inv.quantity+"</td>";
				html +="<td>"+inv.price+"</td>";
				html +="<td>0</td>";
				html +="<td>"+inv.damagedItems+"</td>";
				html +="<td>"+(parseFloat(inv.quantity) -parseFloat(inv.damagedItems))+"</td>";
				html+="<td><a class='waves-effect waves-light btn change' id='edit-"+id+"'>Edit</a></td>";
				html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+id+"'>Delete</a></td>";
				html+="<td><a class='waves-effect waves-light btn viewImage' id = 'viewImage-"+id+"'><i class='material-icons'>pageview</i></a></td>";
				html += "</tr>";
				$("#loadInventoryHere").prepend(html);
				alert("Successfully saved!");
			}
			
					
		}
		connection.end();
	});
}
function deleteItemCodeErr(id, connection){
	var sql = "DELETE FROM tblinventory WHERE itemcode = '"+id+"'";
	connection.query(sql);
}
function getGender(){
	if($("#invGenderMale").prop("checked")){
		return "Male";
	}else if ($("#invGenderFemale").prop("checked")) {
		return "Female";
	}else{
		return "";
	}
}
function getFilterGender(){
	if($("#filterMale").prop("checked")){
		return "Male";
	}else if ($("#filterFemale").prop("checked")) {
		return "Female";
	}else{
		return "";
	}
}
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#invImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function setGender(gender){
	if(gender == "Male"){
		$("#invGenderMale").prop("checked", true);
		var html = "<option value='coat'>Coat</option>"+
			"<option value='slacks'>Slacks</option>"+
			"<option value='polo'>Polo</option>"+
			"<option value='vest'>Vest</option>"+
			"<option value='others'>Others</option>";
		$("#category").html(html);
		$("#category").material_select();
	}else{
		var html = "<option value='cocktail'>Cocktail</option>"+
			"<option value='ballgown'>Ballgown</option>"+
			"<option value='longdress'>Long Dress</option>"+
			"<option value='others'>Others</option>";
		$("#category").html(html);
		$("#category").material_select();
		$("#invGenderFemale").prop("checked", true)
	}
}
function updateInventory(connection, rowid, inv, itemcode){
	var sql = "";
	var available = inv.quantity - inv.damagedItems;
	if(available <0 ){
		available = 0;
	}
	if(inv.img != ""){
	 	sql  = "UPDATE tblinventory SET gender=?, type=?, category=?, color=?, quantity=?, price=?, img=?, damagedItems = ?, availableItems = ? WHERE itemcode = '"+rowid+"'";
		var insert = [inv.gender, inv.type, inv.category, inv.color, inv.quantity, inv.price, inv.img, inv.damagedItems, available];
		sql = mysql.format(sql, insert);
	}else{
		sql  = "UPDATE tblinventory SET gender=?, type=?, category=?, color=?, quantity=?, price=?, damagedItems = ?, availableItems = ? WHERE itemcode = '"+rowid+"'";
		var insert = [inv.gender, inv.type, inv.category, inv.color, inv.quantity, inv.price, inv.damagedItems, available];
		sql = mysql.format(sql, insert);
	}
	connection.query(sql, function(err){
		if(err){
			console.log(err);
			alert("Something went wrong. please try again later.");
		}else{
			alert("Successfully updated!");
			getReservedItem(connection, rowid, function(qty){
				var html = "<td>"+itemcode+"</td>";
				html += "<td>"+inv.gender+"</td>";
				html += "<td>"+inv.type+"</td>";
				html += "<td>"+inv.category+"</td>";
				html += "<td>"+inv.color+"</td>";
				html += "<td>"+inv.quantity+"</td>";
				html += "<td>"+inv.price+"</td>";
				html += "<td>"+qty+"</td><td>"+inv.damagedItems+"</td><td>"+available+"</td>";
				html += "<td><a class='waves-effect waves-light btn change' id='edit-"+rowid+"'>Edit</a></td>";
				html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+rowid+"'>Delete</a></td>";
				html+="<td><a class='waves-effect waves-light btn viewImage' id = 'viewImage-"+rowid+"'><i class='material-icons'>pageview</i></a></td>";
				$("#inv-"+rowid).html(html);
			});
			
		}
	});
}
function deleteInventory(connection, rowid){
	var sql = "DELETE FROM tblinventory WHERE itemcode = '"+rowid+"'";
	connection.query(sql, function(err){
		if(err){
			alert("Something went wrong! please try again later.");
		}else{
			$("#inv-"+rowid).remove();
			alert("Successfully Deleted!");
		}
	});
}
function getReservedItem(connection, itemid, callback){
	var sql = "SELECT SUM(r.qty) AS mysum FROM tbltrans_rent r INNER JOIN tbltrans_location l  ON l.trans_locationid ="+
	" r.locationID WHERE r.itemcode = ? AND l.depositReturned <> 'false'";
	var insert = [itemid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			callback(0);
		}else{
			//console.log(sql);
			if(rows[0].mysum == undefined){
				callback(0);
			}else{
				//console.log(rows.mysum);
				callback(rows[0].mysum);
			}
			
		}
	});
}
function addPackage(connection, pack){
	var sql = "INSERT INTO package(packageName, price, description) VALUES(? , ?, ?)";
	var insert = [pack.packageName, pack.packagePrice, pack.description];
	
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, result){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html = "<tr id ='actualPackage-"+result.insertId+"'>"+
							"<td>"+pack.packageName+"</td><td>"+pack.packagePrice+"</td>"+
							"<td><a class='waves-effect waves-light btn changeEditActualPackage' id='changeEditActualPackage-"+result.insertId+"'>edit</a></td>"+
						"</tr>";
			$("#addPackageItemsHere").append(html);
			alert("Saved!");
		}
	});
}
function loadActualPackages(connection, mypage){
	var sql = "SELECT packageName, packageID, price FROM package LIMIT "+mypage+", 5";
	connection.query(sql, function(err, rows, fields){
	   if(err){
		   console.log(err);
		   alert("Something went wrong, please try again later!");
	   }else{
		   var html;
		   if(rows.length == 0){
				html = "";
			   $("#addPackageItemsHere").html(html);
		   }else{
			   $("#addPackageItemsHere").html("");
			   rows.forEach(function(row){
					html+="<tr id = 'actualPackage-"+row.packageID+"'>";
					html +="<td>"+row.packageName+"</td>";
					html += "<td>"+row.price+"</td>";
					html+="<td><a class='waves-effect waves-light btn changeEditActualPackage' id='editActualPackage-"+row.packageID+"'>Edit</a></td>";
				   html +="<tr/>"
			   });
			   $("#addPackageItemsHere").html(html);
			   html = "";
		   }
	   }
   });
}
function loadItemsOfPackages(connection, packageid){
	var sql = "SELECT i.itemcode2, i.itemcode,  h.itemQuantity, i.price FROM packagehistory h"+
	" INNER JOIN package p ON p.packageID = h.packageID INNER JOIN tblinventory"+
	" i ON i.itemcode = h.inventoryID WHERE p.packageID = ?";
	var insert = [packageid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html="";
			if(rows.length == 0){
				 html = "";
				$("#loaditemsofpackagehere").html(html);
			}else{
				rows.forEach(function(row){
					html+="<tr id = 'itemsOfPackage-"+row.itemcode+"'>";
						html +="<td>"+row.itemcode2+"</td>";
						html +="<td>"+row.itemQuantity+"</td>";
						html +="<td>"+row.price+"</td>";
						html+="<td><a class='waves-effect waves-light btn removeItemsOfPackage defaultItem' id='removeItemsOfPackage-"+row.itemcode+"'>Remove</a></td>";
					html +="</tr>";
				});
				console.log(html);
				$("#loaditemsofpackagehere").html(html);
			}
		}
	});
}

function insertItemsToPackage(connection, package){
	connection.beginTransaction(function(err){
		if(err){
			console.log(err);
			throw err;
			return;
		}
		var sql = "UPDATE package SET packageName = ?, price = ?, description = ? WHERE packageID = ?";
		var insert = [package.packageName, package.packagePrice, package.description, package.packageID];
		sql = mysql.format(sql, insert);
		connection.query(sql, function(err, result){
			if(err){
				console.log(err);
				alert("Something went wrong. Please try again later!");
				connection.rollback(function(err){
					throw err;	
				});
			}else{
				sql = "DELETE FROM packagehistory WHERE packageID = ?";
				insert = [package.packageID];
				sql = mysql.format(sql, insert);
				connection.query(sql, function(err){
					if(err){
						console.log(err);
						alert("Something went wrong. Please try again later!");
						connection.rollback(function(err){
							throw err;
						});
					}else{
						getSqlForInsertPackage(package, function(sqlstatement){
							connection.query(sqlstatement,function(err){
								if(err){
									console.log(err);
									alert("Something went wrong. Please try again later!");
									connection.rollback(function(err){
										throw err;
									});
								}else{
									connection.commit(function(err){
										if(err){
											connection.rollback(function(err){
												throw err;
											});
											console.log(err);
										}
										connection.end();
										alert("Succesfully saved!");
									});
								}
							});
						});
					}
				});
			}
		});
	});
}
function getSqlForInsertPackage(package, callback){
	var counter = 0;
	sql = "";
	package.items.forEach(function(item, index, array){
		sql += "INSERT INTO packagehistory(itemQuantity, inventoryID, packageID) VALUES (?,?,?); ";
		insert = [item.quantity, item.itemcode, package.packageID];
		sql = mysql.format(sql, insert);
		console.log(sql);
		counter++;
		if (counter == array.length){
			callback(sql);
		}
	});
}
function loadTemporaryItems(connection){
	var sql = "SELECT CustomerID, gender, type, category, color, tempoID, returned, "+
	"quantity, price, dateToBeReturned FROM temporaryitemtable";
	connection.query(sql, function(err, rows){
		var html;
		$("#loadTempoItemsHere").html("");
		rows.forEach(function(row){
			html = "";
			getCustomerNameForTempoItems(connection, row.CustomerID, function(customername){
				html += "<tr><td>"+customername+"</td>";
				html += "<td>"+row.gender+"</td>";
				html += "<td>"+row.type+"</td>";
				html += "<td>"+row.category+"</td>";
				html += "<td>"+row.color+"</td>";
				html += "<td>"+row.quantity+"</td>";
				html += "<td>"+row.price+"</td>";
				html += "<td>"+row.dateToBeReturned.toString().slice(0,15)+"</td>";
				if(row.returned == "true"){
					html+="<td><button class='waves-effect waves-light btn addToInventory' id='addToInventory-"+row.tempoID+"' disabled>Add</button></td></tr>";
				}else{
					html+="<td><button class='waves-effect waves-light btn addToInventory' id='addToInventory-"+row.tempoID+"'>Add</button></td></tr>";
				}
				
				$("#loadTempoItemsHere").append(html);
			});
		});
	});
}
function getCustomerNameForTempoItems(connection, custID, callback){
	var sql = "SELECT lastname, firstname FROM tblcustomer WHERE custID = ?";
	var insert = [custID];
	sql  = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback("");
		}else{
			callback(rows[0].lastname + ", "+ rows[0].firstname);
		}
	});
}
function transferItemsToInventory(connection, tempoitemid){
	var sql = "SELECT gender, type, category, color, quantity, price FROM temporaryitemtable "+
				"WHERE tempoID = ?";
	var insert = [tempoitemid];
	sql  =  mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			alert("Something went wrong please try again later!");
			return;
		}else{
			sql = "INSERT INTO tblinventory(gender, type, category, color, quantity, price) "+
			"VALUES (?,?,?,?,?,?)";
			insert = [rows[0].gender,rows[0].type, rows[0].category, rows[0].color,
			rows[0].quantity,rows[0].price];
			sql = mysql.format(sql, insert);
			connection.query(sql, function(err, result){
				if(err){
					console.log(err);
					alert("Something went wrong. Please try again later!");
					return;
				}else{
					var itemcode = generateItemCode(result.insertId);
					//console.log(itemcode);
					updateItemCode(connection, result.insertId, itemcode, null);
					sql = "UPDATE temporaryitemtable SET returned = 'true' WHERE tempoID = ?";
					insert = [tempoitemid];
					sql  = mysql.format(sql, insert);
					connection.query(sql, function(err){
						if(err){
							console.log(err);
							alert("Something went wrong. please try again later!");
							return;
						}else{
							alert("saved!");
							$("#addToInventory-"+tempoitemid).prop("disabled", true);
						}
					});
				}
			});
		}
	});
}
function checkIfPackageAlreadyExist(connection, packagename, callback){
	var sql = "SELECT packageName FROM package WHERE packageName = ?";
	var insert = [packagename];
	sql  = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(true);
		}else{
			if(rows.length>0){
				console.log(rows);
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function getDescriptionPackage(connection, packageid, callback){
	var sql = "SELECT description FROM package WHERE packageID = ?";
	var insert = [packageid];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(false);
		}else{
			callback(rows[0].description);
		}
	});
}