var transid;
var dateTransacted;
var time_transacted;
var image = 0;
var locationid = 0;
var itemsSelected = [];
var item = {
    quantity:0,
    price: 0,
    itemcode2: "",
    itemcode:""
};
var maletailorid = 0;
var femaletailorid = 0;
var myimage = 0;
var maleTailor = [];
var femaleTailor = [];
var addmaletailorid = 0;
var addedfemaletailorid = 0;
var mylocationid = 0;
var deleteTransac = "";
var deleteTransLocId = 0;
var edittailormale = false;
var editmaletailorid = 0;
var edittailorfemale = false;
var editfemaletailorid = 0;
var mypage = 0;
var rentdatestart;
var rentdateend;
var geteventstart;
var geteventend;
var rentattire =false;
var conflict = "update";
var transacUpdate;
var transacInsert;
$(document).ready(function(){
    w3.includeHTML(function(){
        $("div .dropdown-button").dropdown();
        $(".button-collapse").sideNav();
        $('select').material_select();
        $('.modal').modal();
        $(".datepicker1").pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
      });
        $('#eventTime, #eventTimeStart, #eventTimeStartTrans, #eventTimeEndTrans').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: true, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
      });
        $('#eventTimeTrans, #photoStartTimeTrans, #photoStartTime, #photoEndTimeTrans, #photoEndTime, #stylistStartTimeTrans, #stylistStartTime, #stylistEndTimeTrans, #stylistEndTime').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: true, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
      });
            $('ul.tabs').tabs();
            var now = new Date();
            var mydate = new Date(now.setDate(now.getDate() + 15));
            var thisdate = mydate.getDate()+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
            $("#claimdateTrans").val(thisdate);
    });
    
    $("#eventDateStart").on('change', function(){
        $("eventDateStart").val(" ");
        var dt = new Date($('#eventDateStart').val());
        var mydate = new Date(dt.setDate(dt.getDate()) - 1);
        var thisdate = mydate.getDate()+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
        $('#rentstartdate').val(thisdate);
    });
    $('#eventDateEnd').on('change', function(){
        $("eventDateEnd").val(" ");
        var dt = new Date($('#eventDateEnd').val());
        var mydate = new Date(dt.setDate(dt.getDate()) + 1);
        var checkdate = new Date(mydate.getFullYear(), mydate.getMonth()+1, 0);
        if (checkdate.getDate()==dt.getDate()){
            var thisdate = 1+" "+getMonthName(mydate.getMonth()+2)+", "+mydate.getFullYear();
        }
        else{
            var thisdate = (mydate.getDate()+1)+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
        }
        $('#rentenddate').val(thisdate);
    });
    tableLoader("#loadTransacListHere", "6");
    connectMysql2(function(connection){
        getUserType(connection, sessionStorage.getItem('id'), function(usertype){
            if(usertype != 'Admin'){
                $("#userPage").remove();
            }
        });
        getTransactionRows(function(rows){
            if(rows<=5){
                visiblepages=1;
            }else {
                visiblepages=5;
            }
            if(rows%5!=0){
                totalpages=Math.floor((rows/5))+1;
            }else{
                totalpages=rows/5;
            }
            if(rows==0){
                $("#loadTransacListHere").html("");
                return;
            }
            $('#transactionListPagination').twbsPagination({
                            totalPages: totalpages,
                            visiblePages: visiblepages,
                            onPageClick: function (event, page){
                                mypage = (5*(page-1));
                                    loadTransactionList(connection, mypage);
                            }
             });
        });
 
    });
    $("#loadTransacListHere").on('click', '.viewDetails', function(){
        transid = $(this).prop("id").split("-")[1];
        $("#rentattireTransactionModal").modal("open");
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        connectMysql2(function(connection){
            getMaxRows(function(rows){
                if(rows<=5){
                    visiblepages=1;
                }else {
                    visiblepages=5;
                }
                if(rows%5!=0){
                    totalpages=Math.floor((rows/5))+1;
                }else{
                    totalpages=rows/5;
                }
                if(rows==0){
                    $("#loadInventoryItemsHere").html("");
                    return;
                }
                $('#inventoryPagination').twbsPagination({
                                totalPages: totalpages,
                                visiblePages: visiblepages,
                                onPageClick: function (event, page){
                                    inventoryPage = (5*(page-1));
                                        loadInventoryTable(connection, inventoryPage);
                                }
                 });
            }, "tblinventory", "");
            selectedLocation(connection, transid, function(locationid){
                getDateTransacted(connection, locationid);
                getTimeTransacted(connection, locationid)
                getDeposit(connection, locationid);
                getTotalPayment(connection, transid, locationid);
                downPayment(connection, transid, locationid);
                getEventTimeStart(connection, locationid);
                getDateStartDateEnd(connection, transid, locationid);
                getRent(connection, transid, locationid);
                checkIfTransHasTheme(connection, transid, locationid, function(hastheme, themeid){
                    loadTheme(connection, hastheme, themeid);
                });
                getCakes(connection, transid, locationid, function(cakearr){
                    loadCake(connection, cakearr);
                });
                getPhotographer(connection, transid, locationid, function(photoid){
                    loadPhotographer(connection, photoid);
                });
                loadPackages(connection);
                loadMaleTailor(connection, transid, locationid);
                loadTailorFemale(connection, transid, locationid);
                loaditemsadded(connection, transid, locationid);
                getStylist(connection, transid, locationid, function(stylistid){
                    loadStylist(connection, stylistid);
                });
            });
        });
 
        dateTransacted = product[2];
        time_transacted = product[3];
        $(".userLabel").addClass("active");
        $("#eventDate").val(getDateFormat(dateTransacted));
        $("#eventTime").val(get12Hour(time_transacted));
    }); 
    
    $(".mymodal").on('change', '#cake', function(){
        var cakeid = $("#cake").val()[$("#cake").val().length-1];
        deleteImages(function(){
            getImageCake(cakeid, function(img){
                readWriteFile(img, image,function(address){
                    initPayment();
                    $('#cakeImage').prop('src', '../public/temp/out'+image+'.jpg');
                    image++;
                });
            });
        });
    });
    $(".mymodal").on('change', '#theme',function(){
        var themeid = $("#theme").val()[$("#theme").val().length-1];
        deleteImages(function(){
            getImageTheme(themeid, function(img){
                readWriteFile(img, image,function(address){
                    initPayment();
                    $('#themeImage').prop('src', '../public/temp/out'+image+'.jpg');
                    image++;
                });
            });
        });
    });
    $(".mymodal").on('click', '#rentAttireButton', function(){
        $("#rentattireModal").modal("open");
    });
    $(".mymodal").on('click', '#loadInventoryItemsHere .additem', function(){
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        item.quantity = product[1];
        item.price = product[2];
        item.itemcode2 = product[0];
        item.itemcode = $(this).prop("id").split("-")[1];
        if(item.quantity == 0){
            alert("This item is already 0 quantity!")
            return;
        }
        var myindex = $(this).prop("id").split("-")[1];
        itemsSelected = [];
        $('#addItemsHere > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            itemsSelected[$(this).prop("id").split("-")[1]]={
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            };
            //console.log(itemsSelected);
        });
        var html = "";
        if(itemsSelected.length==0){
            //console.log(item.itemcode2);
            html += "<tr id = 'addeditem-"+item.itemcode+"'><td>"+item.itemcode2+"</td>"+
            "<td>"+1+"</td>"+
            "<td>"+item.price+"</td>"+
            "<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+item.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
            +"</tr>";
            itemsSelected.push({
                quantity:1,
                price:item.price,
                itemcode:item.itemcode,
                itemcode2:item.itemcode2
            });
            //myindex=-1;
            $("#addItemsHere").prepend(html);
            html = "<td>"+item.itemcode2+"</td>"+
            "<td>"+(item.quantity-1)+"</td>"+
            "<td>"+item.price+"</td>"+
            "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
            $("#item-"+item.itemcode).html(html);
            html = "";
        }else{
 
            html = "<td>"+item.itemcode2+"</td>"+
             "<td>"+(item.quantity-1)+"</td>"+
             "<td>"+item.price+"</td>"+
             "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
             $("#item-"+item.itemcode).html(html);
            //myindex=-1;
            html ="";
             if(typeof itemsSelected[myindex] === 'undefined'){
                 console.log(itemsSelected[myindex]);
                 console.log(myindex);
                 html += "<tr id = 'addeditem-"+myindex+"'>";
                 html += "<td>"+item.itemcode2+"</td>";
                 html += "<td>"+1+"</td>";
                 html += "<td>"+item.price+"</td>";
                 html += "<td><a class='waves-effect waves-light btn removeitem' id = 'additem-"+myindex+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
                 html += "</tr>";
                 $("#addItemsHere").prepend(html);
             }else{
 
                html += "<td>"+item.itemcode2+"</td>";
                html += "<td>"+(itemsSelected[myindex].quantity = parseFloat(itemsSelected[myindex].quantity)+parseFloat(1))+"</td>";
                html += "<td>"+(item.price*itemsSelected[myindex].quantity)+"</td>";
                html += "<td><a class='waves-effect waves-light btn removeitem' id = 'additem-"+myindex+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
 
                $("#addeditem-"+myindex).html(html);
             }
        }
        initPayment();
    });
    $(".mymodal").on('click', '#addItemsHere .removeitem',function(){
        //alert("Hello");
        var i = 0;
        var myproduct = [""];
        var product = [""];
        var items = [];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                myproduct[i]=$(this).html();
                i++;
            });
        });
        myid = $(this).prop("id").split("-")[1];
        $('#loadInventoryItemsHere > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            items.push({
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            });
        });
        //console.log(myid);
        if(myproduct[1]==1){
            $("#addeditem-"+myid).remove();
            itemsSelected.splice(myid, 1);
            console.log(itemsSelected);
        }
        items.forEach(function(row, index){
            if(row.itemcode == myid){
                    var html = "<td>"+row.itemcode2+"</td>";
                    html += "<td>"+(row.quantity= parseFloat(row.quantity)+parseFloat(1))+"</td>";
                    html += "<td>"+row.price+"</td>";
                    html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
                $("#item-"+row.itemcode).html(html);
            }
        });
        getItemPrice(connection, myid, function(price){
            html = "<td>"+myproduct[0]+"</td>";
            html += "<td>"+(myproduct[1]= parseFloat(myproduct[1])-parseFloat(1))+"</td>";
            html += "<td>"+(parseFloat(myproduct[2])-parseFloat(price))+"</td>";
            html += "<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+myid+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
            $("#addeditem-"+myid).html(html);
        });
       
        initPayment();
    });
    $(".mymodal").on('click', '#loadInventoryItemsHere .viewImage', function(){
		$("#imagemodal").modal("open");
		var rowid = $(this).prop("id").split("-")[1];
        //$('#inventoryModal').modal("open");
		deleteImages(function(){
			getImageInventory(rowid, function(img){
				console.log(myimage);
				readWriteFile(img, image,function(address){
					$('#itemImageModal').prop('src', '../public/temp/out'+image+'.jpg');
					image++;
				});
			});
		});
	}); 
    $(".mymodal").on('click', '#tailorAttireButton', function(){
        $("#tailorattireModal").modal("open");
    });
    $(".mymodal").on('click', '#putTailorMaleHere .removeMale', function(){
        var mymaletailorid = $(this).prop("id").split("-")[1];
        $("#tailorMale-"+mymaletailorid).remove();
        initPayment();
    });
    $(".mymodal").on('click', '#putTailorFemaleHere .removeFemale', function(){
        var myfemaletailorid = $(this).prop("id").split("-")[1];
        $("#tailorFemale-"+myfemaletailorid).remove();
        initPayment();
    });
    $(".mymodal").on('click', '#putTailorMaleHere .removeAddedMale', function(){
        var mymaletailorid = $(this).prop("id").split("-")[1];
        $("#addedMale-"+mymaletailorid).remove();
        initPayment();
    });
    $(".mymodal").on('click', '#putTailorFemaleHere .removeAddedFemale', function(){
        var myfemaletailorid = $(this).prop("id").split("-")[1];
        $("#addedFemale-"+myfemaletailorid).remove();
        initPayment();
    });
    $(".mymodal").on('click', '#putTailorMaleHere .editAddedMale', function(){
        edittailormale = true;
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        editmaletailorid = $(this).prop("id");
        var maleimage = $("#addedMale-"+$(this).prop("id").split("-")[1]).data("value");
        $("#neckMale").val(product[2]);
        $("#chest").val(product[3]);
        $("#waistMale").val(product[4]);
        $("#seatMale").val(product[5]);
        $("#hipMale").val(product[6]);
        $("#sleevesMale").val(product[9]);
        $("#backlengthMale").val(product[10]);
        $("#inseamMale").val(product[11]);
        $("#outseamMale").val(product[12]);
        $("#hemMale").val(product[13]);
        $("#priceMale").val(product[15]);
        $("#nameMale").val(product[0]);
        $("#typeMale").val(product[16]);
        $("#typeMale").material_select();
        $("#fabricMale").val(product[14]);
        $("#fabricMale").material_select();
        $("#shoulderMale").val(product[7]);
        $("#wristMale").val(product[8]);
        $("#maleTailorImage").prop("src", maleimage);
        $("#measurementMaleModal").modal("open");
    });
    $(".mymodal").on('click', '#putTailorMaleHere .editMale', function(){
        edittailormale = true;
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        editmaletailorid = $(this).prop("id");
        var maleimage = $("#tailorMale-"+$(this).prop("id").split("-")[1]).data("value");
        $("#neckMale").val(product[2]);
        $("#chest").val(product[3]);
        $("#waistMale").val(product[4]);
        $("#seatMale").val(product[5]);
        $("#hipMale").val(product[6]);
        $("#sleevesMale").val(product[9]);
        $("#backlengthMale").val(product[10]);
        $("#inseamMale").val(product[11]);
        $("#outseamMale").val(product[12]);
        $("#hemMale").val(product[13]);
        $("#priceMale").val(product[15]);
        $("#nameMale").val(product[0]);
        $("#typeMale").val(product[16]);
        $("#typeMale").material_select();
        $("#fabricMale").val(product[14]);
        $("#fabricMale").material_select();
        $("#shoulderMale").val(product[7]);
        $("#wristMale").val(product[8]);
        //alert(product[16]+" "+product[14]);
        //$("#maleTailorImage").prop("src", maleimage);
        femaletailorid = $(this).prop("id").split("-")[1];
        $("#measurementMaleModal").modal("open");
        setTimeout(function(){
            $('#maleTailorImage').prop('src', '../public/temp/out'+(myimage-1)+'.jpg');
        }, 200);
 
 
    });
    $(".mymodal").on('click', '#putTailorFemaleHere .editAddedFemale', function(){
        edittailorfemale=true;
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        editfemaletailorid = $(this).prop("id");
        var femaleimage = $("#addedFemale-"+$(this).prop("id").split("-")[1]).data("value");
        $("#nameFemale").val(product[0]);
        $("#typeFemale").val(product[11]);
        $("#typeFemale").material_select();
        $("#fabricFemale").val(product[10]);
        $("#fabricFemale").material_select();
        $("#chestFemale").val(product[2]);
        $("#bustFemale").val(product[3]);
        $("#hipFemale").val(product[4]);
        $("#waistFemale").val(product[5]);
        $("#wristFemale").val(product[6]);
        $("#shoulderFemale").val(product[7]);
        $("#frontskirtFemale").val(product[8]);
        $("#backskirtFemale").val(product[9]);
        $("#priceFemale").val(product[12]);
        $("#femaleTailorImage").prop("src", femaleimage);
        $("#measurementFemaleModal").modal("open");
    });
    $(".mymodal").on('click', '#putTailorFemaleHere .editFemale', function(){
        edittailorfemale=true;
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        //var femaleimage = $("#tailorFemale-"+$(this).prop("id").split("-")[1]).data("value");
        editfemaletailorid = $(this).prop("id");
        $("#nameFemale").val(product[0]);
        $("#typeFemale").val(product[11]);
        $("#typeFemale").material_select();
        $("#fabricFemale").val(product[10]);
        $("#fabricFemale").material_select();
        $("#chestFemale").val(product[2]);
        $("#bustFemale").val(product[3]);
        $("#hipFemale").val(product[4]);
        $("#waistFemale").val(product[5]);
        $("#wristFemale").val(product[6]);
        $("#shoulderFemale").val(product[7]);
        $("#frontskirtFemale").val(product[8]);
        $("#backskirtFemale").val(product[9]);
        $("#priceFemale").val(product[12]);
        //$("#femaleTailorImage").prop("src", femaleimage);
        var femaletailorid = $(this).prop("id").split("-")[1];
 
        setTimeout(function(){
            $('#femaleTailorImage').prop('src', '../public/temp/out'+(myimage-1)+'.jpg');
        }, 200);
        $("#measurementFemaleModal").modal("open");
    });
    $(".mymodal").on('click', '#putTailorFemaleHere .rowTailorFemale', function(){
        //console.log($(this).data("value"));
        femaletailorid = $(this).prop("id").split("-")[1];
        if($(this).data("value")===undefined){
            deleteImages(function(){
                getImageFemaleTailor(femaletailorid, function(img){
                    readWriteFile(img, myimage,function(address){
                        $('#ImageViewTailor').prop('src', '../public/temp/out'+myimage+'.jpg');
                        myimage++;
                    });
                });
            });
        }else{
            if($(this).data("value").includes("base64")){
                $("#ImageViewTailor").prop("src", $("#addedFemale-"+femaletailorid).data("value"));
            }else{
                deleteImages(function(){
                    getImageFemaleTailor(femaletailorid, function(img){
                        readWriteFile(img, myimage,function(address){
                            $('#ImageViewTailor').prop('src', '../public/temp/out'+myimage+'.jpg');
                            myimage++;
                        });
                    });
                });
            }
        }
    });
    $(".mymodal").on('click', '#putTailorMaleHere tr', function(){
        //alert("Hello");
        femaletailorid = $(this).prop("id").split("-")[1];
        if($(this).data("value")===undefined){
            deleteImages(function(){
                getImageMaleTailor(femaletailorid, function(img){
                    readWriteFile(img, myimage,function(address){
                        $('#ImageViewTailor').prop('src', '../public/temp/out'+myimage+'.jpg');
                        myimage++;
                    });
                });
            });
        }else{
            if($(this).data("value").includes("base64")){
                $("#ImageViewTailor").prop("src", $("#addedMale-"+femaletailorid).data("value"));
            }else{
                deleteImages(function(){
                    getImageMaleTailor(femaletailorid, function(img){
                        readWriteFile(img, myimage,function(address){
                            $('#ImageViewTailor').prop('src', '../public/temp/out'+myimage+'.jpg');
                            myimage++;
                        });
                    });
                });
            }
        }
    });
    $(".mymodal").on('click', '#tailorAttireMale', function(){
        $("#measurementMaleModal").modal("open");
        edittailormale = false;
    });
    $(".mymodal").on('click', '#tailorAttireFemale', function(){
        $("#measurementFemaleModal").modal("open");
        edittailorfemale = false;
    });
    $(".mymodal").on('click', '#attireMaleModalButton', function(){
 
        if($("#typeMale").val()=="coat"){
            if($("#neckMale").val()== "" ||$("#waistMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
                    $("#backlengthMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
                $("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
                || $("#wristMale").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMale").val()=="slacks") {
            if($("#waistMale").val()== "" ||
                    $("#seatMale").val()== "" ||$("#hipMale").val()== "" ||$("#inseamMale").val()== "" ||$("#outseamMale").val()== "" ||
                $("#hemMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
                $("#typeMale").val()== "" ||$("#fabricMale").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMale").val()=="polo") {
            if($("#neckMale").val()== "" ||$("#waistMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
                    $("#backlengthMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
                $("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
                || $("#wristMale").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMale").val()=="vest") {
            if($("#neckMale").val()== "" ||$("#waistMale").val()== ""  ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== ""
               ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
                $("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else{
            if($("#neckMale").val()== "" ||$("#chest").val()== "" ||$("#waistMale").val()== "" ||
                    $("#seatMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
                    $("#backlengthMale").val()== "" ||$("#inseamMale").val()== "" ||$("#outseamMale").val()== "" ||
                $("#hemMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
                $("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
                || $("#wristMale").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }
        maleTailor.push({
            neckMale:$("#neckMale").val(),
            chest:$("#chest").val(),
            waistMale:$("#waistMale").val(),
            seatMale:$("#seatMale").val(),
            hipMale:$("#hipMale").val(),
            sleevesMale:$("#sleevesMale").val(),
            backlengthMale:$("#backlengthMale").val(),
            inseamMale:$("#inseamMale").val(),
            outseamMale:$("#outseamMale").val(),
            hemMale:$("#hemMale").val(),
            priceMale:$("#priceMale").val(),
            nameMale:$("#nameMale").val(),
            typeMale:$("#typeMale").val(),
            fabricMale:$("#fabricMale").val(),
            shoulderMale:$("#shoulderMale").val(),
            wristMale:$("#wristMale").val(),
            maleTailorImage:$("#maleTailorImage").prop("src")
        });
        if(edittailormale ==false){
            if($("#imgMale").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
        var html = "<tr class='rowTailorMale' data-value = '"+$("#maleTailorImage").prop("src")+"' id = 'addedMale-"+addmaletailorid+"'>";
            html += "<td>"+$("#nameMale").val()+"</td>";
            html += "<td>Male</td>";
            html += "<td>"+$("#neckMale").val()+"</td>";
            html += "<td>"+$("#chest").val()+"</td>";
            html += "<td>"+$("#waistMale").val()+"</td>";
            html += "<td>"+$("#seatMale").val()+"</td>";
            html += "<td>"+$("#hipMale").val()+"</td>";
            html += "<td>"+$("#shoulderMale").val()+"</td>";
            html += "<td>"+$("#wristMale").val()+"</td>";
            html += "<td>"+$("#sleevesMale").val()+"</td>";
            html += "<td>"+$("#backlengthMale").val()+"</td>";
            html += "<td>"+$("#inseamMale").val()+"</td>";
            html += "<td>"+$("#outseamMale").val()+"</td>";
            html += "<td>"+$("#hemMale").val()+"</td>";
            html += "<td>"+$("#fabricMale").val()+"</td>";
            html += "<td>"+$("#priceMale").val()+"</td>";
            html += "<td>"+$("#typeMale").val()+"</td>";
            html += "<td><a class='waves-effect waves-light btn editAddedMale' id = 'editAddedMale-"+addmaletailorid+"'><i class='material-icons'>edit</i></a></td>";
            html += "<td><a class='waves-effect waves-light btn removeAddedMale' id = 'removeAddedMale-"+addmaletailorid+"'><i class='material-icons'>clear</i></a></td>";
            html += "</tr>";
            addmaletailorid++;
            $("#putTailorMaleHere").append(html);
        }else{
            edittailormale =false;
            if(editmaletailorid.split("-")[0] == "editAddedMale"){
                var html = "<td>"+$("#nameMale").val()+"</td>";
                    html += "<td>Male</td>";
                    html += "<td>"+$("#neckMale").val()+"</td>";
                    html += "<td>"+$("#chest").val()+"</td>";
                    html += "<td>"+$("#waistMale").val()+"</td>";
                    html += "<td>"+$("#seatMale").val()+"</td>";
                    html += "<td>"+$("#hipMale").val()+"</td>";
                    html += "<td>"+$("#shoulderMale").val()+"</td>";
                    html += "<td>"+$("#wristMale").val()+"</td>";
                    html += "<td>"+$("#sleevesMale").val()+"</td>";
                    html += "<td>"+$("#backlengthMale").val()+"</td>";
                    html += "<td>"+$("#inseamMale").val()+"</td>";
                    html += "<td>"+$("#outseamMale").val()+"</td>";
                    html += "<td>"+$("#hemMale").val()+"</td>";
                    html += "<td>"+$("#fabricMale").val()+"</td>";
                    html += "<td>"+$("#priceMale").val()+"</td>";
                    html += "<td>"+$("#typeMale").val()+"</td>";
                    html += "<td><a class='waves-effect waves-light btn editAddedMale' id = 'editAddedMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>edit</i></a></td>";
                    html += "<td><a class='waves-effect waves-light btn removeAddedMale' id = 'removeAddedMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                  
                    $("#addedMale-"+editmaletailorid.split("-")[1]).html(html);
                    if( document.getElementById("imgMale").files.length > 0 ){
                        $("#addedMale-"+editmaletailorid.split("-")[1]).data("value", $("#maleTailorImage").prop("src"));
                    }
            }else{
                var html = "<td>"+$("#nameMale").val()+"</td>";
                    html += "<td>Male</td>";
                    html += "<td>"+$("#neckMale").val()+"</td>";
                    html += "<td>"+$("#chest").val()+"</td>";
                    html += "<td>"+$("#waistMale").val()+"</td>";
                    html += "<td>"+$("#seatMale").val()+"</td>";
                    html += "<td>"+$("#hipMale").val()+"</td>";
                    html += "<td>"+$("#shoulderMale").val()+"</td>";
                    html += "<td>"+$("#wristMale").val()+"</td>";
                    html += "<td>"+$("#sleevesMale").val()+"</td>";
                    html += "<td>"+$("#backlengthMale").val()+"</td>";
                    html += "<td>"+$("#inseamMale").val()+"</td>";
                    html += "<td>"+$("#outseamMale").val()+"</td>";
                    html += "<td>"+$("#hemMale").val()+"</td>";
                    html += "<td>"+$("#fabricMale").val()+"</td>";
                    html += "<td>"+$("#priceMale").val()+"</td>";
                    html += "<td>"+$("#typeMale").val()+"</td>";
                    html += "<td><a class='waves-effect waves-light btn editMale' id = 'editMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>edit</i></a></td>";
                    html += "<td><a class='waves-effect waves-light btn removeMale' id = 'removeMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                    
                    $("#tailorMale-"+editmaletailorid.split("-")[1]).html(html);
                    //$("#tailorMale-"+editmaletailorid.split("-")[1]).data("value", $("#maleTailorImage").prop("src"));
                    if( document.getElementById("imgMale").files.length > 0 ){
                        $("#tailorMale-"+editmaletailorid.split("-")[1]).data("value", $("#maleTailorImage").prop("src"));
                    }
            }
        }
        initPayment();
    });
    $(".mymodal").on('change', '#imgMale', function(){
        if(this.files[0].size>5000000){
            alert("Image size must not be greater than 5mb.");
            $("#imgMale").val("");
            return;
        }
        readURLTailorMale(this);
    });
    $(".mymodal").on('change', '#imgFemale', function(){
        if(this.files[0].size>5000000){
            alert("Image size must not be greater than 5mb.");
            $("#imgFemale").val("");
            return;
        }
        readURLTailorFemale(this);
    });
    $(".mymodal").on('click', '#attireFemaleModalButton', function(){
        if($("#nameFemale").val()== "" ||$("#typeFemale").val()== "" ||$("#fabricFemale").val()== "" ||
            $("#chestFemale").val()== "" ||$("#bustFemale").val()== "" ||$("#hipFemale").val()== "" ||
            $("#waistFemale").val()== ""  ||$("#shoulderFemale").val()== "" ||
            $("#frontskirtFemale").val()== "" ||$("#backskirtFemale").val()== "" ||$("#priceFemale").val()== ""
          || $("imgFemale").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
        femaleTailor.push({
                nameFemale: $("#nameFemale").val(),
                typeFemale: $("#typeFemale").val(),
                fabricFemale: $("#fabricFemale").val(),
                chestFemale: $("#chestFemale").val(),
                bustFemale: $("#bustFemale").val(),
                hipFemale: $("#hipFemale").val(),
                waistFemale: $("#waistFemale").val(),
                wristFemale: $("#wristFemale").val(),
                shoulderFemale: $("#shoulderFemale").val(),
                frontskirtFemale: $("#frontskirtFemale").val(),
                backskirtFemale: $("#backskirtFemale").val(),
                priceFemale: $("#priceFemale").val(),
                imgFemale:$("#femaleTailorImage").prop("src")
        });
        if(edittailorfemale==false){
            if($("#imgMale").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
            var html = "<tr id = 'addedFemale-"+addedfemaletailorid+"' class='rowTailorFemale' data-value = '"+$("#femaleTailorImage").prop("src")+"'>";
                html += "<td>"+$("#nameFemale").val()+"</td>";
                html += "<td>Female</td>";
                html += "<td>"+$("#chestFemale").val()+"</td>";
                html += "<td>"+$("#bustFemale").val()+"</td>";
                html += "<td>"+$("#hipFemale").val()+"</td>";
                html += "<td>"+$("#waistFemale").val()+"</td>";
                html += "<td>"+$("#wristFemale").val()+"</td>";
                html += "<td>"+$("#shoulderFemale").val()+"</td>";
                html += "<td>"+$("#frontskirtFemale").val()+"</td>";
                html += "<td>"+$("#backskirtFemale").val()+"</td>";
                html += "<td>"+$("#fabricFemale").val()+"</td>";
                html += "<td>"+$("#typeFemale").val()+"</td>";
                html += "<td>"+$("#priceFemale").val()+"</td>";
                html += "<td><a class='waves-effect waves-light btn editAddedFemale' id = 'editAddedFemale-"+addedfemaletailorid+"'><i class='material-icons'>clear</i></a></td>";
                html += "<td><a class='waves-effect waves-light btn removeAddedFemale' id = 'removeAddedFemale-"+addedfemaletailorid+"'><i class='material-icons'>clear</i></a></td>";
                
                addedfemaletailorid++;
                html += "</tr>";
                $("#putTailorFemaleHere").append(html);
        }else{
            edittailorfemale=false;
            //var html = "<tr id = 'addedFemale-"+addedfemaletailorid+"' class='rowTailorFemale' data-value = '"+$("#femaleTailorImage").prop("src")+"'>";
            if(editfemaletailorid.split("-")[0]== "editAddedFemale"){
                var html = "<td>"+$("#nameFemale").val()+"</td>";
                html += "<td>Female</td>";
                html += "<td>"+$("#chestFemale").val()+"</td>";
                html += "<td>"+$("#bustFemale").val()+"</td>";
                html += "<td>"+$("#hipFemale").val()+"</td>";
                html += "<td>"+$("#waistFemale").val()+"</td>";
                html += "<td>"+$("#wristFemale").val()+"</td>";
                html += "<td>"+$("#shoulderFemale").val()+"</td>";
                html += "<td>"+$("#frontskirtFemale").val()+"</td>";
                html += "<td>"+$("#backskirtFemale").val()+"</td>";
                html += "<td>"+$("#fabricFemale").val()+"</td>";
                html += "<td>"+$("#typeFemale").val()+"</td>";
                html += "<td>"+$("#priceFemale").val()+"</td>";
                html += "<td><a class='waves-effect waves-light btn editAddedFemale' id = 'editAddedFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                html += "<td><a class='waves-effect waves-light btn removeAddedFemale' id = 'removeAddedFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                
                $("#addedFemale-"+editfemaletailorid.split("-")[1]).html(html);
                if( document.getElementById("imgFemale").files.length > 0 ){
                    $("#addedFemale-"+editfemaletailorid.split("-")[1]).data("value", $("#femaleTailorImage").prop("src"));
                }
            }else{
                var html = "<td>"+$("#nameFemale").val()+"</td>";
                html += "<td>Female</td>";
                html += "<td>"+$("#chestFemale").val()+"</td>";
                html += "<td>"+$("#bustFemale").val()+"</td>";
                html += "<td>"+$("#hipFemale").val()+"</td>";
                html += "<td>"+$("#waistFemale").val()+"</td>";
                html += "<td>"+$("#wristFemale").val()+"</td>";
                html += "<td>"+$("#shoulderFemale").val()+"</td>";
                html += "<td>"+$("#frontskirtFemale").val()+"</td>";
                html += "<td>"+$("#backskirtFemale").val()+"</td>";
                html += "<td>"+$("#fabricFemale").val()+"</td>";
                html += "<td>"+$("#typeFemale").val()+"</td>";
                html += "<td>"+$("#priceFemale").val()+"</td>";
                html += "<td><a class='waves-effect waves-light btn editFemale' id = 'editFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                html += "<td><a class='waves-effect waves-light btn removeFemale' id = 'removeFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
                
                $("#tailorFemale-"+editfemaletailorid.split("-")[1]).html(html);
                if( document.getElementById("imgFemale").files.length > 0 ){
                    $("#tailorFemale-"+editfemaletailorid.split("-")[1]).data("value", $("#femaleTailorImage").prop("src"));
                }
            }
        }
        initPayment();
    });
    $(".mymodal").on('click', '.addToInventoryFemale', function(){
        //$("#addItemToInventoryModal").modal("open")
    });
    $(".mymodal").on('click', '.addToInventoryMale', function(){
        
    });
    $(".mymodal").on('click', '#transacUpdateModalButton', function(){
        var itemsrented = [];
        $('#addItemsHere > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            itemsrented.push({
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            });
            //console.log(itemsrented.length);
        });
        //console.log(itemsrented.length);
        //return;
        var measurementMale = [];
        $('.mymodal #putTailorMaleHere > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            measurementMale.push({
                name:product[0],
                category:product[1],
                neck:product[2],
                chest:product[3],
                waist:product[4],
                seat:product[5],
                hip:product[6],
                shoulder:product[7],
                wrist:product[8],
                sleeves:product[9],
                backlength:product[10],
                inseam:product[11],
                outseam:product[12],
                hem:product[13],
                fabric:product[14],
                price:product[15],
                type:product[16],
                img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
            });
        });
        var measurementFemale = [];
        $('.mymodal #putTailorFemaleHere > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            measurementFemale.push({
                name:product[0],
                category:product[1],
                chest:product[2],
                bust:product[3],
                hip:product[4],
                waist:product[5],
                wrist:product[6],
                shoulder:product[7],
                frontskirt:product[8],
                backskirt:product[9],
                fabric:product[10],
                type:product[11],
                price:product[12],
                img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
            });
        });
        var eventDate = $("#eventDate").val();
        var eventTime = $("#eventTime").val();
        var rentStart = $('#rentstartdate').val();
        var rentEnd = $('#rentenddate').val();
        var totalpayment = $("#totalPayment").val();
        var downpayment = $("#downpayment").val();
        var theme = $("#theme").val();
        var cake = $("#cake").val();
        var eventTimeStart = $("#eventTimeStart").val();

        var photographer = $("#photographer").val();
        
        var photographerStartDate = formatDate($("#photoStartDate").val());
		var photographerEndDate = formatDate($("#photoEndDate").val());
		var photographerStartTime = $("#photoStartTime").val();
		var photographerEndTime = $("#photoEndTime").val();
		var photochecker = false;
		if(photographer != "--Photographer--"){
			if(photographerStartDate=="" ||photographerEndDate == ""
				|| photographerStartTime == "" || photographerEndTime == ""){
					alert("Please fill up all the necessary fields for photographer!");
					return;
				}else{
					var d1 = Date.parse(photographerStartDate);
					var d2 = Date.parse(photographerEndDate);
					if (d1 > d2) {
						alert ("Invalid dates for photographer!");
						return;
					}
				}
		}

        var stylist = $("#stylist").val();

        var stylistStartDate = formatDate($("#stylistStartDate").val());
		var stylistEndDate = formatDate($("#stylistEndDate").val());
		var stylistStartTime= $("#stylistStartTime").val();
		var stylistEndTime= $("#stylistEndTime").val();
		if(stylist != "--Stylist--"){
			if(stylistStartDate == "" || stylistEndDate == "" 
				|| stylistStartTime == "" || stylistEndTime == ""){
					alert("Please fill up all the necessary fields for stylist!");
					return;
				}else{
					var d1 = Date.parse(stylistStartDate);
					var d2 = Date.parse(stylistEndDate);
					if (d1 > d2) {
						alert ("Invalid dates for stylist!");
						return;
					}
				}
		}
		
        var deposit = $("#deposit").val();
        var eventdatestart = $("#eventDateStart").val();
        var eventdateend = $("#eventDateEnd").val();
        //console.log(itemsrented.length);
        if(itemsrented.length>0){
            if(deposit==""){
                alert("Deposit is required when renting an item!");
                return;
            }
        }else{
            if(parseFloat(deposit)>parseFloat(0) || isNaN(deposit)){
               
                alert("Deposit is only for renting items!");
                return;
            }
        }
        if((theme == '--Theme--' || theme == "") //&& (measurementFemale.length == 0 || measurementMale.length == 0)
            && (itemsrented.length == 0)){
                alert("Atleast a theme, tailor, or inventory must be in a transaction!");
                return;
            }
        if(eventDate == "" || eventTime == "" || totalPayment == "" || downpayment == "" ||
            eventdatestart == "" || eventdateend == "" || eventTimeStart == ""){
            alert("Please provide all the necessary details!");
            return;
        }
        var items = {
            transid:transid,
            locationid:locationid,
            eventdate:formatDate(eventDate),
            eventTime:getTime(eventTime.slice(0,5), eventTime.slice(-2)),
            rentStart:formatDate(rentStart),
            rentEnd:formatDate(rentEnd),
            totalpayment:totalpayment,
            theme:theme,
            cake:cake,
            photographer:{
				photographer:photographer,
				photoStartDate:photographerStartDate,
				photoEndDate: photographerEndDate,
				photoStartTime: getTime(photographerStartTime.slice(0,5), photographerStartTime.slice(-2)),
				photoEndTime:getTime(photographerEndTime.slice(0,5), photographerEndTime.slice(-2))
			},
			stylist:{
			stylist:stylist,
				stylistStartDate:stylistStartDate,
				stylistEndDate:stylistEndDate,
				stylistStartTime:getTime(stylistStartTime.slice(0,5), stylistStartTime.slice(-2)),
				stylistEndTime:getTime(stylistEndTime.slice(0,5), stylistEndTime.slice(-2))
				},
            itemsrented:itemsrented,
            measurementMale:measurementMale,
            measurementFemale:measurementFemale,
            deposit:deposit,
            eventdatestart:formatDate(eventdatestart),
            eventdateend:formatDate(eventdateend),
            downpayment:downpayment,
            eventTimeStart:getTime(eventTimeStart.slice(0,5), eventTimeStart.slice(-2))
        }
        connectMysql2(function(connection){
            new Promise((resolve, reject)=>{
                checkifmaxtransactsreached(connection, items.eventdatestart, items.eventdateend, function(check){
                    if(check){
                        reject("max3days");
                    }else{
                        resolve(true);
                    }
                });
            }).then((res)=>{
                return new Promise((resolve, reject)=>{
                    checkifmaxtransactsreachedononelocation(connection,  items.eventdatestart, items.eventdateend, items.locationid, function(check){
                        if(check){
                            reject("conflictlocation");
                        }else{
                            resolve(res);
                        }
                    });
                });
            }).then((res)=>{
                //connectMysql2(function(connection){
                    updateTransaction(connection, items);
                //});
            }).catch((err)=>{
                if(err== "conflictlocation"){
                    transacUpdate = items;
                    $("#confirmConflictModal").modal("open")
                    $("#confirmConflictTitle").text("Conflict of event on same date and same place!");
                    conflict = "update";
                }else if(err =="max3days"){
                    transacUpdate = items;
                    $("#confirmConflictModal").modal("open")
                    $("#confirmConflictTitle").text("There should only be max of 3 events on a day!");
                    conflict = "update";
                }
            });    
        });
      
    });
    $(".mymodal").on('click', '#confirmConflictSave', function(){
        if(conflict == "update"){
            connectMysql2(function(connection1){
                updateTransaction(connection1, transacUpdate);
            });
        }else{
            connectMysql2(function(connection){  
                insertTransaction(connection, transacInsert, function(connection){
                        selectedLocation(connection, transid, function(){
                        });
                });
            });
                
        }
	});
    $(".mymodal").on('click', '#addLocationLink', function(){
        $("#removeLocationModal").modal("open");
        connectMysql2(function(connection){
            loadLocationForAdd(connection, transid);
            loadLocation(connection);
        });
    });
    $(".mymodal").on('click', '.addlocation', function(){
        var select = $("#selectedLocation").find("option:selected");
        mylocationid = $(this).prop("id").split("-")[1];
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        connectMysql2(function(connection){
            myloadCake(connection);
            myloadTheme(connection);
            loadLocation(connection);
            myloadStylist(connection);
            myloadPhotographer(connection);
            loadPackagesTrans(connection);
           
        });
        $("#removeLocationModal").modal("close");
        $("#addTransactionModal").modal("open");
        $("#editUserTitleTrans").text("Location:"+product[0]);
    });
    $(".mymodal").on('click', '#rentAttireButtonTrans', function(){
        $("#rentattireModalTrans").modal("open");
         connectMysql2(function(connection){
             getMaxRows(function(rows){
                if(rows<=5){
                    visiblepages=1;
                }else {
                    visiblepages=5;
                }
                if(rows%5!=0){
                    totalpages=Math.floor((rows/5))+1;
                }else{
                    totalpages=rows/5;
                }
                if(rows==0){
                    $("#loadInventoryItemsHereTrans").html("");
                    return;
                }
                $('#inventoryPaginationTrans').twbsPagination({
                                totalPages: totalpages,
                                visiblePages: visiblepages,
                                onPageClick: function (event, page){
                                    inventoryPage = (5*(page-1));
                                        loadInventoryTableTrans(connection, inventoryPage);
                                }
                 });
            }, "tblinventory", "");
         });
    });
    $(".mymodal").on('click', '#tailorAttireButtonTrans', function(){
        $("#tailorattireModalTrans").modal("open");
    });
    $(".mymodal").on('click', '#tailorAttireMaleTrans', function(){
        $("#measurementMaleModalTrans").modal("open");
    });
    $(".mymodal").on("click", '#tailorAttireFemaleTrans', function(){
        $("#measurementFemaleModalTrans").modal("open");
    });
    $(".mymodal").on('change', '#cakeTrans', function(){
        var cakeid = $("#cakeTrans").val()[$("#cakeTrans").val().length-1];
        deleteImages(function(){
            getImageCake(cakeid, function(img){
                readWriteFile(img, image,function(address){
                    initPaymentTrans();
                    $('#cakeImageTrans').prop('src', '../public/temp/out'+image+'.jpg');
                    image++;
                });
            });
        });
    });
    $(".mymodal").on('change', '#themeTrans',function(){
        var themeid = $("#themeTrans").val()[$("#themeTrans").val().length-1];
        deleteImages(function(){
            getImageTheme(themeid, function(img){
                readWriteFile(img, image,function(address){
                    initPaymentTrans();
                    $('#themeImageTrans').prop('src', '../public/temp/out'+image+'.jpg');
                    image++;
                });
            });
        });
    });
    $(".mymodal").on('click', '#loadInventoryItemsHereTrans .additemtrans', function(){
        var i = 0;
        var product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        item.quantity = product[1];
        item.price = product[2];
        item.itemcode2 = product[0];
        item.itemcode = $(this).prop("id").split("-")[1];
        if(item.quantity == 0){
            alert("This item is already 0 quantity!")
            return;
        }
        var myindex = $(this).prop("id").split("-")[1];
        itemsSelected = [];
        $('#addItemsHereTrans > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            itemsSelected[$(this).prop("id").split("-")[1]]={
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            };
            //console.log(itemsSelected);
        });
        var html = "";
        if(itemsSelected.length==0){
            //console.log(item.itemcode2);
            html += "<tr id = 'addeditemTrans-"+item.itemcode+"'><td>"+item.itemcode2+"</td>"+
            "<td>"+1+"</td>"+
            "<td>"+item.price+"</td>"+
            "<td><a class='waves-effect waves-light btn removeitemtrans' id = 'removeitemtrans-"+item.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
            +"</tr>";
            itemsSelected.push({
                quantity:1,
                price:item.price,
                itemcode:item.itemcode,
                itemcode2:item.itemcode2
            });
            //myindex=-1;
            $("#addItemsHereTrans").prepend(html);
            html = "<td>"+item.itemcode2+"</td>"+
            "<td>"+(item.quantity-1)+"</td>"+
            "<td>"+item.price+"</td>"+
            "<td><a class='waves-effect waves-light btn additemtrans' id = 'additemtrans-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
            $("#itemtrans-"+item.itemcode).html(html);
            html = "";
        }else{
 
            html = "<td>"+item.itemcode2+"</td>"+
             "<td>"+(item.quantity-1)+"</td>"+
             "<td>"+item.price+"</td>"+
             "<td><a class='waves-effect waves-light btn additemtrans' id = 'additemtrans-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
             $("#itemtrans-"+item.itemcode).html(html);
            //myindex=-1;
            html ="";
             if(typeof itemsSelected[myindex] === 'undefined'){
                 console.log(itemsSelected[myindex]);
                 console.log(myindex);
                 html += "<tr id = 'addeditemTrans-"+myindex+"'>";
                 html += "<td>"+item.itemcode2+"</td>";
                 html += "<td>"+1+"</td>";
                 html += "<td>"+item.price+"</td>";
                 html += "<td><a class='waves-effect waves-light btn removeitemtrans' id = 'removeitemtrans-"+myindex+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
                 html += "</tr>";
                 $("#addItemsHereTrans").prepend(html);
             }else{
 
                html += "<td>"+item.itemcode2+"</td>";
                html += "<td>"+(itemsSelected[myindex].quantity = parseFloat(itemsSelected[myindex].quantity)+parseFloat(1))+"</td>";
                html += "<td>"+(item.price*itemsSelected[myindex].quantity)+"</td>";
                html += "<td><a class='waves-effect waves-light btn removeitemtrans' id = 'removeitemtrans-"+myindex+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
 
                $("#addeditemTrans-"+myindex).html(html);
             }
        }
        initPaymentTrans();
    });
    $(".mymodal").on('click', '#addItemsHereTrans .removeitemtrans',function(){
        var i = 0;
        var myproduct = [""];
        var product = [""];
        var items = [];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                myproduct[i]=$(this).html();
                i++;
            });
        });
        myid = $(this).prop("id").split("-")[1];
        $('#loadInventoryItemsHereTrans > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            items.push({
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            });
        });
        //console.log(myid);
        if(myproduct[1]==1){
            $("#addeditemTrans-"+myid).remove();
            itemsSelected.splice(myid, 1);
            console.log(itemsSelected);
        }
        
        items.forEach(function(row, index){
            if(row.itemcode == myid){
                var html = "<td>"+row.itemcode2+"</td>";
                html += "<td>"+(row.quantity= parseFloat(row.quantity)+parseFloat(1))+"</td>";
                html += "<td>"+row.price+"</td>";
                html += "<td><a class='waves-effect waves-light btn additemtrans' id = 'additemtrans-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
                $("#itemtrans-"+row.itemcode).html(html);
                myitemcode = row.itemcode;
                myitemcode2 = row.itemcode2;
                myprice = row.price;
            }
        });
        getItemPrice(connection, myid, function(price){
            html = "<td>"+myproduct[0]+"</td>";
            html += "<td>"+(myproduct[1]= parseFloat(myproduct[1])-parseFloat(1))+"</td>";
            html += "<td>"+(parseFloat(myproduct[2])-parseFloat(price))+"</td>";
            html += "<td><a class='waves-effect waves-light btn removeitemtrans' id = 'removeitemtrans-"+myid+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
            $("#addeditemTrans-"+myid).html(html);
        });
        
        initPaymentTrans();
    });
    $(".mymodal").on('click', '#attireMaleModalButtonTrans', function(){
       
        if($("#typeMaleTranse").val()=="coat"){
            //alert("Hello");
            if($("#neckMaleTrans").val()== "" ||$("#waistMaleTrans").val()== "" ||$("#hipMaleTrans").val()== "" ||$("#sleevesMaleTrans").val()== "" ||
                    $("#backlengthMaleTrans").val()== "" ||$("#priceMaleTrans").val()== "" ||$("#nameMaleTrans").val()== "" ||
                $("#typeMaleTrans").val()== "" ||$("#fabricMaleTrans").val()== "" || $("#shoulderMaleTrans").val()==""
                || $("#wristMaleTrans").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMaleTrans").val()=="slacks") {
            if($("#waistMaleTrans").val()== "" ||
                $("#seatMaleTrans").val()== "" ||$("#hipMaleTrans").val()== "" ||$("#inseamMaleTrans").val()== "" ||$("#outseamMaleTrans").val()== "" ||
                $("#hemMaleTrans").val()== "" ||$("#priceMaleTrans").val()== "" ||$("#nameMaleTrans").val()== "" ||
                $("#typeMaleTrans").val()== "" ||$("#fabricMaleTrans").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMaleTrans").val()=="polo") {
            if($("#neckMaleTrans").val()== "" ||$("#waistMaleTrans").val()== "" ||$("#hipMaleTrans").val()== "" ||$("#sleevesMaleTrans").val()== "" ||
                    $("#backlengthMaleTrans").val()== "" ||$("#priceMaleTrans").val()== "" ||$("#nameMaleTrans").val()== "" ||
                $("#typeMaleTrans").val()== "" ||$("#fabricMaleTrans").val()== "" || $("#shoulderMaleTrans").val()==""
                || $("#wristMaleTrans").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else if ($("#typeMaleTrans").val()=="vest") {
            if($("#neckMaleTrans").val()== "" ||$("#waistMaleTrans").val()== ""  ||$("#hipMaleTrans").val()== "" ||$("#sleevesMaleTrans").val()== ""
               ||$("#priceMaleTrans").val()== "" ||$("#nameMaleTrans").val()== "" ||
                $("#typeMaleTrans").val()== "" ||$("#fabricMaleTrans").val()== "" || $("#shoulderMaleTrans").val()==""){
                alert("Please provide all the necessary details!");
                return;
            }
        }else{
            //alert("Hi");
            if($("#neckMaleTrans").val()== "" ||$("#chestTrans").val()== "" ||$("#waistMaleTrans").val()== "" ||
                    $("#seatMaleTrans").val()== "" ||$("#hipMaleTrans").val()== "" ||$("#sleevesMaleTrans").val()== "" ||
                    $("#backlengthMaleTrans").val()== "" ||$("#inseamMaleTrans").val()== "" ||$("#outseamMaleTrans").val()== "" ||
                $("#hemMaleTrans").val()== "" ||$("#priceMaleTrans").val()== "" ||$("#nameMaleTrans").val()== "" ||
                $("#typeMaleTrans").val()== "" ||$("#fabricMaleTrans").val()== "" || $("#shoulderMaleTrans").val()==""
                || $("#wristMaleTrans").val() == ""){
                alert("Please provide all the necessary details!");
                return;
            }
        }
        maleTailor.push({
            neckMale:$("#neckMaleTrans").val(),
            chest:$("#chestTrans").val(),
            waistMale:$("#waistMaleTrans").val(),
            seatMale:$("#seatMaleTrans").val(),
            hipMale:$("#hipMaleTrans").val(),
            sleevesMale:$("#sleevesMaleTrans").val(),
            backlengthMale:$("#backlengthMaleTrans").val(),
            inseamMale:$("#inseamMaleTrans").val(),
            outseamMale:$("#outseamMaleTrans").val(),
            hemMale:$("#hemMaleTrans").val(),
            priceMale:$("#priceMaleTrans").val(),
            nameMale:$("#nameMaleTrans").val(),
            typeMale:$("#typeMaleTrans").val(),
            fabricMale:$("#fabricMaleTrans").val(),
            shoulderMale:$("#shoulderMaleTrans").val(),
            wristMale:$("#wristMaleTrans").val(),
            maleTailorImage:$("#maleTailorImageTrans").prop("src")
        });
 
        var html = "<tr class='rowTailorMaleTrans' data-value = '"+$("#maleTailorImageTrans").prop("src")+"' id = 'addedMaleTrans-"+addmaletailorid+"'>";
            html += "<td>"+$("#nameMaleTrans").val()+"</td>";
            html += "<td>Male</td>";
            html += "<td>"+$("#neckMaleTrans").val()+"</td>";
            html += "<td>"+$("#chestTrans").val()+"</td>";
            html += "<td>"+$("#waistMaleTrans").val()+"</td>";
            html += "<td>"+$("#seatMaleTrans").val()+"</td>";
            html += "<td>"+$("#hipMaleTrans").val()+"</td>";
            html += "<td>"+$("#shoulderMaleTrans").val()+"</td>";
            html += "<td>"+$("#wristMaleTrans").val()+"</td>";
            html += "<td>"+$("#sleevesMaleTrans").val()+"</td>";
            html += "<td>"+$("#backlengthMaleTrans").val()+"</td>";
            html += "<td>"+$("#inseamMaleTrans").val()+"</td>";
            html += "<td>"+$("#outseamMaleTrans").val()+"</td>";
            html += "<td>"+$("#hemMaleTrans").val()+"</td>";
            html += "<td>"+$("#fabricMaleTrans").val()+"</td>";
            html += "<td>"+$("#priceMaleTrans").val()+"</td>";
            html += "<td>"+$("#typeMaleTrans").val()+"</td>";
            html += "<td><a class='waves-effect waves-light btn removeMaleTrans' id = 'removeMaleTrans-"+addmaletailorid+"'><i class='material-icons'>clear</i></a></td>";
            html += "</tr>";
            addmaletailorid++;
            $("#putTailorMaleHereTrans").append(html);
            $("#measurementMaleModalTrans").modal("close");
            initPaymentTrans();
    });
    $(".mymodal").on('click', '#attireFemaleModalButtonTrans', function(){
        if($("#nameFemaleTrans").val()== "" ||$("#typeFemaleTrans").val()== "" ||$("#fabricFemaleTrans").val()== "" ||
            $("#chestFemaleTrans").val()== "" ||$("#bustFemaleTrans").val()== "" ||$("#hipFemaleTrans").val()== "" ||
            $("#waistFemaleTrans").val()== "" ||$("#shoulderFemaleTrans").val()== "" ||
            $("#frontskirtFemaleTrans").val()== "" ||$("#backskirtFemaleTrans").val()== "" ||$("#priceFemaleTrans").val()== ""
          || $("imgFemaleTrans").val()== ""){
                alert("Please provide all the necessary details!");
                return;
            }
        femaleTailor.push({
                nameFemale: $("#nameFemaleTrans").val(),
                typeFemale: $("#typeFemaleTrans").val(),
                fabricFemale: $("#fabricFemaleTrans").val(),
                chestFemale: $("#chestFemaleTrans").val(),
                bustFemale: $("#bustFemaleTrans").val(),
                hipFemale: $("#hipFemaleTrans").val(),
                waistFemale: $("#waistFemaleTrans").val(),
                wristFemale: $("#wristFemaleTrans").val(),
                shoulderFemale: $("#shoulderFemaleTrans").val(),
                frontskirtFemale: $("#frontskirtFemaleTrans").val(),
                priceFemale: $("#priceFemaleTrans").val(),
                backskirtFemale: $("#backskirtFemaleTrans").val(),
                imgFemale:$("#femaleTailorImageTrans").prop("src")
        });
        var html = "<tr id = 'addedFemaleTrans-"+addedfemaletailorid+"' class='rowTailorFemaleTrans' data-value = '"+$("#femaleTailorImageTrans").prop("src")+"'>";
            html += "<td>"+$("#nameFemaleTrans").val()+"</td>";
            html += "<td>Female</td>";
            html += "<td>"+$("#chestFemaleTrans").val()+"</td>";
            html += "<td>"+$("#bustFemaleTrans").val()+"</td>";
            html += "<td>"+$("#hipFemaleTrans").val()+"</td>";
            html += "<td>"+$("#waistFemaleTrans").val()+"</td>";
            html += "<td>"+$("#wristFemaleTrans").val()+"</td>";
            html += "<td>"+$("#shoulderFemaleTrans").val()+"</td>";
            html += "<td>"+$("#frontskirtFemaleTrans").val()+"</td>";
            html += "<td>"+$("#backskirtFemaleTrans").val()+"</td>";
            html += "<td>"+$("#fabricFemaleTrans").val()+"</td>";
            html += "<td>"+$("#typeFemaleTrans").val()+"</td>";
            html += "<td>"+$("#priceFemaleTrans").val()+"</td>";
            html += "<td><a class='waves-effect waves-light btn removeFemaleTrans' id = 'removeFemaleTrans-"+addedfemaletailorid+"'><i class='material-icons'>clear</i></a></td>";
            addedfemaletailorid++;
            html += "</tr>";
            $("#putTailorFemaleHereTrans").append(html);
            initPaymentTrans();
    });
    $(".mymodal").on('change', '#imgMaleTrans', function(){
        if(this.files[0].size>5000000){
            alert("Image size must not be greater than 5mb.");
            $("#imgMaleTrans").val("");
            return;
        }
        readURLTailorMaleTrans(this);
    });
    $(".mymodal").on('change', '#imgFemaleTrans', function(){
        if(this.files[0].size>5000000){
            alert("Image size must not be greater than 5mb.");
            $("#imgFemaleTrans").val("");
            return;
        }
        readURLTailorFemaleTrans(this);
    });
    $(".mymodal").on('click', '#putTailorMaleHereTrans .removeMaleTrans', function(){
        maletailorid = $(this).prop("id").split("-")[1];
        $("#tailorMaleTrans-"+maletailorid).remove();
        initPaymentTrans();
    });
    $(".mymodal").on('click', '#putTailorFemaleHereTrans .removeFemaleTrans', function(){
        femaletailorid = $(this).prop("id").split("-")[1];
        $("#tailorFemaleTrans-"+femaletailorid).remove();
        initPaymentTrans();
    });
    $(".mymodal").on('click', '#putTailorFemaleHereTrans .rowTailorFemaleTrans', function(){
        //console.log($(this).data("value"));
        //alert("Hello");
        femaletailorid = $(this).prop("id").split("-")[1];
 
        if($(this).data("value")===undefined){
            //alert("hi");
            deleteImages(function(){
                getImageFemaleTailor(femaletailorid, function(img){
                    readWriteFile(img, myimage,function(address){
                        $('#ImageViewTailorTrans').prop('src', '../public/temp/out'+myimage+'.jpg');
                        myimage++;
                    });
                });
            });
        }else{
            //alert("Hello");
            $("#ImageViewTailorTrans").prop("src", $("#addedFemaleTrans-"+femaletailorid).data("value"));
        }
    });
    $(".mymodal").on('click', '#putTailorMaleHereTrans tr', function(){
        //console.log($(this).data("value"));
        femaletailorid = $(this).prop("id").split("-")[1];
        if($(this).data("value")===undefined){
            deleteImages(function(){
                getImageFemaleTailor(femaletailorid, function(img){
                    readWriteFile(img, myimage,function(address){
                        $('#ImageViewTailorTrans').prop('src', '../public/temp/out'+myimage+'.jpg');
                        myimage++;
                    });
                });
            });
        }else{
            $("#ImageViewTailorTrans").prop("src", $("#addedMaleTrans-"+femaletailorid).data("value"));
        }
    });
    $(".mymodal").on('click', '#transacUpdateModalButtonTrans', function(){
 
        //var customerName = $("#customerName").val();
        var location = mylocationid;
        var totalpayment = $("#totalPaymentTrans").val();
        var deposit = $("#depositTrans").val();
        var eventdate = formatDate($("#eventDateTrans").val());
        var eventtime = getTime($("#eventTimeTrans").val().slice(0,5), $("#eventTimeTrans").val().slice(-2));
        var eventdatestart = formatDate($("#eventDateStartTrans").val());
        var eventdateend = formatDate($("#eventDateEndTrans").val());
        var rentStart = formatDate($('#rentstartdate').val());
        var rentEnd = formatDate($('#rentenddate').val());
        var downpayment = $("#downpaymentTrans").val();
        var theme = $("#themeTrans").val();
        var cake = $("#cakeTrans").val();

        var photographer = $("#photographerTrans").val();
        
        var photographerStartDate = formatDate($("#photoStartDateTrans").val());
		var photographerEndDate = formatDate($("#photoEndDateTrans").val());
		var photographerStartTime = $("#photoStartTimeTrans").val();
		var photographerEndTime = $("#photoEndTimeTrans").val();
		var photochecker = false;
		if(photographer != "--Photographer--"){
			if(photographerStartDate=="" ||photographerEndDate == ""
				|| photographerStartTime == "" || photographerEndTime == ""){
					alert("Please fill up all the necessary fields for photographer!");
					return;
				}else{
					var d1 = Date.parse(photographerStartDate);
					var d2 = Date.parse(photographerEndDate);
					if (d1 > d2) {
						alert ("Invalid dates for photographer!");
						return;
					}
				}
		}
        var stylist = $("#stylistTrans").val();

        var stylistStartDate = formatDate($("#stylistStartDateTrans").val());
		var stylistEndDate = formatDate($("#stylistEndDateTrans").val());
		var stylistStartTime= $("#stylistStartTimeTrans").val();
		var stylistEndTime= $("#stylistEndTimeTrans").val();
		if(stylist != "--Stylist--"){
			if(stylistStartDate == "" || stylistEndDate == "" 
				|| stylistStartTime == "" || stylistEndTime == ""){
					alert("Please fill up all the necessary fields for stylist!");
					return;
				}else{
					var d1 = Date.parse(stylistStartDate);
					var d2 = Date.parse(stylistEndDate);
					if (d1 > d2) {
                        alert ("Invalid dates for stylist!");
                        return;
					}
				}
		}
        var itemsrented = [];
        var eventtimestart = getTime($("#eventTimeStartTrans").val().slice(0,5), $("#eventTimeStartTrans").val().slice(-2));
        var eventtimeend = getTime($("#eventTimeEndTrans").val().slice(0,5), $("#eventTimeEndTrans").val().slice(-2));
        if(theme == "--Theme--"){
            theme = null;
        }
        if(photographer == "--Photographer--"){
            photographer= null;
        }
        if(stylist == "--Stylist--"){
            stylist = null;
        }
        $('#addItemsHereTrans > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            itemsrented.push({
                itemcode:$(this).prop("id").split("-")[1],
                itemcode2:product[0],
                quantity:product[1],
                price:product[2]
            });
        });
        var measurementMale = [];
        $('#putTailorMaleHereTrans > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            measurementMale.push({
                name:product[0],
                category:product[1],
                neck:product[2],
                chest:product[3],
                waist:product[4],
                seat:product[5],
                hip:product[6],
                shoulder:product[7],
                wrist:product[8],
                sleeves:product[9],
                backlength:product[10],
                inseam:product[11],
                outseam:product[12],
                hem:product[13],
                fabric:product[14],
                price:product[15],
                type:product[16],
                img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
            });
        });
        var measurementFemale = [];
        $('#putTailorFemaleHereTrans > tr').each(function(){
            i = 0;
            product = [""];
            $(this).closest('tr').each(function(){
                $(this).find('td').each(function(){
                    product[i]=$(this).html();
                    i++;
                });
            });
            measurementFemale.push({
                name:product[0],
                category:product[1],
                chest:product[2],
                bust:product[3],
                hip:product[4],
                waist:product[5],
                wrist:product[6],
                shoulder:product[7],
                frontskirt:product[8],
                backskirt:product[9],
                fabric:product[10],
                type:product[11],
                price:product[12],
                img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
            });
        });
        //console.log(measurementFemale);
        if(itemsrented.length>0){
            if(deposit==""){
                alert("Deposit is required when renting an item!");
                return;
            }
        }else{
            if(deposit!=""){
                alert("Deposit is only for renting items!");
                return;
            }
        }
        if( location == "" || totalpayment == "" || totalpayment == "0" ||
            eventdatestart == "" || eventdateend == "" || eventtime == ""
        || eventdate == "" ||eventtimestart == "" ||eventtimeend== ""){
            alert("Please fill up all the necessary fields!");
            return;
        }
        //alert(theme+"--"+itemsrented.length+"--"+measurementMale.length+"--"+measurementFemale.length);
        if((measurementFemale.length == 0 || measurementMale.length == 0)
                && (theme === null) && (itemsrented.length == 0)){
                    alert("Atleast a theme, tailor or item must be selected to be able to transact.");
                    return;
                }
 
    connectMysql2(function(connection){
        getCustomer(connection, transid, function(customer){
            var customerTransac = {
                customerName:customer,
                location:location,
                totalPayment:totalpayment,
                downpayment:downpayment,
                eventDateStart:eventdatestart,
                eventDateEnd:eventdateend,
                rentStart:rentStart,
                rentEnd:rentEnd,
                eventTimeTransacted:eventtime,
                eventDateTransacted:eventdate,
                depositTransact:deposit,
                theme:theme,
                cake:cake,
                photographer:{
                    photographer:photographer,
                    photoStartDate:photographerStartDate,
                    photoEndDate: photographerEndDate,
                    photoStartTime: getTime(photographerStartTime.slice(0,5), photographerStartTime.slice(-2)),
                    photoEndTime:getTime(photographerEndTime.slice(0,5), photographerEndTime.slice(-2))
                },
                stylist:{
                    stylist:stylist,
                        stylistStartDate:stylistStartDate,
                        stylistEndDate:stylistEndDate,
                        stylistStartTime:getTime(stylistStartTime.slice(0,5), stylistStartTime.slice(-2)),
                        stylistEndTime:getTime(stylistEndTime.slice(0,5), stylistEndTime.slice(-2))
                        },
                itemsrented:itemsrented,
                measurementMale:measurementMale,
                measurementFemale:measurementFemale,
                transid: transid,
                eventtimestart:eventtimestart,
                eventtimeend:eventtimeend
            };
            new Promise((resolve, reject)=>{
                checkifmaxtransactsreached(connection, customerTransac.eventDateStart, customerTransac.eventDateEnd, function(check){
                    if(check){
                        reject("max3days");
                    }else{
                        resolve(true);
                    }
                });
            }).then((res)=>{
                return new Promise((resolve, reject)=>{
                    checkifmaxtransactsreachedononelocation(connection,  customerTransac.eventDateStart, customerTransac.eventDateEnd, customerTransac.location, function(check){
                        if(check){
                            reject("conflictlocation");
                        }else{
                            resolve(res);
                        }
                    });
                });
            }).then((res)=>{
                insertTransaction(connection, customerTransac, function(connection){
                        selectedLocation(connection, transid, function(){
                    });
                });
            }).catch((err)=>{
                if(err== "conflictlocation"){
                    transacInsert = customerTransac;
                    $("#confirmConflictModal").modal("open")
                    $("#confirmConflictTitle").text("Conflict of event on same date and same place!");
                    conflict = "update";
                }else if(err =="max3days"){
                    transacInsert = customerTransac;
                    $("#confirmConflictModal").modal("open")
                    $("#confirmConflictTitle").text("There should only be max of 3 events on a day!");
                    conflict = "update";
                }
            });
            });
        });
        
    });
    
    $(".mymodal").on('change', '#selectedLocation', function(){
        locationid = $("#selectedLocation").val();
        connectMysql2(function(connection){
            getMaxRows(function(rows){
                if(rows<=5){
                    visiblepages=1;
                }else {
                    visiblepages=5;
                }
                if(rows%5!=0){
                    totalpages=Math.floor((rows/5))+1;
                }else{
                    totalpages=rows/5;
                }
                if(rows==0){
                    $("#loadInventoryItemsHere").html("");
                    return;
                }
                $('#inventoryPagination').twbsPagination({
                                totalPages: totalpages,
                                visiblePages: visiblepages,
                                onPageClick: function (event, page){
                                    inventoryPage = (5*(page-1));
                                        loadInventoryTable(connection, inventoryPage);
                                }
                 });
            }, "tblinventory", "");
            getDateTransacted(connection, locationid);
            getTimeTransacted(connection, locationid);
            getDeposit(connection, locationid);
            getTotalPayment(connection, transid, locationid);
            downPayment(connection, transid, locationid);
            getDateStartDateEnd(connection, transid, locationid);
            getRent(connection, transid, locationid);
            checkIfTransHasTheme(connection, transid, locationid, function(hastheme, themeid){
                loadTheme(connection, hastheme, themeid);
            });
            getCakes(connection, transid, locationid, function(cakearr){
                loadCake(connection, cakearr);
            });
            getPhotographer(connection, transid, locationid, function(photoid){
                loadPhotographer(connection, photoid);
            });
            loadMaleTailor(connection, transid, locationid);
            loadTailorFemale(connection, transid, locationid);
            loaditemsadded(connection, transid, locationid);
            getStylist(connection, transid, locationid, function(stylistid){
                loadStylist(connection, stylistid);
            });
 
        });
    });
    $(".mymodal").on('click', '#removeLocationLink', function(){
        var length = document.getElementById('selectedLocation').length;
        if (length==1){
            alert("This transaction have only one location and cant be remove!");
            return;
        }
        $("#removeLocationModal").modal("open");
        connectMysql2(function(connection){
            loadRemoveLocation(connection, transid);
        });
        $("#removeLocationModal").modal("open");
    });
    $(".mymodal").on('click', '.removelocation', function(){
        $("#removeLocationModal").modal("close");
        $("#deleteModal").modal("open");
        $("#deleteTitle").text("Are you sure you want to delete this transaction?");
        deleteTransac = "deleteLocation";
        deleteTransLocId = $(this).data("value");
        //alert(deleteTransLocId);
    });
    $(".mymodal").on('click', '#deleteConfirmButton', function(){
        if(deleteTransac=="deleteLocation"){
            connectMysql2(function(connection){
                deleteLocation(connection, deleteTransLocId, function(conn, error){
                    if(!error){
                        conn.commit(function(err){
                            if(err){
                                conn.rollback(function(){
                                    throw err;
                                });
                            }else{
                                deleteMyLocation(connection, deleteTransLocId, function(connection){
                                    selectedLocation(connection, transid);
                                });
                            }
                        });
                    }
                });
            });
        }else if (deleteTransac=="notreturned") {
            connectMysql2(function(connection){
                notReturnedItem(connection,  $("#paymentLocation").val(), function(mycon){
                    $("#notReturnedButton").attr("disabled", true);
                });
            });
        }else{
            connectMysql2(function(connection){
                getLocationsForDelete(connection, transid, function(rows){
                    rows.forEach(function(row){
                        deleteLocation(connection, row.trans_locationid, function(){
                            deleteMyLocation(connection, row.trans_locationid, function(){
                                console.log('deleted '+row.trans_locationid);
                            });
                        });
                    });
                });
                setTimeout(function(){
                    deleteMyTransaction(connection, transid);
                }, 200)
            });
        }
    });
    $("#loadTransacListHere").on('click', '.viewPayment', function(){
        transid = $(this).prop("id").split("-")[1];
        $("#editPaymentModal").modal("open");
        connectMysql2(function(connection){
            selectedLocationPayment(connection, transid, function(locationid){
                checkifitemsadded(connection, transid, locationid, function(checker){
                    if(checker){
                        console.log(locationid);
                        checkIfDepositReturned(connection, locationid, function(deposit){
                            if(deposit){
                                
                                checkIfItemsReturnedLocationId(connection, locationid, function(mycheck){
                                    if(mycheck){
                                        $("#returnDepositss").attr("disabled", true);
                                        $("#notReturnedButton").attr("disabled", true);
                                    }else{ 
                                        $("#returnDepositss").attr("disabled", false);
                                        $("#notReturnedButton").attr("disabled", false);
                                    }
                                });
                            }else{
                                $("#returnDepositss").attr("disabled", true);
                                $("#notReturnedButton").attr("disabled", true);
                            }
                        });
                    }else{
                        $("#returnDepositss").attr("disabled", true);
                        $("#notReturnedButton").attr("disabled", true);
                    }
                });
                checkIfTailorItems(connection, transid, locationid, function(checktailor){
                    if(checktailor){
                        checkItemClaimed(connection, locationid, function(checktailorb){
                            if(checktailorb){
                                $("#claimItem").attr("disabled", true);
                            }else{
                                $("#claimItem").attr("disabled", false);
                            }
                        });
                    }else{
                        $("#claimItem").attr("disabled", true);
                    }
                });
                getPayment(connection, locationid);
            });
        });
    });
    $(".mymodal").on('click', '#returnDepositss', function(){
 
        $("#returnItemsModal").modal("open");
        connectMysql2(function(connection){
            loadItemsRented(connection, locationid);
            loadItemsForReturn(connection, locationid);
            getDepositOnHand(connection, locationid);
        });
    });
    $(".mymodal").on('click', '.returneditem', function(){
        var itemid = $(this).prop("id").split("-")[1];
        deleteImages(function(){
            getImageInventory(itemid, function(img){
                readWriteFile(img, myimage,function(address){
                    $('#invReturnImage').prop('src', '../public/temp/out'+myimage+'.jpg');
                    myimage++;
                });
            });
        });
        var i = 0;
        var product = [""];
        var myitems = {
            quantity:0,
            price: 0,
            itemcode2: "",
            itemcode:""
        }
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        if(product[1]==0){
            return;
        }
        myitems.quantity = product[1];
        myitems.price = product[2];
        myitems.itemcode = itemid
        myitems.itemcode2 = product[0]
 
        var html = "<td>"+myitems.itemcode2+"</td>"+
        "<td>"+(myitems.quantity-1)+"</td>"+
        "<td>"+myitems.price+"</td>"+
        "<td><a class='waves-effect waves-light btn returneditem' id = 'returneditem-"+myitems.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
        $("#returneditem-"+myitems.itemcode).html(html);
        var y = 0;
        var myquantity = 0;
        var myprice =0;
        $("#returnitem-"+myitems.itemcode+" > td").each(function() {
            if(y==1){
                myquantity = $(this).html();
            }
            if(y==2){
                myprice = $(this).html();
            }
            y++;
        });
        var damaged = $("#returnitemText-"+myitems.itemcode).val();
        if(damaged == ""){
            damaged = 0;
        }
        html = "<td>"+myitems.itemcode2+"</td>"+
        "<td>"+(parseFloat(myquantity)+parseFloat(1))+"</td>"+
        "<td>"+((parseFloat(myquantity)+parseFloat(1))*myitems.price)+"</td>"+
        "<td><a class='waves-effect waves-light btn returnitem' id = 'returnitem-"+myitems.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"+
        "<td><input type = 'number' id = 'returnitemText-"+myitems.itemcode+"' class = 'returnitemText' value = '"+damaged+"'></td>";
        $("#returnitem-"+myitems.itemcode).html(html);
    });
    $(".mymodal").on('click', '.returnitem', function(){
        var itemid = $(this).prop("id").split("-")[1];
        deleteImages(function(){
            getImageInventory(itemid, function(img){
                readWriteFile(img, myimage,function(address){
                    $('#invReturnImage').prop('src', '../public/temp/out'+myimage+'.jpg');
                    myimage++;
                });
            });
        });
        var i = 0;
        var product = [""];
        var myitems = {
            quantity:0,
            price: 0,
            itemcode2: "",
            itemcode:""
        }
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        if(product[1]==0){
            return;
        }
        myitems.quantity = product[1];
        myitems.price = product[2];
        myitems.itemcode = itemid;
        myitems.itemcode2 = product[0];
        var damaged = $("#returnitemText-"+myitems.itemcode).val();
        if(damaged == ""){
            damaged = 0;
        }
        var html = "<td>"+myitems.itemcode2+"</td>"+
        "<td>"+(myitems.quantity-1)+"</td>"+
        "<td>"+((myitems.quantity-1)*myitems.price)+"</td>"+
        "<td><a class='waves-effect waves-light btn returnitem' id = 'returnitem-"+myitems.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"+
        "<td><input type = 'number' id = 'returnitemText-"+myitems.itemcode+"' class = 'returnitemText' value = '"+damaged+"'></td>";
        $("#returnitem-"+myitems.itemcode).html(html);
        var y = 0;
        var myquantity = 0;
        var myprice =0;
        $("#returneditem-"+myitems.itemcode+" > td").each(function() {
            if(y==1){
                myquantity = $(this).html();
            }
            if(y==2){
                myprice = $(this).html();
            }
            y++;
        });
        html = "<td>"+myitems.itemcode2+"</td>"+
        "<td>"+(parseFloat(myquantity)+parseFloat(1))+"</td>"+
        "<td>"+(myprice)+"</td>"+
        "<td><a class='waves-effect waves-light btn returneditem' id = 'returneditem-"+myitems.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
        $("#returneditem-"+myitems.itemcode).html(html);
    });
    $(".mymodal").on('click', '#returnTailorAttire', function(){
        $("#returnTailorAttireModal").modal("open");
    });
    $(".mymodal").on('click', '#claimItem', function(){
        $("#claimItem").attr("disabled", true);
        connectMysql2(function(connection){
            claimItem(connection, $("#paymentLocation").val());
        });
    });
    $(".mymodal").on('click', '#updatePaymentButton', function(){
        if($("#amountDeposit").val() == ""){
            connectMysql2(function(connection){
                checkifitemsadded(connection, transid, $("#paymentLocation").val(), function(check){
                    if(check){
                        alert("This item have rented items, it must have a deposit!");
                        connection.end();
                        return;
                    }
                    connection.end();
                });
            });
        }
 
        if($("#totalPaymentss").val() == "" || $("#currentBalance").val() == "" || $("#payBalance").val() == ""){
            alert("Please provide all the necessary details!");
            return;
        }
        if(parseFloat($("#payBalance").val()) > parseFloat($("#totalPaymentss").val())){
            alert("Paid balance cant be greater than total payment!");
            return;
        }
        if(parseFloat($("#payBalance").val()) > parseFloat($("#currentBalance").val())){
            alert("Pay Balance cannot be greater than Current Balance!");
            return;
 
        }
        obj = {
            balance: $("#payBalance").val(),
            deposit: $("#amountDeposit").val(),
            payment: $("#totalPaymentss").val(),
            locationid: $("#paymentLocation").val()
        }
        connectMysql2(function(connection){
            updatePayments(connection, $("#paymentLocation").val(), obj);
        });
    });
    $(".mymodal").on('change', '#paymentLocation', function(){
        locationid = $("#paymentLocation").val();
        connectMysql2(function(connection){
                checkifitemsadded(connection, transid, locationid, function(checker){
                    if(checker){
                        checkIfItemsReturnedLocationId(connection, locationid, function(mycheck){
                            if(mycheck){
                                $("#returnDepositss").attr("disabled", true);
                                $("#notReturnedButton").attr("disabled", true);
                            }else{
                               
                                $("#returnDepositss").attr("disabled", false);
                                $("#notReturnedButton").attr("disabled", false);
                            }
                        });
                    }else{
                        $("#returnDepositss").attr("disabled", true);
                        $("#notReturnedButton").attr("disabled", true);
                    }
                });
                checkIfTailorItems(connection, transid, locationid, function(checktailor){
                    if(checktailor){
                        checkItemClaimed(connection, locationid, function(checktailorb){
                            if(checktailorb){
                                $("#claimItem").attr("disabled", true);
                            }else{
                                $("#claimItem").attr("disabled", false);
                            }
                        });
                    }else{
                        $("#claimItem").attr("disabled", true);
                    }
                });
                getPayment(connection, locationid);
        });
    });
    $("#loadTransacListHere").on('click', '.delete', function(){
        $("#removeLocationModal").modal("close");
        $("#deleteModal").modal("open");
        $("#deleteTitle").text("Are you sure you want to delete this transaction?");
        deleteTransac = "deleteTransac";
        transid = $(this).prop("id").split("-")[1];
    });
    $("#searchTransacList").on('keyup', function(){
        connectMysql2(function(connection){
            searchTransaction(connection, '%'+$('#searchTransacList').val()+'%');
        });
    });
    $(".mymodal").on('change', '#typeMale', function(){
        $("#chest").prop("disabled", false);
        $("#waistMale").prop("disabled", false);
        $("#hipMale").prop("disabled", false);
        $("#sleevesMale").prop("disabled", false);
        $("#backlengthMale").prop("disabled", false);
        $("#inseamMale").prop("disabled", false);
        $("#outseamMale").prop("disabled", false);
        $("#seatMale").prop("disabled", false);
        $("#hemMale").prop("disabled", false);
        $("#shoulderMale").prop("disabled", false);
        $("#wristMale").prop("disabled", false);
        $("#neckMale").prop("disabled", false);
        if($("#typeMale").val()=="coat"){
            $("#chest").val("").prop("disabled", true);
            $("#seatMale").val("").prop("disabled", true);
            $("#inseamMale").val("").prop("disabled", true);
            $("#outseamMale").val("").prop("disabled", true);
            $("#hemMale").val("").prop("disabled", true);
        }else if ($("#typeMale").val()=="slacks") {
            $("#neckMale").val("").prop("disabled", true);
            $("#chest").val("").prop("disabled", true);
            $("#sleevesMale").val("").prop("disabled", true);
            $("#backlengthMale").val("").prop("disabled", true);
            $("#shoulderMale").val("").prop("disabled", true);
            $("#wristMale").val("").prop("disabled", true);
        }else if ($("#typeMale").val()=="polo") {
            $("#chest").val("").prop("disabled", true);
            $("#seatMale").val("").prop("disabled", true);
            $("#inseamMale").val("").prop("disabled", true);
            $("#outseamMale").val("").prop("disabled", true);
            $("#hemMale").val("").prop("disabled", true);
        }else if ($("#typeMale").val()=="vest") {
            $("#chest").val("").prop("disabled", true);
            $("#seatMale").val("").prop("disabled", true);
            $("#backlengthMale").val("").prop("disabled", true);
            $("#inseamMale").val("").prop("disabled", true);
            $("#outseamMale").val("").prop("disabled", true);
            $("#hemMale").val("").prop("disabled", true);
            $("#wristMale").val("").prop("disabled", true);
        }else{
            $("#chest").prop("disabled", false);
            $("#waistMale").prop("disabled", false);
            $("#hipMale").prop("disabled", false);
            $("#sleevesMale").prop("disabled", false);
            $("#backlengthMale").prop("disabled", false);
            $("#inseamMale").prop("disabled", false);
            $("#hemMale").prop("disabled", false);
            $("#shoulderMale").prop("disabled", false);
            $("#wristMale").prop("disabled", false);
            $("#neckMale").prop("disabled", false);
            $("#outseamMale").prop("disabled", false);
            $("#seatMale").prop("disabled", false);
        }
    });
    $(".mymodal").on('change', '#typeMaleTrans', function(){
        $("#chestTrans").prop("disabled", false);
        $("#waistMaleTrans").prop("disabled", false);
        $("#hipMaleTrans").prop("disabled", false);
        $("#sleevesMaleTrans").prop("disabled", false);
        $("#backlengthMaleTrans").prop("disabled", false);
        $("#inseamMaleTrans").prop("disabled", false);
        $("#outseamMaleTrans").prop("disabled", false);
        $("#seatMaleTrans").prop("disabled", false);
        $("#hemMaleTrans").prop("disabled", false);
        $("#shoulderMaleTrans").prop("disabled", false);
        $("#wristMaleTrans").prop("disabled", false);
        $("#neckMaleTrans").prop("disabled", false);
        if($("#typeMaleTrans").val()=="coat"){
            $("#chestTrans").val("").prop("disabled", true);
            $("#seatMaleTrans").val("").prop("disabled", true);
            $("#inseamMaleTrans").val("").prop("disabled", true);
            $("#outseamMaleTrans").val("").prop("disabled", true);
            $("#hemMaleTrans").val("").prop("disabled", true);
        }else if ($("#typeMaleTrans").val()=="slacks") {
            $("#neckMaleTrans").val("").prop("disabled", true);
            $("#chestTrans").val("").prop("disabled", true);
            $("#sleevesMaleTrans").val("").prop("disabled", true);
            $("#backlengthMaleTrans").val("").prop("disabled", true);
            $("#shoulderMaleTrans").val("").prop("disabled", true);
            $("#wristMaleTrans").val("").prop("disabled", true);
        }else if ($("#typeMaleTrans").val()=="polo") {
            $("#chestTrans").val("").prop("disabled", true);
            $("#seatMaleTrans").val("").prop("disabled", true);
            $("#inseamMaleTrans").val("").prop("disabled", true);
            $("#outseamMaleTrans").val("").prop("disabled", true);
            $("#hemMaleTrans").val("").prop("disabled", true);
        }else if ($("#typeMaleTrans").val()=="vest") {
            $("#chestTrans").val("").prop("disabled", true);
            $("#seatMaleTrans").val("").prop("disabled", true);
            $("#backlengthMaleTrans").val("").prop("disabled", true);
            $("#inseamMaleTrans").val("").prop("disabled", true);
            $("#outseamMaleTrans").val("").prop("disabled", true);
            $("#hemMaleTrans").val("").prop("disabled", true);
            $("#wristMaleTrans").val("").prop("disabled", true);
        }else{
            $("#chestTrans").prop("disabled", false);
            $("#waistMaleTrans").prop("disabled", false);
            $("#hipMaleTrans").prop("disabled", false);
            $("#sleevesMaleTrans").prop("disabled", false);
            $("#backlengthMaleTrans").prop("disabled", false);
            $("#inseamMaleTrans").prop("disabled", false);
            $("#hemMaleTrans").prop("disabled", false);
            $("#shoulderMaleTrans").prop("disabled", false);
            $("#wristMaleTrans").prop("disabled", false);
            $("#neckMaleTrans").prop("disabled", false);
            $("#outseamMaleTrans").prop("disabled", false);
            $("#seatMaleTrans").prop("disabled", false);
        }
    });
    $(".mymodal").on('click', '#notReturnedButton', function() {
        $("#deleteModal").modal("open");
        $("#deleteTitle").text("Are you sure you want to mark this item as not returned? this action cant be reverted.");
        deleteTransac = "notreturned";
    });
    $(".mymodal").on('click', '#confirmReturnItemsButton', function(){
        if($("#depositReturnedInventory").val()==""){
            alert("Please fill up the deposit to be returned!");
            return;
        }
       
        var items = [];
        var damagedItems;
        var returnitem = true;
        new Promise((resolve, reject)=>{
            $("#loadItemsRentedInventoryHere > tr").each(function(){
                i = 0;
                product = [""];
                $(this).closest('tr').each(function(){
                    $(this).find('td').each(function(){
                        product[i]=$(this).html();
                        i++;
                        
                    });
                });
                damagedItems = $("#returnitemText-"+$(this).prop("id").split("-")[1]).val();
                if(damagedItems == ""){
                    damagedItems = 0;
                }
                if(damagedItems <0){
                    reject(true);
                }
                if(parseFloat(damagedItems)>parseFloat(product[1])){
                    reject(false);
                }
                items.push({
                    quantity:product[1],
                    itemcode:product[0],
                    rentid: $(this).data("value"),
                    itemid:$(this).prop("id").split("-")[1],
                    damagedItems:damagedItems
                });
                //console.log($(this).prop("id").split("-")[1]);
            });
            //return;
            //console.log(items)
            $("#loadItemsReturnedIventoryHere > tr").each(function(){
                
                i = 0;
                product = [""];
                $(this).closest('tr').each(function(){
                    $(this).find('td').each(function(){
                        product[i]=$(this).html();
                        i++;
                        
                    });
                });
                if(product[1]>0){
                    returnitem =false;
                }
            });
     
            if(returnitem == true){
                $("#returnDepositss").attr("disabled", true);
            }
           resolve(true);
        }).then((res)=>{
            connectMysql2(function(connection){
                returnItemsIventory(connection, $("#paymentLocation").val(), items, returnitem, $("#depositReturnedInventory").val());
            });
        }).catch((err)=>{
            if(err == true){
                alert("Please dont enter negative values on damaged items!");
            }else{
                alert("Damaged item cannot be greater than quantity!");
            }
        });
        
    });
    $("#refreshTransactionListButton").on('click', function(){
        connectMysql2(function(connection){
            loadTransactionList(connection, mypage);
        });
    });
    $(".mymodal").on('change', '#photographer', function(){
        //console.log($("#photographer").val());
        initPayment();
    });
    $(".mymodal").on('change', '#stylist', function(){
        initPayment();
    });
    $(".mymodal").on('change', '#photographerTrans', function(){
        initPaymentTrans();
    });
    $(".mymodal").on('change', '#stylistTrans', function(){
        initPaymentTrans();
    });
    $(".mymodal").on('click', '#closeTransacUpdateModalButton', function(){
        deleteImages(function(){
            $('#themeImage').prop('src', '../public/images/default.jpg');
            $('#cakeImage').prop('src', '../public/images/default.jpg');
        });
    });
    $(".mymodal").on('change', '#rentpackageTrans', function(){
		var packageid = $("#rentpackageTrans").val();
		getPackagePriceTrans(connection, packageid);
    });
    $(".mymodal").on('change', '#rentpackage', function(){
		var packageid = $("#rentpackage").val();
		getPackagePrice(connection, packageid);
	});
});
function loadTransactionList(connection, page){
    var sql = "SELECT c.firstname, c.lastname, t.transID, tl.time_transacted, tl.date "+
                        "FROM tblcustomer c "+
                        "INNER JOIN tbltransaction t "+
                    "ON c.custID = t.custID INNER JOIN tbltrans_location tl ON t.transID = tl.transID "+
                "AND tl.trans_locationid = ( "+
            "SELECT MAX(tl1.trans_locationid) "+
            "FROM tbltrans_location tl1 "+
            "WHERE tl1.transID = t.transID "+
        ")  ORDER BY "+
                    "t.transID DESC  LIMIT "+page+",5";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the transactions! please try again later!");
        }else{
            var html = "";
            $("#loadTransacListHere").html(html);
            //console.log(rows);
            rows.forEach(function(row){
                checkIfTransHaveBalance(connection, row.transID, function(check){
                    checkIfItemsReturned(connection, row.transID, function(check2){
                        html = "";
                        if(check==true && check2==false){
                            //not returned but and have balance blue
                            html += "<tr id = 'trans-"+row.transID+"' style='background-color: #b1b7f9;'>";
                        }else if (check==true && check2==true) {
                            //have balance and returned red
                            html += "<tr id = 'trans-"+row.transID+"' style='background-color: #ed7b8c;'>";
                        }else if (check==false && check2==false) {
                            //doesnt have balance but not returned pink
                            html += "<tr id = 'trans-"+row.transID+"' style='background-color: #f2a4f0;'>";
                        }else{
                            html += "<tr id = 'trans-"+row.transID+"'>";
                        }
                        html += "<td>"+row.transID+"</td>";
                        html += "<td>"+row.firstname+" "+row.lastname+"</td>";
                        html += "<td>"+row.date.toString().slice(0,15)+"</td>";
                        html += "<td>"+row.time_transacted+"</td>";
                        html += "<td><a class='waves-effect waves-light btn viewDetails' id = 'view-"+row.transID+"'><i class='material-icons'>pageview</i></a></td>";
                        html += "<td><a class='waves-effect waves-light btn viewPayment' id = 'payment-"+row.transID+"'><i class='material-icons'>payment</i></a></td>";
                        html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.transID+"'><i class='material-icons'>delete</i></a></td>";
                        html += "</tr>";
                        $("#loadTransacListHere").append(html);
                    });
                });
            });
        }
    });
}
function searchTransaction(connection, search){
    var sql = "SELECT tblcustomer.firstname, tblcustomer.lastname, tbltransaction.transID, "+
    "tbltransaction.time_transacted, tbltransaction.date FROM tbltransaction INNER JOIN "+
    "tblcustomer ON tblcustomer.custID = tbltransaction.custID "+
     "WHERE tblcustomer.firstname LIKE ? OR tblcustomer.lastname LIKE ? "+
     "OR tbltransaction.date LIKE ?  ORDER BY tbltransaction.transID DESC";
     var insert = [search, search, search];
     sql = mysql.format(sql, insert);
     connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the transactions! please try again later!");
        }else{
            var html = "";
            $("#loadTransacListHere").html(html);
            //console.log(rows);
            rows.forEach(function(row){
                checkIfTransHaveBalance(connection, row.transID, function(check){
                    html = "";
                    if(check){
                        html += "<tr id = 'trans-"+row.transID+"' style='background-color: #ed7b8c;'>";
                    }else{
                        html += "<tr id = 'trans-"+row.transID+"'>";
                    }
                    html += "<td>"+row.transID+"</td>";
                    html += "<td>"+row.firstname+" "+row.lastname+"</td>";
                    html += "<td>"+row.date.toString().slice(0,15)+"</td>";
                    html += "<td>"+row.time_transacted+"</td>";
                    html += "<td><a class='waves-effect waves-light btn viewDetails' id = 'view-"+row.transID+"'><i class='material-icons'>pageview</i></a></td>";
                    html += "<td><a class='waves-effect waves-light btn viewPayment' id = 'payment-"+row.transID+"'><i class='material-icons'>payment</i></a></td>";
                    html += "<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.transID+"'><i class='material-icons'>delete</i></a></td>";
                    html += "</tr>";
                    $("#loadTransacListHere").append(html);
                });
            });
        }
        connection.end();
    });
}
function selectedLocation(connection, transid, callback){
 sql = "SELECT tbltrans_location.trans_locationid, tbllocation.locationname, tbllocation.locationID FROM"+
                " tbltrans_location INNER JOIN tbllocation ON tbllocation.locationID = tbltrans_location.locationID"+
        " INNER JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID WHERE "+
                " tbltransaction.transID = '"+transid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the location. Please try again later!");
        }else {
            //console.log(sql);
            var html = "";
            rows.forEach(function(row){
                html += "<option value='"+row.trans_locationid+"' data-foo = '"+row.locationID+"'>"+row.locationname+"</option>";
            });
            $("#selectedLocation").html(html);
            $("#selectedLocation").material_select();
        }
        locationid = rows[0].trans_locationid;
        callback(rows[0].trans_locationid);
    });
}
function selectedLocationPayment(connection, transid, callback){
 sql = "SELECT tbltrans_location.trans_locationid, tbllocation.locationname, tbllocation.locationID FROM"+
                " tbltrans_location INNER JOIN tbllocation ON tbllocation.locationID = tbltrans_location.locationID"+
        " INNER JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID WHERE "+
                " tbltransaction.transID = '"+transid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the location. Please try again later!");
        }else {
            //console.log(sql);
            var html = "";
            rows.forEach(function(row){
                html += "<option value='"+row.trans_locationid+"' data-foo = '"+row.locationID+"'>"+row.locationname+"</option>";
            });
            $("#paymentLocation").html(html);
            $("#paymentLocation").material_select();
        }
        locationid = rows[0].trans_locationid;
        callback(rows[0].trans_locationid);
    });
}
function getDateFormat(date){
    return date.split(" ")[2]+" "+getMonthNameAbv(date.split(" ")[1])+", "+date.split(" ")[3];
}
function getMonthNameAbv(month){
    if(month == "Jan"){
        return "January";
    }else if (month == "Feb") {
        return "February";
    }else if (month == "Mar") {
        return "March";
    }else if (month == "Apr") {
        return "April";
    }else if (month == "May") {
        return "May";
    }else if (month == "Jun") {
        return "June";
    }else if (month == "Jul") {
        return "July";
    }else if (month == "Aug") {
        return "August";
    }else if (month == "Sep") {
        return "September";
    }else if (month == "Oct") {
        return "October";
    }else if (month == "Nov") {
        return "November";
    }else if (month == "Dec") {
        return "December";
    }
}
function getTotalPayment(connection, transid, locationid){
    var sql = "SELECT payment FROM tbltrans_location WHERE trans_locationid='"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. please try again later!");
        }else{
            $("#totalPayment").val(rows[0].payment);
        }
    });
}
function getDateStartDateEnd(connection, transid, locationid){
    var sql = "SELECT event_date_start, event_date_end FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            $("#eventDateStart").val(getDateFormat(rows[0].event_date_start.toString().slice(0,15)));
            $("#eventDateEnd").val(getDateFormat(rows[0].event_date_end.toString().slice(0,15)));
            geteventstart = rows[0].event_date_start;
            geteventend = rows[0].event_date_end;
        }
    });
}

function downPayment(connection, transid){
    var sql = "SELECT downpayment FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. please try again later!");
        }else{
            $("#downpayment").val(rows[0].downpayment);
        }
    });
}
function loadTheme(connection, hastheme, themeid){
    var sql = "SELECT themeID, themename, description FROM tbltheme";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the themes, please try again later!");
        }else{
            var html = "";
 
                    if(hastheme){
                        html += "<option value = '--Theme--'>--Theme--</option>";
                        rows.forEach(function(row){
                            if(themeid == row.themeID){
                                html += "<option value = '"+row.themeID+"' selected>"+row.themename+" ("+row.description+")</option>";
                            }else{
                                html += "<option value = '"+row.themeID+"'>"+row.themename+"  ("+row.description+")</option>";
                            }
                        });
                       
                        //deleteImages(function(){
                            getImageTheme(themeid, function(img){
                                readWriteFile(img, image,function(address){
                                    initPayment();
                                    $('#themeImage').prop('src', '../public/temp/out'+image+'.jpg');
                                    image++;
                                });
                            });
                        //});
                    }else{
                        html += "<option selected value = '--Theme--'>--Theme--</option>";
 
                        rows.forEach(function(row){
                                html += "<option value = '"+row.themeID+"'>"+row.themename+"  ("+row.description+")</option>";
                        });
 
                    }
 
            $("#theme").html(html);
            $("#theme").material_select();
        }
    });
}
function getCakes(connection, transid, locationid, callback){
    var sql = "SELECT tblcake.cakeID FROM tbltrans_location INNER JOIN tbltrans_cake ON "+
    "tbltrans_location.trans_locationid = tbltrans_cake.locationID INNER JOIN tblcake ON "+
    "tblcake.cakeID = tbltrans_cake.cakeID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the cakes. please try again later!");
        }else{
            var html = [];
            rows.forEach(function(row){
                html.push(row.cakeID);
            });
            callback(html);
        }
    });
}
function loadCake(connection, cakes){
    var sql = "SELECT cakeID, cakename, price FROM tblcake";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading cakes, please try again later!");
        }else{
            var html = "<option disabled selected>--Cake--</option>";
            var selectedcake = 0;
            rows.forEach(function(row){
                if(cakes.includes(row.cakeID)){
                        selectedcake = row.cakeID;
                        html += "<option value = '"+row.cakeID+"' selected>"+row.cakename+" ("+row.price+")</option>";
                }else{
                        html += "<option value = '"+row.cakeID+"'>"+row.cakename+" ("+row.price+")</option>";
                }
 
            });
            if(rows.length>0){
                var cakeid = selectedcake;
 
                    setTimeout(function(){
                        getImageCake(cakeid, function(img){
                            readWriteFile(img, image,function(address){
                                initPayment();
                                $('#cakeImage').prop('src', '../public/temp/out'+image+'.jpg');
                                image++;
                            });
                        });
                    }, 1000);
 
            }
            $("#cake").html(html);
            $("#cake").material_select();
        }
    });
}
function loadPhotographer(connection, photoid){
    var sql = "SELECT photoID, firstname, lastname, fee FROM tblphotographer";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the photographers. Please try again later!");
        }else{
            var html = "<option value = '--Photographer--'>--Photographer--</option>";
            rows.forEach(function(row){
                if(photoid==row.photoID){
                        html += "<option value = '"+row.photoID+"' selected>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
                }else{
                        html += "<option value = '"+row.photoID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
                }
 
            });
            $("#photographer").html(html);
            $("#photographer").material_select();
        }
    });
}
function loadStylist(connection, stylistid){
    var sql = "SELECT stylistID, firstname, lastname, fee FROM tblstylist";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the stylists. Please try again later!");
        }else{
            var html = "<option value = '--Stylist--'>--Stylist--</option>";
            rows.forEach(function(row){
                if(stylistid==row.stylistID){
                    html += "<option value = '"+row.stylistID+"' selected>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
                }else {
                    html += "<option value = '"+row.stylistID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
                }
 
            });
            $("#stylist").html(html);
            $("#stylist").material_select();
        }
    });
}
function getPhotographer(connection, transid, locationid, callback){
    var sql = "SELECT tblphotographer.photoID FROM tbltrans_location INNER JOIN tbltrans_photography ON "+
    "tbltrans_location.trans_locationid = tbltrans_photography.locationID INNER JOIN tblphotographer ON "+
    "tblphotographer.photoID = tbltrans_photography.photoID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"'"+
    " AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the photographers. Please try again later!");
        }else{
            if(rows.length > 0){
                    callback(rows[0].photoID);
            }else{
                callback(0);
            }
 
        }
    });
}
function getStylist(connection, transid, locationid, callback){
    var sql = "SELECT tblstylist.stylistID FROM tbltrans_location INNER JOIN tbltrans_stylist ON "+
    "tbltrans_location.trans_locationid = tbltrans_stylist.locationID INNER JOIN tblstylist ON "+
    "tblstylist.stylistID = tbltrans_stylist.stylistID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        //console.log(sql);
        if(err){
            console.log(err);
            alert("Something went wrong while loading the stylists. please try again later.");
        }else{
            if(rows.length>0){
                callback(rows[0].stylistID);
            }else {
                callback(0);
            }
        }
    });
}
function loadInventoryTable(connection, page){
 var sql = "SELECT itemcode, itemcode2, availableItems, price FROM tblinventory ORDER BY itemcode DESC LIMIT "+page+", 5";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the items. Please try again later!");
        }else{
            var html = "";
            var myitem =[];
            var i = 0;
            var product=[""];
            var exist = false;
            var myindex = 0;
            $('#addItemsHere > tr').each(function(){
                i = 0;
                product = [""];
                $(this).closest('tr').each(function(){
                    $(this).find('td').each(function(){
                        product[i]=$(this).html();
                        i++;
                    });
                });
                myitem.push({
                    itemcode:$(this).prop("id").split("-")[1],
                    itemcode2:product[0],
                    quantity:product[1],
                    price:product[2]
                });
            });
            rows.forEach(function(row){
                html += "<tr id = 'item-"+row.itemcode+"'>";
 
                    html += "<td>"+row.itemcode2+"</td>";
                    if(myitem.length>0){
                        myitem.forEach(function(myrow, index){
                            if(myrow.itemcode == row.itemcode){
                                //html += "<td>"+(row.quantity-myrow.quantity)+"</td>";
                                //return;
                                exist = true;
                                myindex = index;
                            }
                        });
                        if(exist){
                            if(row.itemcode == myitem[myindex].itemcode){
                                html += "<td>"+(row.availableItems-myitem[myindex].quantity)+"</td>";
                            }else{
                                html += "<td>"+row.availableItems+"</td>";
                            }
                        }else{
                            html += "<td>"+row.availableItems+"</td>";
                        }
                        console.log(exist);
                    }else{
                        html += "<td>"+row.availableItems+"</td>";
                    }
                    html += "<td>"+row.price+"</td>";
                    html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
                    html+="<td><a class='waves-effect waves-light btn viewImage' id = 'viewImage-"+row.itemcode+"'><i class='material-icons'>pageview</i></a></td></tr>";
                });
            $(".mymodal #loadInventoryItemsHere").html(html);
        }
    });
}
function loadInventoryTableTrans(connection, page){
 var sql = "SELECT itemcode, itemcode2, availableItems, price FROM tblinventory ORDER BY itemcode DESC LIMIT "+page+", 5";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the items. Please try again later!");
        }else{
            var html = "";
            var myitem =[];
            var i = 0;
            var product=[""];
            var exist = false;
            var myindex = 0;
            $('#addItemsHereTrans > tr').each(function(){
                i = 0;
                product = [""];
                $(this).closest('tr').each(function(){
                    $(this).find('td').each(function(){
                        product[i]=$(this).html();
                        i++;
                    });
                });
                myitem.push({
                    itemcode:$(this).prop("id").split("-")[1],
                    itemcode2:product[0],
                    quantity:product[1],
                    price:product[2]
                });
            });
            rows.forEach(function(row){
                html += "<tr id = 'itemtrans-"+row.itemcode+"'>";
 
                    html += "<td>"+row.itemcode2+"</td>";
                    if(myitem.length>0){
                        myitem.forEach(function(myrow, index){
                            if(myrow.itemcode == row.itemcode){
                                //html += "<td>"+(row.quantity-myrow.quantity)+"</td>";
                                //return;
                                exist = true;
                                myindex = index;
                            }
                        });
                        if(exist){
                            if(row.itemcode == myitem[myindex].itemcode){
                                html += "<td>"+(row.availableItems-myitem[myindex].quantity)+"</td>";
                            }else{
                                html += "<td>"+row.availableItems+"</td>";
                            }
                        }else{
                            html += "<td>"+row.availableItems+"</td>";
                        }
                        //console.log(exist);
                    }else{
                        html += "<td>"+row.availableItems+"</td>";
                    }
                    html += "<td>"+row.price+"</td>";
                    html += "<td><a class='waves-effect waves-light btn additemtrans' id = 'additemtrans-"+row.itemcode+"'><i class='material-icons'>add</i></a></td></tr>";
            });
            $(".mymodal #loadInventoryItemsHereTrans").html(html);
        }
    });
}
function loaditemsadded(connection, transid, locationid){
    var sql = "SELECT tblinventory.itemcode2, tbltrans_rent.itemcode, tbltrans_rent.rentID, tbltrans_rent.qty, tblinventory.price "+
    "FROM tbltrans_rent INNER JOIN tblinventory ON tblinventory.itemcode = tbltrans_rent.itemcode "+
    "INNER JOIN tbltrans_location ON tbltrans_location.trans_locationid = tbltrans_rent.locationID "+
    "INNER JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID WHERE "+
    "tbltransaction.transID = '"+transid+"' AND tbltrans_location.trans_locationid='"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the items. please try again later!");
        }else {
            var html = "";
            rows.forEach(function(row){
                html += "<tr id = 'addeditem-"+row.itemcode+"'><td>"+row.itemcode2+"</td>"+
                "<td>"+row.qty+"</td>"+
                "<td>"+(row.price*row.qty)+"</td>"+
                "<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
                +"</tr>";
            });
            $("#addItemsHere").html(html);
        }
 
    });
}
function checkifitemsadded(connection, transid, locationid, callback){
    var sql = "SELECT tblinventory.itemcode2, tbltrans_rent.itemcode, tbltrans_rent.rentID, tbltrans_rent.qty, tblinventory.price "+
    "FROM tbltrans_rent INNER JOIN tblinventory ON tblinventory.itemcode = tbltrans_rent.itemcode "+
    "INNER JOIN tbltrans_location ON tbltrans_location.trans_locationid = tbltrans_rent.locationID "+
    "INNER JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID WHERE "+
    "tbltransaction.transID = '"+transid+"' AND tbltrans_location.trans_locationid='"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the items. please try again later!");
        }else {
            if(rows.length>0){
                callback(true);
            }else{
                callback(false);
            }
        }
    });
}
function checkIfItemsOnDatabase(connection, transid, locationid, callback){
    var sql = "SELECT tblinventory.itemcode2, tbltrans_rent.itemcode, tbltrans_rent.rentID, tbltrans_rent.qty, tblinventory.price "+
    "FROM tbltrans_rent INNER JOIN tblinventory ON tblinventory.itemcode = tbltrans_rent.itemcode "+
    "INNER JOIN tbltrans_location ON tbltrans_location.trans_locationid = tbltrans_rent.locationID "+
    "INNER JOIN tbltransaction ON tbltransaction.transID = tbltrans_location.transID WHERE "+
    "tbltransaction.transID = '"+transid+"' AND tbltrans_location.trans_locationid='"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the items. please try again later!");
        }else {
            if(rows.length > 0){
                callback(true);
            }else{
                callback(false);
            }
        }
    });
}
function loadMaleTailor(connection, transid, locationid){
    var sql = "SELECT tbltrans_tailor_boy.*,tbltailor.claimdate FROM tbltrans_location INNER JOIN tbltrans_tailor_boy ON "+
    "tbltrans_location.trans_locationid = tbltrans_tailor_boy.locationID INNER JOIN tbltailor ON "+
    "tbltailor.tailorID = tbltrans_tailor_boy.tailorID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the tailor details. Please try again later!");
        }else{
            var html = "";
            rows.forEach(function(row){
                var image = btoa(String.fromCharCode(...new Uint8Array(row.img)));
                html += "<tr class='rowTailorMale'  id = 'tailorMale-"+row.id+"' data-value = '"+image+"'>";
                    html += "<td>"+row.name+"</td>";
                    html += "<td>Male</td>";
                    html += "<td>"+row.neck+"</td>";
                    html += "<td>"+row.chest+"</td>";
                    html += "<td>"+row.waist+"</td>";
                    html += "<td>"+row.seat+"</td>";
                    html += "<td>"+row.hip+"</td>";
                    html += "<td>"+row.shoulder+"</td>";
                    html += "<td>"+row.wrist+"</td>";
                    html += "<td>"+row.sleeves+"</td>";
                    html += "<td>"+row.backlength+"</td>";
                    html += "<td>"+row.inseam+"</td>";
                    html += "<td>"+row.outseam+"</td>";
                    html += "<td>"+row.hem+"</td>";
                    html += "<td>"+row.fabric+"</td>";
                    html += "<td>"+row.price+"</td>";
                    html += "<td>"+row.type+"</td>";
                    html += "<td><a class='waves-effect waves-light btn editMale' id = 'editMale-"+row.id+"'><i class='material-icons'>edit</i></a></td>";
                    html += "<td><a class='waves-effect waves-light btn removeMale' id = 'removeMale-"+row.id+"'><i class='material-icons'>clear</i></a></td>";
                    html += "</tr>";
            });
            $("#putTailorMaleHere").html(html);
            if(rows.length>0){
                $("#claimdate").val(getDateFormat(rows[0].claimdate.toString().slice(0,15)));
            }
 
            $(".userLabel").addClass('active');
        }
    });
}
function loadTailorFemale(connection, transid, locationid){
    var sql = "SELECT tbltrans_tailor_girl.*,tbltailor.claimdate FROM tbltrans_location INNER JOIN tbltrans_tailor_girl ON" +
    " tbltrans_location.trans_locationid = tbltrans_tailor_girl.locationID INNER JOIN tbltailor ON"+
    " tbltailor.tailorID = tbltrans_tailor_girl.tailorID INNER JOIN tbltransaction ON tbltransaction.transID ="+
     " tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the female tab. please try again later!");
        }else{
            var html = "";
            rows.forEach(function(row){
                var image = btoa(String.fromCharCode(...new Uint8Array(row.img)));
                html += "<tr class='rowTailorFemale'  id = 'tailorFemale-"+row.id+"' data-value = '"+image+"'>";
                html += "<td>"+row.name+"</td>";
                html += "<td>Female</td>";
                html += "<td>"+row.chest+"</td>";
                html += "<td>"+row.bust+"</td>";
                html += "<td>"+row.hip+"</td>";
                html += "<td>"+row.waist+"</td>";
                html += "<td>"+row.wrist+"</td>";
                html += "<td>"+row.shoulder+"</td>";
                html += "<td>"+row.frontskirt+"</td>";
                html += "<td>"+row.backskirt+"</td>";
                html += "<td>"+row.fabric+"</td>";
                html += "<td>"+row.type+"</td>";
                html += "<td>"+row.price+"</td>";
                html += "<td><a class='waves-effect waves-light btn editFemale' id = 'editFemale-"+row.id+"'><i class='material-icons'>edit</i></a></td>";
                html += "<td><a class='waves-effect waves-light btn removeFemale' id = 'removeFemale-"+row.id+"'><i class='material-icons'>clear</i></a></td>";
                html += "</tr>";
 
            });
            $("#putTailorFemaleHere").html(html);
        }
    });
}
function checkIfTailorItems(connection, transid, locationid, callback){
    var sql = "SELECT tbltrans_tailor_boy.*,tbltailor.claimdate FROM tbltrans_location INNER JOIN tbltrans_tailor_boy ON "+
    "tbltrans_location.trans_locationid = tbltrans_tailor_boy.locationID INNER JOIN tbltailor ON "+
    "tbltailor.tailorID = tbltrans_tailor_boy.tailorID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
        }else{
            if(rows.length>0){
                callback(true);
            }else{
                sql = "SELECT tbltrans_tailor_girl.*,tbltailor.claimdate FROM tbltrans_location INNER JOIN tbltrans_tailor_girl ON" +
                " tbltrans_location.trans_locationid = tbltrans_tailor_girl.locationID INNER JOIN tbltailor ON"+
                " tbltailor.tailorID = tbltrans_tailor_girl.tailorID INNER JOIN tbltransaction ON tbltransaction.transID ="+
                 " tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
                "AND tbltrans_location.trans_locationid = '"+locationid+"'";
                connection.query(sql, function(err, rows){
                    if(err){
                        console.log(err);
                    }else{
                        if(rows.length > 0){
                            callback(true);
                        }else{
                            callback(false);
                        }
                    }
                });
            }
        }
    });
}
function getTailorID(connection, transid, locationid, callback){
    var sql = "SELECT tbltrans_tailor_boy.*,tbltailor.claimdate, tbltailor.tailorID FROM tbltrans_location INNER JOIN tbltrans_tailor_boy ON "+
    "tbltrans_location.trans_locationid = tbltrans_tailor_boy.locationID INNER JOIN tbltailor ON "+
    "tbltailor.tailorID = tbltrans_tailor_boy.tailorID INNER JOIN tbltransaction ON tbltransaction.transID "+
    "= tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
    "AND tbltrans_location.trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
        }else{
            if(rows[0] === undefined){
                sql = "SELECT tbltrans_tailor_girl.*,tbltailor.claimdate, tbltailor.tailorID FROM tbltrans_location INNER JOIN tbltrans_tailor_girl ON" +
                " tbltrans_location.trans_locationid = tbltrans_tailor_girl.locationID INNER JOIN tbltailor ON"+
                " tbltailor.tailorID = tbltrans_tailor_girl.tailorID INNER JOIN tbltransaction ON tbltransaction.transID ="+
                 " tbltrans_location.transID WHERE tbltransaction.transID = '"+transid+"' "+
                "AND tbltrans_location.trans_locationid = '"+locationid+"'";
                connection.query(sql, function(err, rows, fields){
                    if(err){
                        console.log(err);
                    }else{
                        if(rows[0] === undefined){
 
                            callback(0);
                        }else{
                            callback(rows[0].tailorID);
                        }
                    }
                });
            }else{
                callback(rows[0].tailorID);
            }
        }
    });
}
function readURLTailorMale(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
 
            reader.onload = function (e) {
                $('#maleTailorImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function readURLTailorFemale(input){
        if (input.files && input.files[0]) {
                var reader = new FileReader();
 
                reader.onload = function (e) {
                        $('#femaleTailorImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
        }
    }
function readURLTailorMaleTrans(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
 
            reader.onload = function (e) {
                $('#maleTailorImageTrans').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function readURLTailorFemaleTrans(input){
            if (input.files && input.files[0]) {
                    var reader = new FileReader();
 
                    reader.onload = function (e) {
                            $('#femaleTailorImageTrans').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
            }
        }
function getRentDate(date){
    var now = new Date(date);
    var mydate = new Date(now.setDate(now.getDate() - 1));
    var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
    return thisdate;
}
function getReturnDate(date){
    var now = new Date(date);
    var mydate = new Date(now.setDate(now.getDate() + 1));
    var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
    return thisdate;
}

function updateTransaction(connection, trans){
            var error = false;
            connection.beginTransaction(function(){
            var sql = "UPDATE tbltrans_location SET payment = ?, balance = ?, date = ?, "+
            "time_transacted = ?, event_date_start = ?, event_date_end = ?, deposit = ?, "+
            "downpayment = ?, event_time = ? WHERE trans_locationid = ?";
            var insert1 = [trans.totalpayment, trans.totalpayment, trans.eventdate, trans.eventTime,
                trans.eventdatestart, trans.eventdateend, trans.deposit, trans.downpayment,
                trans.eventTimeStart,trans.locationid];
            sql = mysql.format(sql, insert1);
            connection.beginTransaction(function(err){
                if(err){
                    console.log(err);
                    return;
                }
                /*new Promise((resolve, reject)=>{
                    checkifmaxtransactsreached(connection, trans.eventdatestart, trans.eventdateend, function(check){
                        if(check){
                            reject("max3days");
                        }else{
                            resolve(true);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        checkifmaxtransactsreachedononelocation(connection,  trans.eventdatestart, trans.eventdateend, trans.locationid, function(check){
                            if(check){
                                reject("conflictlocation");
                            }else{
                                resolve(true);
                            }
                        });
                    });
                }).then((res)=>{
                    return*/ 
                    new Promise((resolve, reject)=>{
                        connection.query(sql, function(err){
                            if(err){
                                reject(err);
                            }else{
                                resolve(true);
                            }
                        });
                    //});
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.cake.length >0){
                            sql = "DELETE c FROM tbltrans_cake c INNER JOIN tbltrans_location e ON "+
                            "e.trans_locationid = c.locationID WHERE e.transID = '"+trans.transid+"' "+
                            "AND c.locationID = '"+trans.locationid+"'";
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    sql = "";
                                    trans.cake.forEach(function(cake){
                                        sql += "INSERT INTO tbltrans_cake(locationID, cakeID) VALUES(?, ?); ";
                                        var myinsert = [trans.locationid, cake];
                                        sql = mysql.format(sql, myinsert);
                                    });
                                    connection.query(sql, function(err){
                                        if(err){
                                            reject(err);
                                        }else{
                                            resolve(true);
                                        }
                                    });
                                }
                            });
                        }else{
                            sql = "DELETE c FROM tbltrans_cake c INNER JOIN tbltrans_location e ON "+
                            "e.trans_locationid = c.locationID WHERE e.transID = '"+trans.transid+"' "+
                            "AND c.locationID = '"+trans.locationid+"'";
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(true);
                                }
                            });
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.theme=="--Theme--"){
                            sql = "DELETE t FROM tbltrans_theme t INNER JOIN tbltrans_location l "+
                            "ON t.locationID = l.trans_locationid WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(true);
                                }
                            });
                        }else{
                            sql = "DELETE t FROM tbltrans_theme t INNER JOIN tbltrans_location l "+
                            "ON t.locationID = l.trans_locationid WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                   reject(err);
                                }else{
                                    sql = "INSERT tbltrans_theme(locationID, themeID) VALUES(?,?)";
                                    var myinsert = [trans.locationid, trans.theme];
                                    sql = mysql.format(sql, myinsert);
                                    connection.query(sql, function(err){
                                        if(err){
                                            reject(err);
                                        }else{
                                            resolve(true);
                                        }
                                    });
                                }
                            });
     
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.photographer.photographer == "--Photographer--"){
                            sql  = "DELETE p FROM tbltrans_photography p INNER JOIN tbltrans_location"+
                            " l ON l.trans_locationid = p.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve("no photographer");
                                }
                            });
                        }else{
                            checkForConflictsPhotographer(connection, trans.photographer.photoStartDate, trans.photographer.photoEndDate, 
                                trans.photographer.photographer, function(check){
                                    if(check){
                                        reject("photographer");
                                    }else{
                                        resolve("has photographer")
                                    }
                            });
                            /**/
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(res == "has photographer"){
                            
                            sql  = "DELETE p FROM tbltrans_photography p INNER JOIN tbltrans_location"+
                            " l ON l.trans_locationid = p.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                   reject(err);
                                }else{
                                    sql = "INSERT INTO tbltrans_photography(locationID, photoID, date_start, date_end, "+
                                    "startTime, endTime) VALUES(?,?,?,?,?,?)";
     
                                    var myinsert = [trans.locationid, trans.photographer.photographer, trans.photographer.photoStartDate,
                                        trans.photographer.photoEndDate, trans.photographer.photoStartTime, trans.photographer.photoEndTime];
                                    
                                    sql =  mysql.format(sql, myinsert);
                                    //console.log(sql);
                                    connection.query(sql, function(err){
                                        if(err){
                                            reject(err);
                                        }else{
                                            resolve(true);
                                        }
                                    });
                                }
                            });
                        }else{
                            resolve(true);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.stylist.stylist == '--Stylist--'){
                            sql = "DELETE s FROM tbltrans_stylist s INNER JOIN tbltrans_location "+
                            "l ON  l.trans_locationid = s.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql =  mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve("no stylist");
                                }
                            });
                        }else{
                            sql = "DELETE s FROM tbltrans_stylist s INNER JOIN tbltrans_location "+
                            "l ON  l.trans_locationid = s.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql =  mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    checkForConflictsStylist(connection, trans.stylist.stylistStartDate, 
                                        trans.stylist.stylistEndDate, trans.stylist.stylist, function(check){
                                            if(check){
                                                reject("stylist");
                                            }else{
                                                resolve("has stylist");
                                            }
                                    });
                                }
                            });
     
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(res== "has stylist"){
                            sql = "INSERT INTO tbltrans_stylist(locationID, stylistID, startDate, "+
                            "endDate, startTime, endTime) VALUES(?,?,?,?,?,?)";
                            var myinsert = [trans.locationid, trans.stylist.stylist, trans.stylist.stylistStartDate,
                                trans.stylist.stylistEndDate, trans.stylist.stylistStartTime, trans.stylist.stylistEndTime];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(res[1]);
                                }
                            });
                        }else{
                            resolve(true);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.measurementMale.length > 0){
                            if($("#claimdate").val()==""){
                                reject("no claimdate");
                             }else{
                                getTailorID(connection, trans.transid, trans.locationid, function(tailorid){
                                    if(tailorid == 0){
                                        sql = "INSERT INTO tbltailor(claimdate) VALUES(?)";
                                        var myinsert = [formatDate($("#claimdate").val())];
                                        sql = mysql.format(sql, myinsert);
                                        connection.query(sql, function(err, tailorresult){
                                            if(err){
                                                reject(err);
                                            }else{
                                                sql = "DELETE b FROM tbltrans_tailor_boy b INNER JOIN tbltrans_location"+
                                                " l ON l.trans_locationid = b.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                                var myinsert = [trans.locationid, trans.transid];
                                                sql = mysql.format(sql, myinsert);
                                                connection.query(sql, function(err){
                                                    if(err){
                                                        reject(err);
                                                    }else{
                                                        sql = "";
                                                        trans.measurementMale.forEach(function(row){
                                                            sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
                                                            "hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
                                                            "type, fabric, img, price, locationID) "+
                                                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                            var myinsert = [tailorresult.insertId, row.name, row.neck,
                                                                    row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
                                                            row.backlength, row.inseam, row.outseam, row.hem,
                                                            row.type, row.fabric, row.img, row.price, trans.locationid];
                                                            sql = mysql.format(sql, myinsert);
                                                        });
                                                        connection.query(sql, function(err){
                                                            if(err){
                                                                reject(err);
                                                            }else{
                                                                resolve(["has male", tailorresult.insertId]);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }else{
                                        sql = "UPDATE tbltailor SET claimdate = ? WHERE tailorID = ?";
                                        var myinsert = [formatDate($("#claimdate").val()), tailorid];
                                        sql = mysql.format(sql, myinsert);
                                        connection.query(sql, function(err){
                                            if(err){
                                                reject(err);
                                            }else{
                                                sql = "DELETE b FROM tbltrans_tailor_boy b INNER JOIN tbltrans_location"+
                                                " l ON l.trans_locationid = b.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                                var myinsert = [trans.locationid, trans.transid];
                                                sql = mysql.format(sql, myinsert);
                                                connection.query(sql, function(err){
                                                    if(err){
                                                        reject(err);
                                                    }else{
                                                        sql = "";
                                                        trans.measurementMale.forEach(function(row){
                                                            sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
                                                            "hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
                                                            "type, fabric, img, price, locationID) "+
                                                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                            var myinsert = [tailorid, row.name, row.neck,
                                                                    row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
                                                            row.backlength, row.inseam, row.outseam, row.hem,
                                                            row.type, row.fabric, row.img, row.price, trans.locationid];
                                                            sql = mysql.format(sql, myinsert);
                                                        });
                                                        connection.query(sql, function(err){
                                                            if(err){
                                                                reject(err);
                                                            }else{
                                                                resolve(["has male",tailorid]);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                             }
                        }else{
                            sql = "DELETE b FROM tbltrans_tailor_boy b INNER JOIN tbltrans_location"+
                            " l ON l.trans_locationid = b.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(["no male", 0]);
                                }
                            });
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.measurementFemale.length>0){
                            if(res[0]=="has male"){
                                if(res[1]==0){
                                    sql = "INSERT INTO tbltailor(claimdate) VALUES(?)";
                                    var myinsert = [formatDate($("#claimdate").val())];
                                    sql = mysql.format(sql, myinsert);
                                    connection.query(sql, function(err, tailorresult){
                                        if(err){
                                            reject(err);
                                        }else{
                                            sql = "DELETE b FROM tbltrans_tailor_boy b INNER JOIN tbltrans_location"+
                                            " l ON l.trans_locationid = b.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                            var myinsert = [trans.locationid, trans.transid];
                                            sql = mysql.format(sql, myinsert);
                                            connection.query(sql, function(err){
                                                if(err){
                                                    reject(err);
                                                }else{
                                                    sql = "";
                                                    trans.measurementMale.forEach(function(row){
                                                        sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
                                                        "hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
                                                        "type, fabric, img, price, locationID) "+
                                                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                        var myinsert = [tailorresult.insertId, row.name, row.neck,
                                                                row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
                                                        row.backlength, row.inseam, row.outseam, row.hem,
                                                        row.type, row.fabric, row.img, row.price, trans.locationid];
                                                        sql = mysql.format(sql, myinsert);
                                                    });
                                                    connection.query(sql, function(err){
                                                        if(err){
                                                            reject(err);
                                                        }else{
                                                            resolve(true);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }else{
                                    sql = "DELETE g FROM tbltrans_tailor_girl g INNER JOIN tbltrans_location" +
                                    " l ON l.trans_locationid = g.locationID WHERE l.trans_locationid = ? AND l.transID = ?"
                                    var myinsert = [trans.locationid, trans.transid];
                                    sql = mysql.format(sql, myinsert);
                                    connection.query(sql, function(err){
                                        if(err){
                                            reject(err);
                                        }else{
                                            sql = "";
                                            trans.measurementFemale.forEach(function(row){
                                                sql += "INSERT INTO tbltrans_tailor_girl(tailorID, name, chest, bust, hip, waist,"+
                                                " wrist, shoulder, frontskirt, backskirt, "+
                                                "type, fabric, img, price, locationID) "+
                                                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                var myinsert = [res[1], row.name, row.chest, row.bust,
                                                row.hip, row.waist, row.wrist, row.shoulder,
                                                row.frontskirt, row.backskirt,
                                                row.type, row.fabric, row.img, row.price,trans.locationid];
                                                sql = mysql.format(sql, myinsert);
                                            });
                                            connection.query(sql, function(err){
                                                if(err){
                                                    reject(err);
                                                }else{
                                                    resolve(true)
                                                }
                                            });
                                        }
                                    });
                                }
                            }else{
                                getTailorID(connection, trans.transid, trans.locationid, function(tailorid){
                                    if(tailorid==0){
                                        sql = "INSERT INTO tbltailor(claimdate) VALUES(?)";
                                        var myinsert = [formatDate($("#claimdate").val())];
                                        sql = mysql.format(sql, myinsert);
                                        connection.query(sql, function(err, tailorresult){
                                            if(err){
                                                reject(err);
                                            }else{
                                                sql = "DELETE b FROM tbltrans_tailor_boy b INNER JOIN tbltrans_location"+
                                                " l ON l.trans_locationid = b.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                                var myinsert = [trans.locationid, trans.transid];
                                                sql = mysql.format(sql, myinsert);
                                                connection.query(sql, function(err){
                                                    if(err){
                                                        reject(err);
                                                    }else{
                                                        sql = "";
                                                        trans.measurementMale.forEach(function(row){
                                                            sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
                                                            "hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
                                                            "type, fabric, img, price, locationID) "+
                                                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                            var myinsert = [tailorresult.insertId, row.name, row.neck,
                                                                    row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
                                                            row.backlength, row.inseam, row.outseam, row.hem,
                                                            row.type, row.fabric, row.img, row.price, trans.locationid];
                                                            sql = mysql.format(sql, myinsert);
                                                        });
                                                        connection.query(sql, function(err){
                                                            if(err){
                                                                reject(err);
                                                            }else{
                                                                resolve(true);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }else{
                                        sql = "DELETE g FROM tbltrans_tailor_girl g INNER JOIN tbltrans_location" +
                                        " l ON l.trans_locationid = g.locationID WHERE l.trans_locationid = ? AND l.transID = ?"
                                        var myinsert = [trans.locationid, trans.transid];
                                        sql = mysql.format(sql, myinsert);
                                        connection.query(sql, function(err){
                                            if(err){
                                                reject(err);
                                            }else{
                                                sql = "";
                                                trans.measurementFemale.forEach(function(row){
                                                    sql += "INSERT INTO tbltrans_tailor_girl(tailorID, name, chest, bust, hip, waist,"+
                                                    " wrist, shoulder, frontskirt, backskirt, "+
                                                    "type, fabric, img, price, locationID) "+
                                                    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                                    var myinsert = [tailorid, row.name, row.chest, row.bust,
                                                    row.hip, row.waist, row.wrist, row.shoulder,
                                                    row.frontskirt, row.backskirt,
                                                    row.type, row.fabric, row.img, row.price,trans.locationid];
                                                    sql = mysql.format(sql, myinsert);
                                                });
                                                connection.query(sql, function(err){
                                                    if(err){
                                                        reject(err);
                                                    }else{
                                                        resolve(true)
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }else{
                            sql = "DELETE g FROM tbltrans_tailor_girl g INNER JOIN tbltrans_location" +
                            " l ON l.trans_locationid = g.locationID WHERE l.trans_locationid = ? AND l.transID = ?"
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(true);
                                }
                            });
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(trans.itemsrented.length>0){
                            sql = "UPDATE tblinventory AS i INNER JOIN tbltrans_rent AS r ON i.itemcode = "+
                            "r.itemcode INNER JOIN tbltrans_location l ON l.trans_locationid = r.locationID "+
                            "SET i.quantity = (i.availableItems + r.qty) WHERE l.trans_locationid = ? AND l.transID = ?";
                            var myinsert = [trans.locationid, trans.transid];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    sql = "DELETE r FROM tbltrans_rent r INNER JOIN tbltrans_location "+
                                    "l ON l.trans_locationid = r.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                    var myinsert = [trans.locationid, trans.transid];
                                    sql = mysql.format(sql, myinsert);
                                    connection.query(sql, function(){
                                        if(err){
                                           reject(err);
                                        }else{
                                            sql = "";
                                            trans.itemsrented.forEach(function(row){
                                                sql += "UPDATE tblinventory SET availableItems = availableItems - "+row.quantity+" WHERE itemcode = '"+row.itemcode+"'; ";
                                            });
                                            trans.itemsrented.forEach(function(row){
                                                sql += "INSERT INTO tbltrans_rent(locationID, itemcode, qty, rentdate, returndate) "+
                                                            "VALUES('"+trans.locationid+"', '"+row.itemcode+"', '"+row.quantity+"', '"+trans.rentStart+"', '"+trans.rentEnd+"'); ";
                                            });
                                            connection.query(sql, function(err){
                                                if(err){
                                                    reject(err);
                                                }else{
                                                    resolve(true);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }else{
                            checkIfItemsOnDatabase(connection, trans.transid, trans.locationid, function(check){
                                if(check==true){
                                    sql = "UPDATE tblinventory AS i INNER JOIN tbltrans_rent AS r ON i.itemcode = "+
                                    "r.itemcode INNER JOIN tbltrans_location l ON l.trans_locationid = r.locationID "+
                                    "SET i.quantity = (i.availableItems + r.qty) WHERE l.trans_locationid = ? AND l.transID = ?";
                                    var myinsert = [trans.locationid, trans.transid];
                                    sql = mysql.format(sql, myinsert);
                                    connection.query(sql, function(err){
                                        if(err){
                                            reject(err);
                                        }else{
                                            sql = "DELETE r FROM tbltrans_rent r INNER JOIN tbltrans_location "+
                                            "l ON l.trans_locationid = r.locationID WHERE l.trans_locationid = ? AND l.transID = ?";
                                            var myinsert = [trans.locationid. trans.transid];
                                            connection.query(sql, function(err){
                                                if(err){
                                                    reject(err);
                                                }else{
                                                    resolve(true);
                                                }
                                            });
                                        }
                                    });
                                }else{
                                    resolve(true);
                                }
                            });
                        }
                    });
                }).then((res)=>{
                    connection.commit(function(err){
                        if(err){
                            connection.rollback(function(err){
                                if(err){
                                    throw err;
                                }
                            });
                        }else{
                            alert("Saved!");
                        }
                    });
                }).catch((err)=>{
                    console.log(err);
                    if(err=="photographer"){
                        alert("Conflict of dates on photographer!");
                    }else if(err == "stylist"){
                        alert("Conflict of dates on stylist!");
                    }else if(err== "conflictlocation"){
                        alert("Conflict of event on same date and same place!");
                    }else if(err =="max3days"){
                        alert("There should only be max of 3 events on a day!");
                    }else if(err=="no claimdate"){
                        alert("Please provide the claimdate for the tailor!");
                    }else{
                        alert("Something went wrong. please try again later!");
                    }
                    connection.rollback(function(err){
                        if(err){
                            throw err;
                        }
                    });
                });
            });
 
        });
}
function getRentDate(date){
    var now = new Date(date);
    var mydate = new Date(now.setDate(now.getDate() - 1));
    var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
    return thisdate;
    //$("#claimdate").val(thisdate);
}
function getReturnDate(date){
    var now = new Date(date);
    var mydate = new Date(now.setDate(now.getDate() + 1));
    var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
    return thisdate;
}
function loadLocationForAdd(connection, transid){
    var sql = "SELECT l.* FROM tbllocation l WHERE NOT EXISTS (SELECT t.* "+
    "FROM tbllocation t INNER JOIN tbltrans_location q ON t.locationID = "+
    "q.locationID INNER JOIN tbltransaction a ON q.transID = a.transID WHERE "+
    "q.transID = '"+transid+"' AND t.locationID = l.locationID)";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the location. Please try again later!");
        }else{
            var html = "";
            rows.forEach(function(row){
                html +="<tr id = 'location-"+row.locationID+"'>";
                html += "<td>"+row.locationname+"</td>";
                html += "<td><a class='waves-effect waves-light btn addlocation' id = 'addlocation-"+row.locationID+"'>Add</a></td>";
                html += "</tr>";
            });
            $("#putLocationHere").html(html);
        }
        connection.end();
    });
}
function loadRemoveLocation(connection, transid){
    var sql = "SELECT t.*, q.trans_locationid "+
    "FROM tbllocation t INNER JOIN tbltrans_location q ON t.locationID = "+
    "q.locationID INNER JOIN tbltransaction a ON q.transID = a.transID WHERE "+
    "q.transID = '"+transid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the location. Please try again later!");
        }else{
            var html = "";
            rows.forEach(function(row){
                html +="<tr id = 'locationremove-"+row.locationID+"' data-value = '"+row.trans_locationid+"'>";
                html += "<td>"+row.locationname+"</td>";
                html += "<td><a class='waves-effect waves-light btn removelocation' data-value = '"+row.trans_locationid+"' id = 'removelocation-"+row.locationID+"'>Remove</a></td>";
                html += "</tr>";
            });
            $("#putLocationHere").html(html);
        }
        connection.end();
    });
}
function loadLocation(connection){
    var sql = "SELECT locationID, locationname FROM tbllocation";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the locations. Please try again later!");
        }else{
            var html = "";
            rows.forEach(function(row){
                html += "<option value = '"+row.locationID+"'>"+row.locationname+"</option>";
            });
            $("#selectedLocationTrans").html(html);
            $("#selectedLocationTrans").material_select();
        }
        //connection.end();
    });
}
function myloadTheme(connection){
    var sql = "SELECT themeID, themename, description FROM tbltheme";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong loading the themes, please try again later!");
        }else{
            var html = "<option value = '--Theme--' selected>--Theme--</option>";
            rows.forEach(function(row){
                html += "<option value = '"+row.themeID+"'>"+row.themename+" "+row.description+"</option>";
            });
            $("#themeTrans").html(html);
            $("#themeTrans").material_select();
        }
    });
}
function myloadCake(connection){
    var sql = "SELECT cakeID, cakename, price FROM tblcake";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading cakes, please try again later!");
        }else{
            var html = "<option disabled selected>--Cake--</option>";
            rows.forEach(function(row){
                html += "<option value = '"+row.cakeID+"'>"+row.cakename+" ("+row.price+")</option>";
            });
            $("#cakeTrans").html(html);
            $("#cakeTrans").material_select();
        }
    });
}
function myloadPhotographer(connection){
    var sql = "SELECT photoID, firstname, lastname, fee FROM tblphotographer";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the photographers. Please try again later!");
        }else{
            var html = "<option value = '--Photographer--' selected>--Photographer--</option>";
            rows.forEach(function(row){
                html += "<option value = '"+row.photoID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
            });
            $("#photographerTrans").html(html);
            $("#photographerTrans").material_select();
        }
    });
}
function myloadStylist(connection){
    var sql = "SELECT stylistID, firstname, lastname, fee FROM tblstylist";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the stylists. Please try again later!");
        }else{
            var html = "<option value = '--Stylist--' selected>--Stylist--</option>";
            rows.forEach(function(row){
                html += "<option value = '"+row.stylistID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
            });
            $("#stylistTrans").html(html);
            $("#stylistTrans").material_select();
        }
    });
}
function insertTransaction(connection, cust, callback){
 
            sql = "INSERT INTO tbltrans_location(locationID, transID, payment, balance, date, time_transacted, "+
            "event_date_start, event_date_end, deposit, downpayment, event_time, eventTimeEnd) VALUES(?,?,?,?,?,?,?,?,?,?, ?, ?)";
            var insert2 = [cust.location, cust.transid, cust.totalPayment, (cust.totalPayment-cust.downpayment), cust.eventDateTransacted,
                cust.eventTimeTransacted, cust.eventDateStart, cust.eventDateEnd, cust.depositTransact, cust.downpayment, cust.eventtimestart, cust.eventtimeend];
            sql = mysql.format(sql, insert2);
            connection.beginTransaction(function(err){
                if(err){
                    console.log(err);
                    return;
                }
               
                 new Promise((resolve, reject)=>{
                    checkifmaxtransactsreached(connection, cust.eventDateStart, cust.eventDateEnd, function(check){
                        if(check){
                            reject("max3days");
                        }else{
                            resolve(true);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        checkifmaxtransactsreachedononelocation(connection,  cust.eventDateStart, cust.eventDateEnd, cust.location, function(check){
                            if(check){
                                reject("conflictlocation");
                            }else{
                                resolve(true);
                            }
                        });
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        connection.query(sql, function(err1, result1){
                            if(err1){
                                reject(err);
                            }else{
                                resolve(result1.insertId);
                            }
                        });
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(cust.itemsrented.length>0){
                            sql = "";
                            cust.itemsrented.forEach(function(row){
                                sql += "UPDATE tblinventory SET availableItems = availableItems - "+row.quantity+" WHERE itemcode = '"+row.itemcode+"'; ";
                            });
                            cust.itemsrented.forEach(function(row){
                                sql += "INSERT INTO tbltrans_rent(locationID, itemcode, qty, rentdate, returndate) "+
                                            "VALUES('"+res+"', '"+row.itemcode+"', '"+row.quantity+"', '"+cust.rentStart+"', '"+cust.rentEnd+"'); ";
                            });
                            connection.query(sql, function(err, result){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(res);
                                }
                            });
                        }else{
                            resolve(res);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(cust.measurementMale.length>0 || cust.measurementFemale.length>0){
                            sql = "INSERT INTO tbltailor(claimdate) VALUES(?)";
                            insert3=[formatDate($("#claimdate").val())];
                            sql = mysql.format(sql, insert3);
                            connection.query(sql, function(err, result3){
                                if(err){
                                    reject(err);
                                }else{
                                    sql = "";
                                    if(cust.measurementMale.length>0){
                                        cust.measurementMale.forEach(function(row){
     
                                            sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
                                            "hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
                                            "type, fabric, img, price, locationID) "+
                                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                            var myinsert = [result3.insertId, row.name, row.neck,
                                                    row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
                                            row.backlength, row.inseam, row.outseam, row.hem,
                                            row.type, row.fabric, row.img, row.price, res];
                                            sql = mysql.format(sql, myinsert);
                                        });
                                    }
                                    if(cust.measurementFemale.length>0){
                                        cust.measurementFemale.forEach(function(row){
     
                                            sql += "INSERT INTO tbltrans_tailor_girl(tailorID, name, chest, bust, hip, waist,"+
                                            " wrist, shoulder, frontskirt, backskirt, "+
                        "type, fabric, img, price, locationID) "+
                                            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
                                            var myinsert = [result3.insertId, row.name, row.chest, row.bust,
                                            row.hip, row.waist, row.wrist, row.shoulder,
                                            row.frontskirt, row.backskirt,
                                            row.type, row.fabric, row.img, row.price,res];
                                            sql = mysql.format(sql, myinsert);
                                        });
                                    }
                                    if(sql != ""){
                                        connection.query(sql, function(err){
                                            if(err){
                                               reject(err);
                                            }else{
                                                resolve(res)
                                            }
                                        });
                                    }else{
                                        resolve(res);
                                    }
                                }
                            });
                        }else{
                            resolve(res);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        sql = "";
                        if(cust.cake.length>0){
                            cust.cake.forEach(function(row){
                                sql += "INSERT INTO tbltrans_cake(locationID, cakeID) VALUES(?,?); ";
                                var myinsert = [res, row];
                                sql = mysql.format(sql, myinsert);
                            });
                            connection.query(sql, function(err, result4){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(res);
                                }
                            });
                        }else{
                            resolve(res);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(!(cust.photographer.photographer===null)){
                            checkForConflictsPhotographer(connection, cust.photographer.photoStartDate, cust.photographer.photoEndDate, 
                                cust.photographer.photographer, function(check){
                                    if(check){
                                        reject("photographer");
                                    }else{
                                        resolve(["has photographer", res])
                                    }
                            });
                        }else{
                            resolve(["no photohrapher", res]);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(res[0]=="has photographer"){
                            sql = "INSERT INTO tbltrans_photography(locationID, photoID, date_start, date_end, "+
                            "startTime, endTime) VALUES(?,?,?,?,?,?)";
                            var myinsert = [res[1], cust.photographer.photographer, cust.photographer.photoStartDate,
                                cust.photographer.photoEndDate, cust.photographer.photoStartTime, cust.photographer.photoEndTime];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(res[1]);
                                }
                            });
                        }else{  
                            resolve(res[1]);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve, reject)=>{
                        if(!(cust.stylist.stylist===null)){
                            checkForConflictsStylist(connection, cust.stylist.stylistStartDate, 
                                cust.stylist.stylistEndDate, cust.stylist.stylist, function(check){
                                    if(check){
                                        reject("stylist");
                                    }else{
                                        resolve(["has stylist", res]);
                                    }
                            });
                        }else{
                            resolve(["no stylist", res]);
                        }
                    });
                }).then((res)=>{
                    return new Promise((resolve,reject)=>{
                        if(res[0]=="has stylist"){
                            sql = "INSERT INTO tbltrans_stylist(locationID, stylistID, startDate, "+
                            "endDate, startTime, endTime) VALUES(?,?,?,?,?,?)";
                            var myinsert = [res[1], cust.stylist.stylist, cust.stylist.stylistStartDate,
                                cust.stylist.stylistEndDate, cust.stylist.stylistStartTime, cust.stylist.stylistEndTime];
                            sql = mysql.format(sql, myinsert);
                            connection.query(sql, function(err){
                                if(err){
                                    reject(err);
                                }else{
                                    resolve(res[1]);
                                }
                            });
                        }else{
                            resolve(res[1]);
                        }
                    });
                }).then((res)=>{
                    if(!(cust.theme===null)){
                        sql = "INSERT INTO tbltrans_theme(locationID, themeID) VALUES(?,?)";
                        var myinsert = [res, cust.theme];
                        sql = mysql.format(sql, myinsert);
                        connection.query(sql, function(err){
                            if(err){
                                reject(err);
                            }else{
                                connection.commit(function(err){
                                    if(err){
                                        reject(err);
                                    }
                                    callback(connection);
                                });
                            }
                        });
                    }else{
                        connection.commit(function(err){
                            if(err){
                                reject(err);
                            }
                            callback(connection);
                        });
                    }
                }).catch((err)=>{
                    console.log(err);
                    if(err=="photographer"){
                        alert("Conflict of dates on photographer!");
                    }else if(err == "stylist"){
                        alert("Conflict of dates on stylist!");
                    }else if(err== "conflictlocation"){
                        alert("Conflict of event on same date and same place!");
                    }else if(err =="max3days"){
                        alert("There should only be max of 3 events on a day!");
                    }else{
                        alert("Something went wrong. please try again later!");
                    }
                    connection.rollback(function(err){
                        if(err){
                            console.log(err);
                            throw err;
                        }
                    });
                    callback(connection);
                });
            }); 
}
function getCustomer(connection, transid, callback){
    var sql = "SELECT custID FROM tbltransaction  WHERE transID = '"+transid+"'";
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            callback(rows[0].custID);
        }
    });
}
function getDeposit(connection, locationid){
    var sql = "SELECT deposit FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            throw err;
        }else{
            $("#deposit").val(rows[0].deposit);
        }
    });
}
function getTimeTransacted(connection, locationid){
    var sql = "SELECT time_transacted FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            throw err;
        }else{
            $("#eventTime").val(rows[0].time_transacted);
        }
    });
}
function getDateTransacted(connection, locationid){
    var sql = "SELECT date FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            throw err;
        }else{
            $("#eventDate").val(getDateFormat(rows[0].date.toString().slice(0,15)));
        }
    });
}
function deleteLocation(connection, translocationid, callback){
    var sql = "DELETE FROM tbltrans_theme WHERE locationID = '"+translocationid+"'";
    var error = false;
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            //callback(connection, true);
            error = true;
        }
    });
    sql = "DELETE FROM tbltrans_cake WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            //callback(connection, true);
            error = true;
        }
    });
    sql = "DELETE FROM tbltrans_photography WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            //callback(connection, true);
            error = true;
        }
    });
    sql = "DELETE FROM tbltrans_stylist WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            //callback(connection, true);
            error = true;
        }
    });
    sql = "DELETE FROM tbltrans_rent WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            error = true;
            //callback(connection, true);
        }
    });
    sql = "DELETE FROM tbltrans_tailor_boy WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            error = true;
            //callback(connection, true);
        }
    });
    sql = "DELETE FROM tbltrans_tailor_girl WHERE locationID = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            connection.rollback(function(){
                throw err;
            });
            error = true;
            //callback(connection, true);
        }
    });
    callback(connection, error);
}
function deleteMyLocation(connection, translocationid, callback){
    var sql = "DELETE FROM tbltrans_location WHERE trans_locationid = '"+translocationid+"'";
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
            connection.rollback(function(){
                throw err;
            });
            callback(connection);
        }else{
            alert("Successfully deleted!");
            callback(connection);
        }
    });
}
function getPayment(connection, translocationid){
    var sql = "SELECT deposit, balance, downpayment, payment FROM tbltrans_location WHERE "+
    "trans_locationid = '"+translocationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            //console.log(rows[0].payment);
            $("#totalPaymentss").val(rows[0].payment);
            $("#amountDeposit").val(rows[0].deposit);
            $("#currentBalance").val(rows[0].balance);
            $("#payBalance").val("");
            $(".paymentLabel").addClass('active');
        }
    });
}
function getRent(connection, transid, locationid){
    var sql ="SELECT rentdate, returndate FROM tbltrans_rent where locationID = '"+ locationid +"'";
     connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong while loading the rents. Please try again later!");
        }else{
            if(rows.length < 1){
                var dt1 = new Date($("#eventDateStart").val());
                var mydate1 = new Date(dt1.setDate(dt1.getDate())-1);
                var thisdate1 = mydate1.getDate()+" "+getMonthName(mydate1.getMonth()+1)+", "+mydate1.getFullYear();
                $('#rentstartdate').val(thisdate1);
                var dt2 = new Date($("#eventDateEnd").val());
                var mydate2 = new Date(dt2.setDate(dt2.getDate())+ 1);
                var checkdate = new Date(mydate2.getFullYear(), mydate2.getMonth()+1, 0);
                if (checkdate.getDate()==dt2.getDate()){
                    var thisdate2 = 1+" "+getMonthName(mydate2.getMonth()+2)+", "+mydate2.getFullYear();
                }
                else{
                    var thisdate2 = (mydate2.getDate()+1)+" "+getMonthName(mydate2.getMonth()+1)+", "+mydate2.getFullYear();
                }
                $('#rentenddate').val(thisdate2);   
            }
            else{
                var dt1 = new Date(rows[0].rentdate);
                var mydate1 = new Date(dt1.setDate(dt1.getDate())+1);
                var thisdate1 = mydate1.getDate()+" "+getMonthName(mydate1.getMonth()+1)+", "+mydate1.getFullYear();
                var dt2 = new Date(rows[0].returndate);
                var mydate2 = new Date(dt2.setDate(dt2.getDate())+1);
                var thisdate2 = mydate2.getDate()+" "+getMonthName(mydate2.getMonth()+1)+", "+mydate2.getFullYear();
                $("#rentstartdate").val(thisdate1);
                $("#rentenddate").val(thisdate2);

                
            }
        }
    });
}
function checkIfDepositReturned(connection, translocationid, callback){
    var sql = "SELECT depositReturned FROM tbltrans_location WHERE trans_locationid = '"+translocationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            if(rows[0].depositReturned != "false"){
                callback(true);
            }else{
                callback(false);
            }
        }
    });
}
function returnDeposit(connection, translocationid){
    var sql = "UPDATE tbltrans_location SET depositReturned = 'true', depositReturnedDate = CURDATE() WHERE trans_locationid = ?";
    var insert = [translocationid];
    sql = mysql.format(sql, insert);
    connection.beginTransaction(function(err){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            connection.query(sql, function(err){
                if(err){
                    alert("Something went wrong. Please try again later!");
                    console.log(err);
                    $("#returnDepositss").attr("disabled", false);
                }else{
                    returnItems(connection, translocationid);
                }
            });
        }
    });
}
function returnItems(connection, translocationid){
    var sql = "UPDATE tblinventory AS i INNER JOIN tbltrans_rent AS r ON i.itemcode = "+
    "r.itemcode INNER JOIN tbltrans_location l ON l.trans_locationid = r.locationID "+
    "SET i.quantity = (i.quantity + r.qty) WHERE l.trans_locationid = ?";
    var insert = [translocationid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err){
        if(err){
            console.log("Something went wrong. Please try again later!");
            connection.rollback(function(){
                throw err;
            });
        }else{
            connection.commit(function(err){
                if(err){
                    connection.rollback(function(){
                        throw err;
                        connection.end();
                    });
 
                }else{
                    alert("Successfully updated!");
                    $("#returnDepositss").attr("disabled", true);
                    connection.end();
                }
            });
        }
    });
}
function checkItemClaimed(connection, translocationid, callback){
    var sql = "SELECT itemClaimed FROM tbltrans_location WHERE trans_locationid = ?";
    var insert = [translocationid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            callback(true);
        }else{
            if(rows[0].itemClaimed == 'true'){
                callback(true);
            }else{
                callback(false);
            }
        }
    });
}
function claimItem(connection, translocationid){
    var sql = "UPDATE tbltrans_location SET itemClaimed = 'true', itemClaimedDate = CURDATE() WHERE trans_locationid = ?";
    var insert = [translocationid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
            $("#claimItem").attr("disabled", false);
        }else{
            alert("Successfully updated!");
            $("#claimItem").attr("disabled", true);
        }
        connection.end();
    });
}
function updatePayments(connection, translocationid, obj){
    var sql = "UPDATE tbltrans_location SET balance = balance - ?, downpayment = ?, "+
    "deposit = ?,  payment = ?  WHERE trans_locationid = ?";
    var insert = [obj.balance, obj.downpayment, obj.deposit, obj.payment, obj.locationid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            alert("Successfully Updated!");
            $("#editPaymentModal").modal("close");
        }
        connection.end();
    });
}
function notReturnedItem(connection, transid, callback){
    var sql = "UPDATE tbltrans_location SET depositReturned = 'false' WHERE trans_locationid = ?";
    var insert = [transid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            alert("Successfully updated!");
        }
        connection.end();
        callback(connection);
    });
}
function checkIfTransHaveBalance(connection, transid, callback){
    var sql = "SELECT l.balance FROM tbltrans_location l "+
    "INNER JOIN tbltransaction s ON s.transID = l.transID WHERE s.transID = ? AND l.balance > 0";
    var insert = [transid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            callback(false);
        }else{
            if(rows.length>0){
                callback(true);
            }else{
                callback(false);
            }
        }
    });
}
function checkIfItemsReturned(connection, transid, callback){
    /*var sql = "SELECT l.depositReturned, t.transID FROM tbltrans_location l INNER JOIN tbltransaction t ON t.transID = l.transID"+
                        " WHERE t.transID = ?";*/
        var sql = "SELECT r.*, i.itemcode2, i.price FROM tbltrans_rent r INNER JOIN tblinventory i "+
        "ON i.itemcode = r.itemcode INNER JOIN tbltrans_location l on r.locationID = "+
        "l.trans_locationid INNER JOIN tbltransaction t ON t.transID = l.transID WHERE t.transID = ?";
    var insert = [transid];
    sql  =  mysql.format(sql, insert);
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            var checker = true;
            rows.forEach(function(row){
               
                if(parseFloat(row.qty) > parseFloat(0)){
                    checker = false;
                }
            });
            callback(checker);
        }
    });
}
function checkIfItemsReturnedLocationId(connection, locationid, callback){
    /*var sql = "SELECT l.depositReturned, t.transID FROM tbltrans_location l INNER JOIN tbltransaction t ON t.transID = l.transID"+
                        " WHERE t.transID = ?";*/
        var sql = "SELECT r.*, i.itemcode2, i.price FROM tbltrans_rent r INNER JOIN tblinventory i "+
        "ON i.itemcode = r.itemcode INNER JOIN tbltrans_location l on r.locationID = "+
        "l.trans_locationid WHERE l.trans_locationid = ?";
    var insert = [locationid];
    sql  =  mysql.format(sql, insert);
    //console.log(sql);
    connection.query(sql, function(err, rows, fields){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
        }else{
            var checker = true;
            rows.forEach(function(row){
                if(parseFloat(row.qty) > parseFloat(0)){
                    checker = false;
                }
            });
            callback(checker);
        }
    });
}
function getLocationsForDelete(connection, transid, callback){
    var sql = "SELECT trans_locationid FROM tbltrans_location WHERE transID = ?";
    var insert = [transid];
    sql =  mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            callback([]);
        }else{
            callback(rows)
        }
    });
}
function deleteMyTransaction(connection, transid){
    var sql = "DELETE FROM tbltransaction WHERE transID = ?";
    var insert = [transid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err){
        if(err){
            console.log(err);
        }else{
            //alert("Successfully deleted!");
            $("#trans-"+transid).remove();
        }
    });
}
function initPayment(){
    var payment = 0;
   
    $('#addItemsHere > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[2]);
    });
    $('#putTailorMaleHere > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[15]);
    });
    var measurementFemale = [];
    $('#putTailorFemaleHere > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[12]);
    });
    var cake = $("#cake").val();
    //console.log(cake);
    var firstc;
    var secc;
    var mycake;
    cake.forEach(function(row){
        mycake= $("#cake option[value='"+row+"']").text();
        console.log($("#cake option[value='"+row+"']").text())
        mycake = mycake.split(" ")[1];
 
        firstc = mycake.replace("(", "");
        secc = firstc.replace(")", "");
        payment = parseFloat(payment) + parseFloat(secc);
    });
    //console.log($("#stylist option[value='"+$("#stylist").val()+"']").text().split(" ")[2].replace("(", ""));
    //return;
    if($("#photographer").val()!="--Photographer--"){
        var tempphot = $("#photographer option[value='"+$("#photographer").val()+"']").text().split(" ")[2].replace("(", "");
        tempphot = tempphot.replace(")", "");
        payment = parseFloat(payment) + parseFloat(tempphot);
    }else{
        payment = parseFloat(payment) + parseFloat(0);
    }
    if($("#stylist").val()!="--Stylist--"){
        var tempstylist = $("#stylist option[value='"+$("#stylist").val()+"']").text().split(" ")[2].replace("(", "");
        tempstylist = tempstylist.replace(")", "");
        payment = parseFloat(payment) + parseFloat(tempstylist);
    }else{
        payment = parseFloat(payment) + parseFloat(0);
    }
 
    $("#totalPayment").val(payment);
    $("#totalPaymentLabel").addClass('active');
}
function initPaymentTrans(){
    var payment = 0;
    //console.log("Hello")
    $('#addItemsHereTrans > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[2]);
    });
    $('#putTailorMaleHereTrans > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[15]);
    });
    var measurementFemale = [];
    $('#putTailorFemaleHereTrans > tr').each(function(){
        i = 0;
        product = [""];
        $(this).closest('tr').each(function(){
            $(this).find('td').each(function(){
                product[i]=$(this).html();
                i++;
            });
        });
        payment = parseFloat(payment) + parseFloat(product[12]);
    });
    var cake = $("#cakeTrans").val();
    //console.log(cake);
    var firstc;
    var secc;
    var mycake;
    cake.forEach(function(row){
        mycake= $("#cakeTrans option[value='"+row+"']").text();
        mycake = mycake.split(" ")[1];
 
        firstc = mycake.replace("(", "");
        secc = firstc.replace(")", "");
        payment = parseFloat(payment) + parseFloat(secc);
    });
    if($("#photographerTrans").val()!="--Photographer--"){
        var tempphot = $("#photographerTrans option[value='"+$("#photographerTrans").val()+"']").text().split(" ")[2].replace("(", "");
        tempphot = tempphot.replace(")", "");
        payment = parseFloat(payment) + parseFloat(tempphot);
    }
    if($("#stylistTrans").val()!="--Stylist--"){
        var tempstylist = $("#stylistTrans option[value='"+$("#stylistTrans").val()+"']").text().split(" ")[2].replace("(", "");
        tempstylist = tempstylist.replace(")", "");
        payment = parseFloat(payment) + parseFloat(tempstylist);
    }
 
    $("#totalPaymentTrans").val(payment);
    $("#totalPaymentLabelTrans").addClass('active');
}
function loadItemsRented(connection, locationid){
    var sql = "SELECT r.*, i.itemcode2, i.price FROM tbltrans_rent r INNER JOIN tblinventory i"+
    " ON i.itemcode = r.itemcode INNER JOIN tbltrans_location l on r.locationID = "+
    "l.trans_locationid WHERE r.locationID = ?";
    var insert = [locationid];
    sql =  mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        //console.log(rows)
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!")
        }else{
            var html = "";
            rows.forEach(function(row){
                html += "<tr id = 'returneditem-"+row.itemcode+"' data-value = '"+row.rentID+"'><td>"+row.itemcode2+"</td>"+
                "<td>0</td>"+
                "<td>"+row.price+"</td>"+
                "<td><a class='waves-effect waves-light btn returneditem' id = 'returneditem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
                +"</tr>";
            });
            $("#loadItemsRentedInventoryHere").html(html);
        }
    });
}
function loadItemsForReturn(connection, locationid){
    var sql = "SELECT r.*, i.itemcode2, i.price FROM tbltrans_rent r INNER JOIN tblinventory i"+
    " ON i.itemcode = r.itemcode INNER JOIN tbltrans_location l on r.locationID = "+
    "l.trans_locationid WHERE r.locationID = ?";
    var insert = [locationid];
    sql =  mysql.format(sql, insert);
    connection.query(sql, function(err, rows){
        //console.log(rows)
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!")
        }else{
            var html = "";
            rows.forEach(function(row){
                html += "<tr id = 'returnitem-"+row.itemcode+"'><td>"+row.itemcode2+"</td>"+
                "<td>"+row.qty+"</td>"+
                "<td>"+(row.price*row.qty)+"</td>"+
                "<td><a class='waves-effect waves-light btn returnitem' id = 'returnitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
                +"<td><input type = 'number' id = 'returnitemText-"+row.itemcode+"' class = 'returnitemText' value = '0'></td>"
                +"</tr>";
            });
            $("#loadItemsReturnedIventoryHere").html(html);
        }
    });
}
function getDepositOnHand(connection, locationid){
    var sql = "SELECT deposit FROM tbltrans_location WHERE trans_locationid = '"+locationid+"'";
    connection.query(sql, function(err, rows){
        if(err){
            throw err;
        }else{
            $("#depositOnHandInventory").val(rows[0].deposit);
        }
    });
}
function returnItemsIventory(connection, locationid, items, allitemsreturned, deposit){
    
    var sql = "";
    var myinsert = [];
    var available = 0;
   
    items.forEach(function(row){
        sql +="UPDATE tblinventory SET availableItems = availableItems + ?, damagedItems = damagedItems + ? WHERE itemcode2 = ?; "+
                    "UPDATE tbltrans_rent SET qty = qty - ? WHERE rentID = ?; ";
                    available = row.quantity - row.damagedItems;
                    if(available < 0){
                        available = 0;
                    }
        myinsert = [available, row.damagedItems, row.itemcode, row.quantity, row.rentid];
        sql = mysql.format(sql, myinsert);
    });
    console.log(sql);
    connection.beginTransaction(function(err){
        if(err){
            console.log(err);
            alert("Something went wrong please try again later!");
            return;
        }else{
            connection.query(sql, function(err){
                if(err){
                    console.log(err);
                    alert("Something went wrong. Please try again later!");
                    connection.rollback(function(){
                        throw err;
                    });
                }else{
                    //alert("hrllo");
                    returnDepositInventory(connection, locationid, deposit, allitemsreturned)
                }
            });
        }
    });
}
function returnDepositInventory(connection, locationid, deposit, allitemsreturned){
    var sql = "";
    if(allitemsreturned==false){
     sql = "UPDATE tbltrans_location SET depositReturnedValue = depositReturnedValue + ? WHERE trans_locationid = ?";
    }else{
        sql = "UPDATE tbltrans_location SET depositReturnedValue = depositReturnedValue + ?, depositReturned = 'true', depositReturnedDate = CURDATE() "+
        "WHERE trans_locationid = ?";
    }
    var insert = [deposit, locationid];
    sql = mysql.format(sql, insert);
    connection.query(sql, function(err){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again later!");
            connection.rollback(function(){
                throw err;
                connection.commit(function(err){
                    if(err){
                        connection.rollback(function(){
                            throw err;
                        });
                    }
                });
            });
        }else{
            connection.commit(function(err){
                if(err){
                    connection.rollback(function(){
                        throw err;
                    });
                }else{
                    
                    alert("Successfully updated!");
                    $("#returnItemsModal").modal("close");
                }
            });
        }
    });
}
function getEventTimeStart(connection, locationid){
    var sql = "SELECT event_time FROM tbltrans_location WHERE trans_locationid = ?";
    var insert = [locationid];
    sql = mysql.format(sql, insert);
    //console.log("Hello");
    connection.query(sql, function(err, rows){
        if(err){
            console.log(err);
            alert("Something went wrong. Please try again alter!");
        }else{
            $("#eventTimeStart").val(rows[0].event_time);
        }
    });
}
function checkForConflictsPhotographer(connection, startdate, enddate, photoid, callback){
	var sql = "SELECT * FROM tbltrans_photography WHERE ((? >= date_start AND"+
	" ? <= date_end) OR (? >= date_start AND ? <= date_end) "+
	"OR (date_start >= ? AND date_end <= ?)) AND photoID = ?";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate, photoid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			throw err;
		}else{
			if(rows.length>0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function loadPackagesTrans(connection){
	var sql = "SELECT packageName, packageID FROM package";
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			var html = "";
			rows.forEach(function(row){
				html +="<option value = '"+row.packageID+"'>"+row.packageName+"</option>";
			});
			$("#rentpackageTrans").html(html);
			$('#rentpackageTrans').material_select();
		}
	});
}
function loadPackages(connection){
	var sql = "SELECT packageName, packageID FROM package";
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			var html = "";
			rows.forEach(function(row){
				html +="<option value = '"+row.packageID+"'>"+row.packageName+"</option>";
			});
			$("#rentpackage").html(html);
			$('#rentpackage').material_select();
		}
	});
}
function getPackagePriceTrans(connection, packageID){
	var sql= "SELECT price FROM package WHERE packageID = ?";
	var insert = [packageID];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, row){
		if(err){
			console.log(err);
		}else{
			$("#packagePriceTrans").val(row[0].price);
			$(".userLabel").addClass('active');
		}
	});
	sql = "SELECT i.itemcode, h.itemQuantity, i.itemcode2 FROM packagehistory "+
	"h INNER JOIN package p ON p.packageID = h.packageID INNER JOIN "+
	"tblinventory i ON i.itemcode = h.inventoryID WHERE p.packageID = ?";
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			new Promise((resolve, reject)=>{
				getItemsForValidation(connection, function(rows){
					//resolve(rows);
					var thiscounter = 0;
					rows.forEach(function(row, index, array){
						getReservedItem(connection, row.itemcode, function(qty){
							rows[index].quantity = returnAbs(rows[index].quantity, qty, rows[index].damagedItems);
							thiscounter++;
							if(thiscounter==array.length){
								resolve(rows);
							}
						});
					});
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					//console.log(rows);
					var mycounter = 0;
					res.forEach(function(row, index, array){
						rows.forEach(function(item){
							if(row.itemcode==item.itemcode){
								console.log(item.itemQuantity+", "+row.quantity);
								if(item.itemQuantity > row.quantity){
									//console.log(item.itemcode+", "+row.itemcode);
									reject(true);
								}else{
									var html = "<td>"+row.itemcode2+"</td>";
									html += "<td>"+(row.quantity= parseFloat(row.quantity)-parseFloat(item.itemQuantity))+"</td>";
									html += "<td>"+row.price+"</td>";
									html += "<td><a class='waves-effect waves-light btn additemtrans' id = 'additemtrans-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
									$("#itemtrans-"+row.itemcode).html(html);
								}
							}
						});
						mycounter++;
						if(mycounter==array.length){
							resolve(true);
						}
					});	
				});	
			}).then((res)=>{
				var html = "";
				$("#addItemsHereTrans").html("");
				rows.forEach(function(row){
					getItemPrice(connection, row.itemcode, function(price){
						html = "";
						html += "<tr id = 'addeditemTrans-"+row.itemcode+"'><td>"+row.itemcode2+"</td>"+
						"<td>"+row.itemQuantity+"</td>"+
						"<td>"+(price * row.itemQuantity)+"</td>"+
						"<td><a class='waves-effect waves-light btn removeitemtrans' id = 'removeitemtrans-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
						+"</tr>";
						$("#addItemsHereTrans").prepend(html);
					});
				});
			}).catch((err)=>{
				console.log(err);
				alert("This package have one item that has greater quantity than the actual item on the inventory!");
			});			
		}
	});
}
function getItemsForValidation(connection, callback){
	var sql = "SELECT itemcode, itemcode2, quantity, price, damagedItems FROM tblinventory";
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback([]);
		}else{
			callback(rows);
		}
	});
}
function getReservedItem(connection, itemid, callback){
	var sql = "SELECT SUM(r.qty) AS mysum FROM tbltrans_rent r INNER JOIN tbltrans_location l  ON l.trans_locationid ="+
	" r.locationID WHERE r.itemcode = ? AND l.depositReturned <> 'false'";
	var insert = [itemid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			callback(0);
		}else{
			//console.log(sql);
			if(rows[0].mysum == undefined){
				callback(0);
			}else{
				//console.log(rows.mysum);
				callback(rows[0].mysum);
			}
			
		}
	});
}
function returnAbs(firstvalue, secvalue, thirdvalue){
	var myval = (firstvalue-secvalue)-thirdvalue;
	if(myval<0){
		return 0;
	}else{
		return myval;
	}
}
function getItemPrice(connection, itemcode, callback){
	var sql = "SELECT price FROM tblinventory WHERE itemcode = ?";
	var insert = [itemcode];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			callback(rows[0].price);
		}
	});
}
function getPackagePrice(connection, packageID){
	var sql= "SELECT price FROM package WHERE packageID = ?";
	var insert = [packageID];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, row){
		if(err){
			console.log(err);
		}else{
			$("#packagePrice").val(row[0].price);
			$(".userLabel").addClass('active');
		}
	});
	sql = "SELECT i.itemcode, h.itemQuantity, i.itemcode2 FROM packagehistory "+
	"h INNER JOIN package p ON p.packageID = h.packageID INNER JOIN "+
	"tblinventory i ON i.itemcode = h.inventoryID WHERE p.packageID = ?";
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			new Promise((resolve, reject)=>{
				getItemsForValidation(connection, function(rows){
					//resolve(rows);
					var thiscounter = 0;
					rows.forEach(function(row, index, array){
						getReservedItem(connection, row.itemcode, function(qty){
							rows[index].quantity = returnAbs(rows[index].quantity, qty, rows[index].damagedItems);
							thiscounter++;
							if(thiscounter==array.length){
								resolve(rows);
							}
						});
					});
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					//console.log(rows);
					var mycounter = 0;
					res.forEach(function(row, index, array){
						rows.forEach(function(item){
							if(row.itemcode==item.itemcode){
								console.log(item.itemQuantity+", "+row.quantity);
								if(item.itemQuantity > row.quantity){
									//console.log(item.itemcode+", "+row.itemcode);
									reject(true);
								}else{
									var html = "<td>"+row.itemcode2+"</td>";
									html += "<td>"+(row.quantity= parseFloat(row.quantity)-parseFloat(item.itemQuantity))+"</td>";
									html += "<td>"+row.price+"</td>";
									html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
									$("#item-"+row.itemcode).html(html);
								}
							}
						});
						mycounter++;
						if(mycounter==array.length){
							resolve(true);
						}
					});	
				});	
			}).then((res)=>{
				var html = "";
				$("#addItemsHere").html("");
				rows.forEach(function(row){
					getItemPrice(connection, row.itemcode, function(price){
						html = "";
						html += "<tr id = 'addeditem-"+row.itemcode+"'><td>"+row.itemcode2+"</td>"+
						"<td>"+row.itemQuantity+"</td>"+
						"<td>"+(price * row.itemQuantity)+"</td>"+
						"<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
						+"</tr>";
						$("#addItemsHere").prepend(html);
					});
				});
			}).catch((err)=>{
				console.log(err);
				alert("This package have one item that has greater quantity than the actual item on the inventory!");
			});			
		}
	});
}
function checkifmaxtransactsreached(connection, startdate, enddate, callback){
	var sql = "SELECT * FROM tbltrans_location WHERE (? >= "+
	"event_date_start AND ? <= event_date_end) OR "+
	"(? >= event_date_start AND ? <= event_date_end) "+
	"OR (event_date_start >= ? AND event_date_start <= ?)";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(true);
		}else{
			if(rows.length >= 3){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function checkifmaxtransactsreachedononelocation(connection, startdate, enddate, locationid, callback){
	var sql = "SELECT * FROM tbltrans_location WHERE ((? >= "+
	"event_date_start AND ? <= event_date_end) OR "+
	"(? >= event_date_start AND ? <= event_date_end) "+
	"OR (event_date_start >= ? AND event_date_start <= ?)) AND locationID = ?";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate, locationid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(true);
		}else{
			if(rows.length > 0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}