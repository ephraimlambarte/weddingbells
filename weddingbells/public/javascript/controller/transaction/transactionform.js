var inventoryPage = 0;
var itemsSelected = [{
	quantity:0,
	itemcode:0,
	price:0,
	itemcode2: ""
}];
var visiblepages = 0;
var totalpages = 0;
var item = {
	quantity:0,
	price: 0,
	itemcode2: "",
	itemcode:""
};
var image = 0;
var maleTailor = [];
var femaleTailor = [];
var maletailorid = 0;
var femaletailorid = 0;
var edittailormale = false;
var editmaletailorid = 0;
var edittailorfemale = false;
var editfemaletailorid = 0;
var confirmTransac;
$(document).ready(function(){
	w3.includeHTML(function(){
		$("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
		$('select').material_select();
		$('.modal').modal();
		$(".datepicker1").pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15, // Creates a dropdown of 15 years to control year,
	    today: 'Today',
	    clear: 'Clear',
	    close: 'Ok',
	    closeOnSelect: false // Close upon selecting a date,
	  });
	});
	$("#eventDateStart").on('change', function(){
		var dt = new Date($('#eventDateStart').val());
		var mydate = new Date(dt.setDate(dt.getDate()) - 1);
		var thisdate = mydate.getDate()+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
		$('#rentstartdate').val(thisdate);

		var dateclaim = new Date(formatDate($("#eventDateStart").val()));
		var mydate = new Date(dateclaim.setDate(dateclaim.getDate() + 15));
		var thisdate1 = mydate.getDate()+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
		$("#claimdate").val(thisdate1);
	});
	$('#eventDateEnd').on('change', function(){
		
		var dt = new Date($('#eventDateEnd').val());
		var mydate = new Date(dt.setDate(dt.getDate()) + 1);
		var checkdate = new Date(mydate.getFullYear(), mydate.getMonth()+1, 0);
		if (checkdate.getDate()==dt.getDate()){
			var thisdate = 1+" "+getMonthName(mydate.getMonth()+2)+", "+mydate.getFullYear();
		}
		else{
			var thisdate = (mydate.getDate()+1)+" "+getMonthName(mydate.getMonth()+1)+", "+mydate.getFullYear();
		}
		$('#rentenddate').val(thisdate);
	});
	$("#rentAttireButton").on('click', function(){
		$("#rentattireModal").modal("open");
	});
	$("#tailorAttireButton").on('click', function(){
		$("#tailorattireModal").modal("open");
		$('ul.tabs').tabs();

	});
	$('.mymodal').on('click', '#tailorAttireMale', function(){
		edittailormale = false;
		$("#measurementMaleModal").modal("open");
	});
	$('.mymodal').on('click', '#tailorAttireFemale', function(){
		edittailorfemale = false;
		$("#measurementFemaleModal").modal("open");
	});
	$('#eventTimeTransacted, #eventTime, #photoStartTime,#photoEndTime, #eventTimeEnd, #stylistStartTime, #stylistEndTime').pickatime({
		default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: true, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: 'Clear', // text for clear-button
		canceltext: 'Cancel', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
  	});
	$('.datepicker').pickadate({
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15, // Creates a dropdown of 15 years to control year,
		today: 'Today',
		clear: 'Clear',
		close: 'Ok',
		closeOnSelect: false // Close upon selecting a date,
	});
	//promiseExperiment();
	connectMysql2(function(connection){
		getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			if(usertype != 'Admin'){
				$("#userPage").remove();
			}
		});
		loadCustomers(connection);
		loadTheme(connection);
		loadCake(connection);
		loadPhotographer(connection);
		loadStylist(connection);
		loadLocation(connection);
		loadPackages(connection);
		getMaxRows(function(rows){
			if(rows<=5){
				visiblepages=1;
			}else {
				visiblepages=5;
			}
			if(rows%5!=0){
				totalpages=Math.floor((rows/5))+1;
			}else{
				totalpages=rows/5;
			}
			if(rows==0){
				$("#loadInventoryItemsHere").html("");
				return;
			}
			$('#inventoryPagination').twbsPagination({
							totalPages: totalpages,
							visiblePages: visiblepages,
							onPageClick: function (event, page){
								inventoryPage = (5*(page-1));
									loadInventoryTable(connection, inventoryPage);
							}
			 });
		}, "tblinventory", "");
	});
	$("#cake").on('change', function(){
		var cakeid = $("#cake").val();
		initPayment();
		deleteImages(function(){
			getImageCake(cakeid, function(img){
				readWriteFile(img, image,function(address){
					$('#cakeImage').prop('src', '../public/temp/out'+image+'.jpg');
					image++;
				});
			});
		});
	});
	$("#theme").on('change', function(){
		var themeid = $("#theme").val()[$("#theme").val().length-1];
		deleteImages(function(){
			getImageTheme(themeid, function(img){
				readWriteFile(img, image,function(address){
					$('#themeImage').prop('src', '../public/temp/out'+image+'.jpg');
					image++;
				});
			});
		});
	});
	$(".mymodal").on('click', '#loadInventoryItemsHere tr', function(){
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		item.quantity = product[1];
		item.price = product[2];
		item.itemcode2 = product[0];
		item.itemcode = $(this).prop("id").split("-")[1];
		/*deleteImages(function(){
			getImageInventory(item.itemcode, function(img){
				readWriteFile(img, image,function(address){
					$('#itemImage').prop('src', '../public/temp/out'+image+'.jpg');
					image++;
				});
			});
		});*/
	});
	$(".mymodal").on('click', '#loadInventoryItemsHere .additem', function(){
		var i = 0;
		var product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		item.quantity = product[1];
		item.price = product[2];
		item.itemcode2 = product[0];
		item.itemcode = $(this).prop("id").split("-")[1];
		if(item.quantity == 0){
			alert("This item is already 0 quantity!")
			return;
		}
		var items = [];
		$('#addItemsHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			items.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});
		});
		var html = "";
		if(items.length == 0){
			html += "<tr id = 'addeditem-"+item.itemcode+"'><td>"+item.itemcode2+"</td>"+
			"<td>"+1+"</td>"+
			"<td>"+item.price+"</td>"+
			"<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+item.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
			+"</tr>";
			$("#addItemsHere").prepend(html);
			html = "<td>"+item.itemcode2+"</td>"+
			"<td>"+(item.quantity-1)+"</td>"+
			"<td>"+item.price+"</td>"+
			"<td><a class='waves-effect waves-light btn additem' id = 'additem-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
			$("#item-"+item.itemcode).html(html);
		}else{
			var exist = false;
			items.forEach(function(row){
				if(row.itemcode==item.itemcode){
					exist =  true;
					html = "<td>"+row.itemcode2+"</td>"+
					 "<td>"+(parseFloat(row.quantity)+parseFloat(1))+"</td>"+
					 "<td>"+(item.price*(parseFloat(row.quantity)+parseFloat(1)))+"</td>"+
					 "<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
					 $("#addeditem-"+row.itemcode).html(html);
					 html = "<td>"+item.itemcode2+"</td>"+
		 			"<td>"+(item.quantity-1)+"</td>"+
		 			"<td>"+item.price+"</td>"+
		 			"<td><a class='waves-effect waves-light btn additem' id = 'additem-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
		 			$("#item-"+item.itemcode).html(html);
				}
			});
			if(exist == false){

				html = "<tr id = 'addeditem-"+item.itemcode+"'><td>"+item.itemcode2+"</td>"+
				"<td>"+1+"</td>"+
				"<td>"+item.price+"</td>"+
				"<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+item.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
				+"</tr>";
				$("#addItemsHere").prepend(html);
				html = "<td>"+item.itemcode2+"</td>"+
			 "<td>"+(item.quantity-1)+"</td>"+
			 "<td>"+item.price+"</td>"+
			 "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+item.itemcode+"'><i class='material-icons'>add</i></a></td>";
			 $("#item-"+item.itemcode).html(html);
			}
		}
		initPayment();
	});
	$(".mymodal").on('click', '#loadInventoryItemsHere .viewImage', function(){
		$("#imagemodal").modal("open");
		var rowid = $(this).prop("id").split("-")[1];
		//$('#inventoryModal').modal("open");
		deleteImages(function(){
			getImageInventory(rowid, function(img){
				//console.log(img);
				readWriteFile(img, image,function(address){
					$('#itemImageModal').prop('src', '../public/temp/out'+image+'.jpg');
					image++;
				});
			});
		});
	});
	$(".mymodal").on('click', '#addItemsHere .removeitem',function(){
		//alert("Hello");
		var i = 0;
		var myproduct = [""];
		var product = [""];
		var items = [];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				myproduct[i]=$(this).html();
				i++;
			});
		});
		myid = $(this).prop("id").split("-")[1];
		$('#loadInventoryItemsHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			items.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});
		});
		//console.log(myid);
		if(myproduct[1]==1){
			$("#addeditem-"+myid).remove();
		}
		items.forEach(function(row, index){
			if(row.itemcode == myid){
					var html = "<td>"+row.itemcode2+"</td>";
					html += "<td>"+(row.quantity= parseFloat(row.quantity)+parseFloat(1))+"</td>";
					html += "<td>"+row.price+"</td>";
					html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
				$("#item-"+row.itemcode).html(html);
				html = "<td>"+row.itemcode2+"</td>";
				html += "<td>"+(myproduct[1]= parseFloat(myproduct[1])-parseFloat(1))+"</td>";
				html += "<td>"+row.price+"</td>";
				html += "<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>";
				$("#addeditem-"+row.itemcode).html(html);
			}
		});
	});
	$(".mymodal").on('change', '#imgMale', function(){
		if(this.files[0].size>5000000){
			alert("Image size must not be greater than 5mb.");
			$("#imgMale").val("");
			return;
		}
		readURLTailorMale(this);
	});
	$(".mymodal").on('click', '#attireMaleModalButton', function(){
		if($("#typeMale").val()=="coat"){
			if($("#neckMale").val()== "" ||$("#waistMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
					$("#backlengthMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
				$("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
				|| $("#wristMale").val() == ""){
				alert("Please provide all the necessary details!");
				return;
			}
		}else if ($("#typeMale").val()=="slacks") {
			if($("#waistMale").val()== "" ||
					$("#seatMale").val()== "" ||$("#hipMale").val()== "" ||$("#inseamMale").val()== "" ||$("#outseamMale").val()== "" ||
				$("#hemMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
				$("#typeMale").val()== "" ||$("#fabricMale").val()== ""){
				alert("Please provide all the necessary details!");
				return;
			}
		}else if ($("#typeMale").val()=="polo") {
			if($("#neckMale").val()== "" ||$("#waistMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
					$("#backlengthMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
				$("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
				|| $("#wristMale").val() == ""){
				alert("Please provide all the necessary details!");
				return;
			}
		}else if ($("#typeMale").val()=="vest") {
			if($("#neckMale").val()== "" ||$("#waistMale").val()== ""  ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== ""
			   ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
				$("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""){
				alert("Please provide all the necessary details!");
				return;
			}
		}else{
			if($("#neckMale").val()== "" ||$("#chest").val()== "" ||$("#waistMale").val()== "" ||
					$("#seatMale").val()== "" ||$("#hipMale").val()== "" ||$("#sleevesMale").val()== "" ||
					$("#backlengthMale").val()== "" ||$("#inseamMale").val()== "" ||$("#outseamMale").val()== "" ||
				$("#hemMale").val()== "" ||$("#priceMale").val()== "" ||$("#nameMale").val()== "" ||
				$("#typeMale").val()== "" ||$("#fabricMale").val()== "" || $("#shoulderMale").val()==""
				|| $("#wristMale").val() == ""){
				alert("Please provide all the necessary details!");
				return;
			}
		}
		maleTailor.push({
			neckMale:$("#neckMale").val(),
			chest:$("#chest").val(),
			waistMale:$("#waistMale").val(),
			seatMale:$("#seatMale").val(),
			hipMale:$("#hipMale").val(),
			sleevesMale:$("#sleevesMale").val(),
			backlengthMale:$("#backlengthMale").val(),
			inseamMale:$("#inseamMale").val(),
			outseamMale:$("#outseamMale").val(),
			hemMale:$("#hemMale").val(),
			priceMale:$("#priceMale").val(),
			nameMale:$("#nameMale").val(),
			typeMale:$("#typeMale").val(),
			fabricMale:$("#fabricMale").val(),
			shoulderMale:$("#shoulderMale").val(),
			wristMale:$("#wristMale").val(),
			maleTailorImage:$("#maleTailorImage").prop("src")
		});
		if(edittailormale == false){
			if($("#imgMale").val()== ""){
				alert("Please provide all the necessary details!");
				return;
			}
			var html = "<tr class='rowTailorMale' data-value = '"+$("#maleTailorImage").prop("src")+"' id = 'tailorMale-"+maletailorid+"'>";
				html += "<td>"+$("#nameMale").val()+"</td>";
				html += "<td>Male</td>";
				html += "<td>"+$("#neckMale").val()+"</td>";
				html += "<td>"+$("#chest").val()+"</td>";
				html += "<td>"+$("#waistMale").val()+"</td>";
				html += "<td>"+$("#seatMale").val()+"</td>";
				html += "<td>"+$("#hipMale").val()+"</td>";
				html += "<td>"+$("#shoulderMale").val()+"</td>";
				html += "<td>"+$("#wristMale").val()+"</td>";
				html += "<td>"+$("#sleevesMale").val()+"</td>";
				html += "<td>"+$("#backlengthMale").val()+"</td>";
				html += "<td>"+$("#inseamMale").val()+"</td>";
				html += "<td>"+$("#outseamMale").val()+"</td>";
				html += "<td>"+$("#hemMale").val()+"</td>";
				html += "<td>"+$("#fabricMale").val()+"</td>";
				html += "<td>"+$("#priceMale").val()+"</td>";
				html += "<td>"+$("#typeMale").val()+"</td>";
				html += "<td><a class='waves-effect waves-light btn editMale' id = 'editMale-"+maletailorid+"'><i class='material-icons'>edit</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn removeMale' id = 'removeMale-"+maletailorid+"'><i class='material-icons'>clear</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn addToInventoryMale' id = 'addToInventoryMale-"+maletailorid+"'><i class='material-icons'>add</i></a></td>";
				html += "</tr>";
				maletailorid++;
				$("#putTailorMaleHere").append(html);
				initPayment();
		}else{
			edittailormale = false;
			var html = "<td>"+$("#nameMale").val()+"</td>";
			html += "<td>Male</td>";
			html += "<td>"+$("#neckMale").val()+"</td>";
			html += "<td>"+$("#chest").val()+"</td>";
			html += "<td>"+$("#waistMale").val()+"</td>";
			html += "<td>"+$("#seatMale").val()+"</td>";
			html += "<td>"+$("#hipMale").val()+"</td>";
			html += "<td>"+$("#shoulderMale").val()+"</td>";
			html += "<td>"+$("#wristMale").val()+"</td>";
			html += "<td>"+$("#sleevesMale").val()+"</td>";
			html += "<td>"+$("#backlengthMale").val()+"</td>";
			html += "<td>"+$("#inseamMale").val()+"</td>";
			html += "<td>"+$("#outseamMale").val()+"</td>";
			html += "<td>"+$("#hemMale").val()+"</td>";
			html += "<td>"+$("#fabricMale").val()+"</td>";
			html += "<td>"+$("#priceMale").val()+"</td>";
			html += "<td>"+$("#typeMale").val()+"</td>";
			html += "<td><a class='waves-effect waves-light btn editMale' id = 'editMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>edit</i></a></td>";
			html += "<td><a class='waves-effect waves-light btn removeMale' id = 'removeMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
			html += "<td><a class='waves-effect waves-light btn addToInventoryMale' id = 'addToInventoryMale-"+editmaletailorid.split("-")[1]+"'><i class='material-icons'>add</i></a></td>";
			$("#tailorMale-"+editmaletailorid.split("-")[1]).html(html);
			if( document.getElementById("imgMale").files.length > 0 ){
				$("#tailorMale-"+editmaletailorid.split("-")[1]).data("value", $("#maleTailorImage").prop("src"));
			}
			//initPayment();
		}
		getTailorPrice();
		initPayment();
	});
	$(".mymodal").on('click', '#putTailorMaleHere .rowTailorMale', function(){
		editmaletailorid = $(this).prop("id");
		var rowmaleid = $(this).prop("id");
		$("#ImageViewTailor").prop("src", $("#"+rowmaleid).data("value"));
	});
	$(".mymodal").on('change', '#imgFemale', function(){
		if(this.files[0].size>5000000){
			alert("Image size must not be greater than 5mb.");
			$("#imgFemale").val("");
			return;
		}
		readURLTailorFemale(this);
	});
	$(".mymodal").on('click', '#attireFemaleModalButton', function(){
		if($("#nameFemale").val()== "" ||$("#typeFemale").val()== "" ||$("#fabricFemale").val()== "" ||
			$("#chestFemale").val()== "" ||$("#bustFemale").val()== "" ||$("#hipFemale").val()== "" ||
			$("#waistFemale").val()== "" ||$("#shoulderFemale").val()== "" ||
			$("#frontskirtFemale").val()== "" ||$("#backskirtFemale").val()== "" ||$("#priceFemale").val()== ""){
				alert("Please provide all the necessary details!");
				return;
			}
		femaleTailor.push({
				nameFemale: $("#nameFemale").val(),
				typeFemale: $("#typeFemale").val(),
				fabricFemale: $("#fabricFemale").val(),
				chestFemale: $("#chestFemale").val(),
				bustFemale: $("#bustFemale").val(),
				hipFemale: $("#hipFemale").val(),
				waistFemale: $("#waistFemale").val(),
				wristFemale: $("#wristFemale").val(),
				shoulderFemale: $("#shoulderFemale").val(),
				frontskirtFemale: $("#frontskirtFemale").val(),
				backskirtFemale: $("#backskirtFemale").val(),
				priceFemale: $("#priceFemale").val(),
				imgFemale:$("#femaleTailorImage").prop("src")
		});
		if(edittailorfemale == false){
			if($("imgFemale").val()== ""){
				alert("Please provide all the necessary details!");
				return;
			}
			var html = "<tr id = 'tailorFemale-"+femaletailorid+"' class='rowTailorFemale' data-value = '"+$("#femaleTailorImage").prop("src")+"'>";
				html += "<td>"+$("#nameFemale").val()+"</td>";
				html += "<td>Female</td>";
				html += "<td>"+$("#chestFemale").val()+"</td>";
				html += "<td>"+$("#bustFemale").val()+"</td>";
				html += "<td>"+$("#hipFemale").val()+"</td>";
				html += "<td>"+$("#waistFemale").val()+"</td>";
				html += "<td>"+$("#wristFemale").val()+"</td>";
				html += "<td>"+$("#shoulderFemale").val()+"</td>";
				html += "<td>"+$("#frontskirtFemale").val()+"</td>";
				html += "<td>"+$("#backskirtFemale").val()+"</td>";
				html += "<td>"+$("#fabricFemale").val()+"</td>";
				html += "<td>"+$("#typeFemale").val()+"</td>";
				html += "<td>"+$("#priceFemale").val()+"</td>";
				html += "<td><a class='waves-effect waves-light btn editFemale' id = 'editFemale-"+femaletailorid+"'><i class='material-icons'>edit</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn removeFemale' id = 'removeFemale-"+femaletailorid+"'><i class='material-icons'>clear</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn addToInventoryFemale' id = 'addToInventoryFemale-"+femaletailorid+"'><i class='material-icons'>add</i></a></td>";
				femaletailorid++;
				html += "</tr>";
				$("#putTailorFemaleHere").append(html);
				initPayment();
		}else{
				var html = "<td>"+$("#nameFemale").val()+"</td>";
				html += "<td>Female</td>";
				html += "<td>"+$("#chestFemale").val()+"</td>";
				html += "<td>"+$("#bustFemale").val()+"</td>";
				html += "<td>"+$("#hipFemale").val()+"</td>";
				html += "<td>"+$("#waistFemale").val()+"</td>";
				html += "<td>"+$("#wristFemale").val()+"</td>";
				html += "<td>"+$("#shoulderFemale").val()+"</td>";
				html += "<td>"+$("#frontskirtFemale").val()+"</td>";
				html += "<td>"+$("#backskirtFemale").val()+"</td>";
				html += "<td>"+$("#fabricFemale").val()+"</td>";
				html += "<td>"+$("#typeFemale").val()+"</td>";
				html += "<td>"+$("#priceFemale").val()+"</td>";
				html += "<td><a class='waves-effect waves-light btn editFemale' id = 'editFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>edit</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn removeFemale' id = 'removeFemale-"+editfemaletailorid.split("-")[1]+"'><i class='material-icons'>clear</i></a></td>";
				html += "<td><a class='waves-effect waves-light btn addToInventoryFemale' id = 'addToInventoryFemale-"+femaletailorid+"'><i class='material-icons'>add</i></a></td>";
				$("#tailorFemale-"+editmaletailorid.split("-")[1]).html(html);
				if( document.getElementById("imgFemale").files.length > 0 ){
					$("#tailorFemale-"+editmaletailorid.split("-")[1]).data("value", $("#femaleTailorImage").prop("src"));
				}
				//
		}
		getTailorPrice();
		initPayment();
	});
	$(".mymodal").on('click', '.addToInventoryFemale', function(){
        $("#addItemToInventoryModal").modal("open");
    });
    $(".mymodal").on('click', '.addToInventoryMale', function(){
        $("#addItemToInventoryModal").modal("open");
	});
	$(".mymodal").on('click', '#maleRadioButton', function(){
		
		var html = "<option value='coat'>Coat</option>"+
			"<option value='slacks'>Slacks</option>"+
			"<option value='polo'>Polo</option>"+
			"<option value='vest'>Vest</option>"+
			"<option value='others'>Others</option>";
		$("#categoryTailorItem").html(html);
		$("#categoryTailorItem").material_select();
	});
	$(".mymodal").on('click', '#femaleRadioButton', function(){
		var html = "<option value='cocktail'>Cocktail</option>"+
			"<option value='ballgown'>Ballgown</option>"+
			"<option value='longdress'>Long Dress</option>"+
			"<option value='others'>Others</option>";
		$("#categoryTailorItem").html(html);
		$("#categoryTailorItem").material_select();
	});
	$(".mymodal").on('click', '#addItemToInventoryButton', function(){
		var gender = getGender();
		var category = $("#categoryTailorItem").val();
		var type = $("#typeTailorItem").val();
		var color = $("#colorTailorItem").val();
		var quantity = $("#quantityTailor").val();
		var price = $("#priceTailor").val();
		var dateToBeReturned = $("#dateToBeReturnTailor").val();
		var id = $(this).prop("id");
		if(gender == "" || category == "" ||type == "" ||
		color == "" ||quantity == "" ||price == "" ||dateToBeReturned == ""){
			alert("Please fill up all the necessary fields!");
			return;
		}
		var itemreturn = {
			gender:gender,
			category:category,
			type:type,
			color:color,
			quantity:quantity,
			price:price,
			dateToBeReturned:formatDate(dateToBeReturned),
			customerid:  $("#customerName").val(),
			id:id
		};
		connectMysql2(function(connection){
			insertItemToBeReturned(connection, itemreturn);
		});		
	});
	$(".mymodal").on('click', '#putTailorFemaleHere .rowTailorFemale', function(){
		editfemaletailorid= $(this).prop("id");
		var rowfemaleid = $(this).prop("id");
		$("#ImageViewTailor").prop("src", $("#"+rowfemaleid).data("value"));
	});
	$(".mymodal").on('click', '#putTailorFemaleHere .removeFemale', function(){
		var rowfemaleid = $(this).prop("id").split("-")[1];
		$("#tailorFemale-"+rowfemaleid).remove();
		initPayment();
	});
	$(".mymodal").on('click', '#putTailorMaleHere .removeMale', function(){
		var rowmaleid = $(this).prop("id").split("-")[1];
		$("#tailorMale-"+rowmaleid).remove();
		initPayment();
	});
	$(".mymodal").on('click', '#putTailorMaleHere .editMale', function(){
		edittailormale = true;
		i = 0;
		product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		editmaletailorid = $(this).prop("id");
		var maleimage = $("#addedMale-"+$(this).prop("id").split("-")[1]).data("value");
		$("#neckMale").val(product[2]);
		$("#chest").val(product[3]);
		$("#waistMale").val(product[4]);
		$("#seatMale").val(product[5]);
		$("#hipMale").val(product[6]);
		$("#sleevesMale").val(product[9]);
		$("#backlengthMale").val(product[10]);
		$("#inseamMale").val(product[11]);
		$("#outseamMale").val(product[12]);
		$("#hemMale").val(product[13]);
		$("#priceMale").val(product[15]);
		$("#nameMale").val(product[0]);
		$("#typeMale").val(product[16]);
		$("#typeMale").material_select();
		$("#fabricMale").val(product[14]);
		$("#fabricMale").material_select();
		$("#shoulderMale").val(product[7]);
		$("#wristMale").val(product[8]);
		$("#maleTailorImage").prop("src", maleimage);
		$("#measurementMaleModal").modal("open");
	});
	$(".mymodal").on('click', '#putTailorFemaleHere .editFemale', function(){
		edittailorfemale=true;
		i = 0;
		product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		//var femaleimage = $("#tailorFemale-"+$(this).prop("id").split("-")[1]).data("value");
		editfemaletailorid = $(this).prop("id");
		$("#nameFemale").val(product[0]);
		$("#typeFemale").val(product[11]);
		$("#typeFemale").material_select();
		$("#fabricFemale").val(product[10]);
		$("#fabricFemale").material_select();
		$("#chestFemale").val(product[2]);
		$("#bustFemale").val(product[3]);
		$("#hipFemale").val(product[4]);
		$("#waistFemale").val(product[5]);
		$("#wristFemale").val(product[6]);
		$("#shoulderFemale").val(product[7]);
		$("#frontskirtFemale").val(product[8]);
		$("#backskirtFemale").val(product[9]);
		$("#priceFemale").val(product[12]);
		$("#measurementFemaleModal").modal("open");
	});
	$("#saveTransacButton").on('click', function(){

		var customerName = $("#customerName").val();
		var location = $("#location").val();
		var totalPayment = $("#totalPayment").val();
		var downpayment = $("#downpayment").val();
		if(parseFloat(downpayment) > parseFloat(totalPayment)){

			alert("Downpayment cannot be greater than total payment!");
			return;
		}

		var eventDateStart = formatDate($("#eventDateStart").val());
		var eventDateEnd = formatDate($("#eventDateEnd").val());
		var eventTimeTransacted = $("#eventTimeTransacted").val();

		var rentDateStart = formatDate($('#rentstartdate').val());
		var rentDateEnd = formatDate($('#rentenddate').val());

		var eventDateTransacted = formatDate($("#eventDateTransacted").val());
		var depositTransact = $("#depositTransact").val();
		var theme = $("#theme").val();
		
		var cake = $("#cake").val();
		var photographer = $("#photographer").val();
		var photographerStartDate = formatDate($("#photoStartDate").val());
		var photographerEndDate = formatDate($("#photoEndDate").val());
		var photographerStartTime = $("#photoStartTime").val();
		var photographerEndTime = $("#photoEndTime").val();
		var photochecker = false;
		if(photographer != ""){
			if(photographerStartDate=="" ||photographerEndDate == ""
				|| photographerStartTime == "" || photographerEndTime == ""){
					alert("Please fill up all the necessary fields for photographer!");
					return;
				}else{
					var d1 = Date.parse(photographerStartDate);
					var d2 = Date.parse(photographerEndDate);
					if (d1 > d2) {
						alert ("Invalid dates for photographer!");
						return;
					}
				}
		}
		var stylist = $("#stylist").val();
		var stylistStartDate = formatDate($("#stylistStartDate").val());
		var stylistEndDate = formatDate($("#stylistEndDate").val());
		var stylistStartTime= $("#stylistStartTime").val();
		var stylistEndTime= $("#stylistEndTime").val();
		if(stylist != ""){
			if(stylistStartDate == "" || stylistEndDate == "" 
				|| stylistStartTime == "" || stylistEndTime == ""){
					alert("Please fill up all the necessary fields for stylist!");
					return;
				}else{
					var d1 = Date.parse(stylistStartDate);
					var d2 = Date.parse(stylistEndDate);
					if (d1 > d2) {
						alert ("Invalid dates for stylist!");
						return;
					}
				}
		}
		
		var itemsrented = [];
		var eventTime = $("#eventTime").val();
		var eventTimeEnd = $("#eventTimeEnd").val();
		$('#addItemsHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			itemsrented.push({
				itemcode:$(this).prop("id").split("-")[1],
				itemcode2:product[0],
				quantity:product[1],
				price:product[2]
			});
		});
		var measurementMale = [];
		$('#putTailorMaleHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			measurementMale.push({
				name:product[0],
				category:product[1],
				neck:product[2],
				chest:product[3],
				waist:product[4],
				seat:product[5],
				hip:product[6],
				shoulder:product[7],
				wrist:product[8],
				sleeves:product[9],
				backlength:product[10],
				inseam:product[11],
				outseam:product[12],
				hem:product[13],
				fabric:product[14],
				price:product[15],
				type:product[16],
				img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
			});
		});
		var measurementFemale = [];
		$('#putTailorFemaleHere > tr').each(function(){
			i = 0;
			product = [""];
			$(this).closest('tr').each(function(){
				$(this).find('td').each(function(){
					product[i]=$(this).html();
					i++;
				});
			});
			measurementFemale.push({
				name:product[0],
				category:product[1],
				chest:product[2],
				bust:product[3],
				hip:product[4],
				waist:product[5],
				wrist:product[6],
				shoulder:product[7],
				frontskirt:product[8],
				backskirt:product[9],
				fabric:product[10],
				type:product[11],
				price:product[12],
				img:base64ToBlob($(this).data("value").replace(/^data:image\/(png|jpeg);base64,/,''))
			});
		});
		//console.log(measurementFemale);
		if(itemsrented.length>0){
			if(depositTransact==""){
				alert("Deposit is required when renting an item!");
				return;
			}
		}else{
			if(depositTransact!= ""){
				alert("Deposit is only for renting items!");
				return;
			}
		}
		if(customerName == "" || location == "" || totalPayment == "" ||
			eventDateStart == "" || eventDateEnd == "" || eventTimeTransacted == ""
		|| eventDateTransacted == "" || eventTime == "" || eventTimeEnd == ""){
			alert("Please fill up all the necessary fields!");
			return;
		}

		var customerTransac = {
			customerName:customerName,
			location:location,
			totalPayment:totalPayment,
			downpayment:downpayment,
			eventDateStart:eventDateStart,
			eventDateEnd:eventDateEnd,
			eventTimeTransacted:getTime(eventTimeTransacted.slice(0,5), eventTimeTransacted.slice(-2)),
			eventDateTransacted:eventDateTransacted,
			depositTransact:depositTransact,
			rentDateStart:rentDateStart,
			rentDateEnd:rentDateEnd,
			theme:theme,
			cake:cake,
			photographer:{
				photographer:photographer,
				photoStartDate:photographerStartDate,
				photoEndDate: photographerEndDate,
				photoStartTime: getTime(photographerStartTime.slice(0,5), photographerStartTime.slice(-2)),
				photoEndTime:getTime(photographerEndTime.slice(0,5), photographerEndTime.slice(-2))
			},
			stylist:{
			stylist:stylist,
				stylistStartDate:stylistStartDate,
				stylistEndDate:stylistEndDate,
				stylistStartTime:getTime(stylistStartTime.slice(0,5), stylistStartTime.slice(-2)),
				stylistEndTime:getTime(stylistEndTime.slice(0,5), stylistEndTime.slice(-2))
				},
			itemsrented:itemsrented,
			measurementMale:measurementMale,
			measurementFemale:measurementFemale,
			eventTime:getTime(eventTime.slice(0,5), eventTime.slice(-2)),
			eventTimeEnd:getTime(eventTimeEnd.slice(0,5), eventTimeEnd.slice(-2))
		};
		connectMysql2(function(connection){
			new Promise((resolve, reject)=>{
				checkifmaxtransactsreached(connection, customerTransac.eventDateStart, customerTransac.eventDateEnd, function(check){
					if(check){
						reject("max3days");
					}else{
						resolve(true);
					}
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					checkifmaxtransactsreachedononelocation(connection,  customerTransac.eventDateStart, customerTransac.eventDateEnd, customerTransac.location, function(check){
						if(check){
							reject("conflictlocation");
						}else{
							resolve(res);
						}
					});
				});
			}).then((res)=>{
				insertTransaction(connection, customerTransac);	
			}).catch((err)=>{
				if(err== "conflictlocation"){
					confirmTransac = customerTransac;
					$("#confirmConflictModal").modal("open");
					$("#confirmConflictTitle").text("Conflict of event on same date and same place!");
				}else if(err =="max3days"){
					confirmTransac = customerTransac;
					$("#confirmConflictModal").modal("open");
					$("#confirmConflictTitle").text("There should only be max of 3 events on a day!");
				}else{
					console.log(err);
				}
			});
		});
	});
	$("#customerName").on('change', function(){
		connectMysql2(function(connection){
			changeLocationCustomer(connection, $("#customerName").val());
		});
	});
	$(".mymodal").on('change', '#typeMale', function(){
		$("#chest").prop("disabled", false);
		$("#waistMale").prop("disabled", false);
		$("#hipMale").prop("disabled", false);
		$("#sleevesMale").prop("disabled", false);
		$("#backlengthMale").prop("disabled", false);
		$("#inseamMale").prop("disabled", false);
		$("#outseamMale").prop("disabled", false);
		$("#seatMale").prop("disabled", false);
		$("#hemMale").prop("disabled", false);
		$("#shoulderMale").prop("disabled", false);
		$("#wristMale").prop("disabled", false);
		$("#neckMale").prop("disabled", false);
		if($("#typeMale").val()=="coat"){
			$("#chest").val("").prop("disabled", true);
			$("#seatMale").val("").prop("disabled", true);
			$("#inseamMale").val("").prop("disabled", true);
			$("#outseamMale").val("").prop("disabled", true);
			$("#hemMale").val("").prop("disabled", true);
		}else if ($("#typeMale").val()=="slacks") {
			$("#neckMale").val("").prop("disabled", true);
			$("#chest").val("").prop("disabled", true);
			$("#sleevesMale").val("").prop("disabled", true);
			$("#backlengthMale").val("").prop("disabled", true);
			$("#shoulderMale").val("").prop("disabled", true);
			$("#wristMale").val("").prop("disabled", true);
		}else if ($("#typeMale").val()=="polo") {
			$("#chest").val("").prop("disabled", true);
			$("#seatMale").val("").prop("disabled", true);
			$("#inseamMale").val("").prop("disabled", true);
			$("#outseamMale").val("").prop("disabled", true);
			$("#hemMale").val("").prop("disabled", true);
		}else if ($("#typeMale").val()=="vest") {
			$("#chest").val("").prop("disabled", true);
			$("#seatMale").val("").prop("disabled", true);
			$("#backlengthMale").val("").prop("disabled", true);
			$("#inseamMale").val("").prop("disabled", true);
			$("#outseamMale").val("").prop("disabled", true);
			$("#hemMale").val("").prop("disabled", true);
			$("#wristMale").val("").prop("disabled", true);
		}else{
			$("#chest").prop("disabled", false);
			$("#waistMale").prop("disabled", false);
			$("#hipMale").prop("disabled", false);
			$("#sleevesMale").prop("disabled", false);
			$("#backlengthMale").prop("disabled", false);
			$("#inseamMale").prop("disabled", false);
			$("#hemMale").prop("disabled", false);
			$("#shoulderMale").prop("disabled", false);
			$("#wristMale").prop("disabled", false);
			$("#neckMale").prop("disabled", false);
			$("#outseamMale").prop("disabled", false);
			$("#seatMale").prop("disabled", false);
		}
	});
	$("#photographer").on('change', function(){
		initPayment();
	});
	$("#stylist").on('change', function(){
		initPayment();
	});
	$(".mymodal").on('change', '#rentpackage', function(){
		var packageid = $("#rentpackage").val();
		
		getPackagePrice(connection, packageid);
	});
	$(".mymodal").on('click', '#confirmConflictSave', function(){
		connectMysql2(function(connection){
			insertTransaction(connection, confirmTransac);
		});
	});
	$(".mymodal").on('change', '#valuePriceTailor', function(){
		initPayment();
	});
});

function loadCustomers(connection){
	var sql = "SELECT custID, firstname, lastname FROM tblcustomer";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading the customers. Please try again later!");
		}else{
			var html = "";
			rows.forEach(function(row){
				html += "<option value = '"+row.custID+"'>"+row.firstname+" "+row.lastname+"</option>";
			});
			$("#customerName").html(html);
			$("#customerName").material_select();
		}
		//connection.end();
	});
}
function loadTheme(connection){
	var sql = "SELECT themeID, themename, description FROM tbltheme";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong loading the themes, please try again later!");
		}else{
			var html = "<option selected value = ''>--Theme--</option>";
			rows.forEach(function(row){
				html += "<option value = '"+row.themeID+"'>"+row.themename+" ("+row.description+")</option>";
			});
			$("#theme").html(html);
			$("#theme").material_select();
		}
	});
}
function loadCake(connection){
	var sql = "SELECT cakeID, cakename, price FROM tblcake";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading cakes, please try again later!");
		}else{
			var html = "<option disabled selected>--Cake--</option>";
			rows.forEach(function(row){
				html += "<option value = '"+row.cakeID+"'>"+row.cakename+" ("+row.price+")</option>";
			});
			$("#cake").html(html);
			$("#cake").material_select();
		}
	});
}
function loadLocation(connection){
	var sql = "SELECT locationID, locationname FROM tbllocation";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading the locations. Please try again later!");
		}else{
			var html = "";
			rows.forEach(function(row){
				html += "<option value = '"+row.locationID+"'>"+row.locationname+"</option>";
			});
			$("#location").html(html);
			$("#location").material_select();
		}
		//connection.end();
	});
}
function loadPhotographer(connection){
	var sql = "SELECT photoID, firstname, lastname, fee FROM tblphotographer";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading the photographers. Please try again later!");
		}else{
			var html = "<option selected value = ''>--Photographer--</option>";
			rows.forEach(function(row){
				html += "<option value = '"+row.photoID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
			});
			$("#photographer").html(html);
			$("#photographer").material_select();
		}
	});
}
function loadStylist(connection){
	var sql = "SELECT stylistID, firstname, lastname, fee FROM tblstylist";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading the stylists. Please try again later!");
		}else{
			var html = "<option selected value = ''>--Stylist--</option>";
			rows.forEach(function(row){
				html += "<option value = '"+row.stylistID+"'>"+row.firstname+" "+row.lastname+" ("+row.fee+")</option>";
			});
			$("#stylist").html(html);
			$("#stylist").material_select();
		}
	});
}
function loadInventoryTable(connection, page){
 var sql = "SELECT itemcode, itemcode2, availableItems, price FROM tblinventory ORDER BY itemcode DESC LIMIT "+page+", 5";
	connection.query(sql, function(err, rows, fields){
		if(err){
			console.log(err);
			alert("Something went wrong while loading the items. Please try again later!");
		}else{
			var html = "";
			var myitem =[];
			var i = 0;
			var product=[""];
			var exist = false;
			var myindex = 0;
			$('#addItemsHere > tr').each(function(){
				i = 0;
				product = [""];
				$(this).closest('tr').each(function(){
					$(this).find('td').each(function(){
						product[i]=$(this).html();
						i++;
					});
				});
				myitem.push({
					itemcode:$(this).prop("id").split("-")[1],
					itemcode2:product[0],
					quantity:product[1],
					price:product[2]
				});
			});
			rows.forEach(function(row){
				html += "<tr id = 'item-"+row.itemcode+"'>";

					html += "<td>"+row.itemcode2+"</td>";
					if(myitem.length>0){
						myitem.forEach(function(myrow, index){
							if(myrow.itemcode == row.itemcode){
								//html += "<td>"+(row.quantity-myrow.quantity)+"</td>";
								//return;
								exist = true;
								myindex = index;
							}
						});
						if(exist){
							if(row.itemcode == myitem[myindex].itemcode){
								html += "<td>"+(row.availableItems-myitem[myindex].quantity)+"</td>";
							}else{
								html += "<td>"+row.availableItems+"</td>";
							}
						}else{
							html += "<td>"+row.availableItems+"</td>";
						}
						console.log(exist);
					}else{
						html += "<td>"+row.availableItems+"</td>";
					}
					html += "<td>"+row.price+"</td>";
					html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
					html+="<td><a class='waves-effect waves-light btn viewImage' id = 'viewImage-"+row.itemcode+"'><i class='material-icons'>pageview</i></a></td></tr>";
			});
			$(".mymodal #loadInventoryItemsHere").html(html);
		}
	});
}
function checkIfArrExist(arr, val, callback){
	arr.some(function(currentvalue, index){
		var myval = val == arr.itemcode;
		callback(index, myval);
	});
}
function readURLTailorMale(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#maleTailorImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function readURLTailorFemale(input){
		if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
						$('#femaleTailorImage').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
		}
	}
function insertTransaction(connection, cust){
	var sql = "INSERT INTO tbltransaction (custID)"+
							" VALUES(?)";
	var insert = [cust.customerName];
	sql = mysql.format(sql, insert);
	connection.beginTransaction(function(err){
		if(err){
			console.log(err);
			throw err;
		}else{
			
			/*new Promise((resolve, reject)=>{
				checkifmaxtransactsreached(connection, cust.eventDateStart, cust.eventDateEnd, function(check){
					if(check){
						reject("max3days");
					}else{
						resolve(true);
					}
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					checkifmaxtransactsreachedononelocation(connection,  cust.eventDateStart, cust.eventDateEnd, cust.location, function(check){
						if(check){
							reject("conflictlocation");
						}else{
							resolve(true);
						}
					});
				});
			}).then((res)=>{
				return*/
				 new Promise((resolve, reject) => {
					connection.query(sql, function(err, result){
						if(err){
							reject(err);
						}else{
							resolve(result.insertId);
						}
					});
				
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					sql = "INSERT INTO tbltrans_location(locationID, transID, payment, balance, "+
					"date, time_transacted, event_date_start, event_date_end, deposit, downpayment, event_time, eventTimeEnd) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
					var insert2 = [cust.location, res, cust.totalPayment, (cust.totalPayment-cust.downpayment), cust.eventDateTransacted,
						cust.eventTimeTransacted, cust.eventDateStart, cust.eventDateEnd, cust.depositTransact, cust.downpayment, cust.eventTime, cust.eventTimeEnd];
					sql = mysql.format(sql, insert2);
					connection.query(sql, function(err, result){
						if(err){
							reject(err);
						}else{
							resolve(result.insertId);
						}
					});
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(cust.itemsrented.length>0){
						sql = "";
						cust.itemsrented.forEach(function(row){
							sql += "UPDATE tblinventory SET availableItems = availableItems - "+row.quantity+" WHERE itemcode = '"+row.itemcode+"'; ";
						});
						cust.itemsrented.forEach(function(row){
							sql += "INSERT INTO tbltrans_rent(locationID, itemcode, qty, rentdate, returndate) "+
										"VALUES('"+res+"', '"+row.itemcode+"', '"+row.quantity+"', '"+cust.rentDateStart+"', '"+cust.rentDateEnd+"'); ";
						});
	
						connection.query(sql, function(err, result2){
							if(err){
								reject(err)
							}else{
								resolve(res)
							}
						});
					}else{
						resolve(res);
					}
				});
			}).then((res)=>{
				return new Promise((resolve,reject)=>{
					if(cust.measurementMale.length>0 || cust.measurementFemale.length>0){
						sql = "INSERT INTO tbltailor(claimdate) VALUES(?)";
						insert3=[formatDate($("#claimdate").val())];
						sql = mysql.format(sql, insert3);
						connection.query(sql, function(err, result){
							if(err){
								reject(err);
							}else{
								resolve(['hastailor', res, result.insertId]);
							}
						});
					}else{
						resolve(['notailor', res]);
					}
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(res[0]=='hastailor'){
						sql = "";
						if(cust.measurementMale.length>0){
							cust.measurementMale.forEach(function(row){

								sql += "INSERT INTO tbltrans_tailor_boy(tailorID, name, neck, chest, waist,seat, "+
								"hip, shoulder, wrist, sleeves, backlength, inseam, outseam, hem, "+
								"type, fabric, img, price, locationID) "+
								"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
								var myinsert = [res[2], row.name, row.neck,
										row.chest, row.waist, row.seat, row.hip, row.shoulder, row.wrist, row.sleeves,
								row.backlength, row.inseam, row.outseam, row.hem,
								row.type, row.fabric, row.img, row.price, res[1]];
								sql = mysql.format(sql, myinsert);
							});
						}
						if(cust.measurementFemale.length>0){
							cust.measurementFemale.forEach(function(row){

								sql += "INSERT INTO tbltrans_tailor_girl(tailorID, name, chest, bust, hip, waist,"+
								" wrist, shoulder, frontskirt, backskirt, "+
								"type, fabric, img, price, locationID) "+
								"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
								var myinsert = [res[2], row.name, row.chest, row.bust,
								row.hip, row.waist, row.wrist, row.shoulder,
								row.frontskirt, row.backskirt,
								row.type, row.fabric, row.img, row.price,res[1]];
								sql = mysql.format(sql, myinsert);
							});
						}
						if(sql != ""){
							connection.query(sql, function(err){
								if(err){
									reject(err);
								}else{
									resolve(res[1]);
								}
							});
						}
					}else{
						resolve(res[1]);
					}
				});
			}).then((res) =>{
				return new Promise((resolve, reject)=>{
					sql = "";
					if(cust.cake.length>0){
						cust.cake.forEach(function(row){
							sql += "INSERT INTO tbltrans_cake(locationID, cakeID) VALUES(?,?); ";
							var myinsert = [res, row];
							sql = mysql.format(sql, myinsert);
						});
						connection.query(sql, function(err, result4){
							if(err){
								reject(err);
							}else{
								resolve(res);
							}
						});
					}else{
						resolve(res)
					}
				});	
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(!(cust.photographer.photographer=="")){
						checkForConflictsPhotographer(connection, cust.photographer.photoStartDate, cust.photographer.photoEndDate, 
							cust.photographer.photographer, function(check){
								if(check){
									reject("photographer");
								}else{
									resolve(["has photographer", res])
								}
						});
					}else{
						resolve(["no photohrapher", res]);
					}
					
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(res[0] == "has photographer"){
						sql = "INSERT INTO tbltrans_photography(locationID, photoID, date_start, date_end, "+
						"startTime, endTime) VALUES(?,?,?,?,?,?)";
						var myinsert = [res[1], cust.photographer.photographer, cust.photographer.photoStartDate,
							cust.photographer.photoEndDate, cust.photographer.photoStartTime, cust.photographer.photoEndTime];
						sql = mysql.format(sql, myinsert);
						connection.query(sql, function(err){
							if(err){
								reject(err);
							}else{
								resolve(res[1]);
							}
						});
					}else{
						resolve(res[1]);
					}
				});	
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(!(cust.stylist.stylist=="")){
						checkForConflictsStylist(connection, cust.stylist.stylistStartDate, 
							cust.stylist.stylistEndDate, cust.stylist.stylist, function(check){
								if(check){
									reject("stylist");
								}else{
									resolve(["has stylist", res]);
								}
						});
					}else{
						resolve(["no stylist", res]);
					}
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					if(res[0]=="has stylist"){
						sql = "INSERT INTO tbltrans_stylist(locationID, stylistID, startDate, "+
						"endDate, startTime, endTime) VALUES(?,?,?,?,?,?)";
						var myinsert = [res[1], cust.stylist.stylist, cust.stylist.stylistStartDate,
							cust.stylist.stylistEndDate, cust.stylist.stylistStartTime, cust.stylist.stylistEndTime];
						sql = mysql.format(sql, myinsert);
						connection.query(sql, function(err){
							if(err){
								reject(err);
							}else{
								resolve(res[1]);
							}
						});
					}else{
						resolve(res[1]);
					}
				});
			}).then((res)=>{
				return new Promise((resolve,reject)=>{
					if(!(cust.theme=="")){
						sql = "INSERT INTO tbltrans_theme(locationID, themeID) VALUES(?,?)";
						var myinsert = [res, cust.theme];
						sql = mysql.format(sql, myinsert);
						connection.query(sql, function(err){
							if(err){
								reject(err);
							}else{
								resolve(res);
							}
						});
					}else{
						resolve(res)
					}
				});
			}).then((res)=>{
				connection.commit(function(err){
					if(err){
						connection.rollback(function(err){
							throw err;
						});
					}else{
						alert("Successfully saved!");
					}
				});
			}).catch((err)=>{
				console.log(err);
				if(err=="photographer"){
					alert("Conflict of dates on photographer!");
				}else if(err == "stylist"){
					alert("Conflict of dates on stylist!");
				}else if(err== "conflictlocation"){
					alert("Conflict of event on same date and same place!");
				}else if(err =="max3days"){
					alert("There should only be max of 3 events on a day!");
				}else{
					alert("Something went wrong. please try again later!");
				}
				connection.rollback(function(err){
					throw err;
				});
			});
		}
	});
}
function getRentDate(date){
	var now = new Date(date);
	var mydate = new Date(now.setDate(now.getDate() - 1));
	var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
	return thisdate;
}
function getReturnDate(date){
	var now = new Date(date);
	var mydate = new Date(now.setDate(now.getDate() + 1));
	var thisdate = mydate.getFullYear()+"-"+(mydate.getMonth()+1)+"-"+mydate.getDate();
	return thisdate;
}
function changeLocationCustomer(connection, customer){
	var sql = "SELECT l.locationname, l.locationID FROM tbllocation "+
	"l INNER JOIN customer_location cl ON l.locationID = cl.locationID "+
	"INNER JOIN tblcustomer c ON c.custID = cl.customerID WHERE "+
	"cl.customerLocationID = (SELECT MAX(cl2.customerLocationID) "+
	"FROM customer_location cl2 WHERE cl2.customerID = c.custID) AND c.custID = ?";
	var insert = [customer];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			var html = "<option value = '"+rows[0].locationID+"'>"+rows[0].locationname+"</option>";
			sql = "SELECT locationID, locationname FROM tbllocation WHERE locationID <> '"+rows[0].locationID+"'";
			connection.query(sql, function(err, rows1, fields){
				if(err){
					console.log(err);
					alert("Something went wrong while loading the locations. Please try again later!");
				}else{
					rows1.forEach(function(row){
						html += "<option value = '"+row.locationID+"'>"+row.locationname+"</option>";
					});
					$("#location").html(html);
					$("#location").material_select();
				}
				//connection.end();
			});
		}
	});
}
function initPayment(){
	var payment = 0;
	
	$('#addItemsHere > tr').each(function(){
		i = 0;
		product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		
		payment = parseFloat(payment) + parseFloat(product[2]);
		//console.log(payment);
	});
	payment = parseFloat(payment) + parseFloat($("#valuePriceTailor").val());
	var cake = $("#cake").val();
	//console.log(cake);
	var firstc;
	var secc;
	var mycake;
	cake.forEach(function(row){
		mycake= $("#cake option[value='"+row+"']").text();
		mycake = mycake.split(" ")[1];

		firstc = mycake.replace("(", "");
		secc = firstc.replace(")", "");
		payment = parseFloat(payment) + parseFloat(secc);
	});
	if($("#photographer").val()!=""){
		//console.log($("#photographer").val());
		var tempphot = $("#photographer option[value='"+$("#photographer").val()+"']").text().split(" ")[2].replace("(", "");
		tempphot = tempphot.replace(")", "");
		payment = parseFloat(payment) + parseFloat(tempphot);
	}
	if($("#stylist").val()!=""){
		var tempstylist = $("#stylist option[value='"+$("#stylist").val()+"']").text().split(" ")[2].replace("(", "");
		tempstylist = tempstylist.replace(")", "");
		payment = parseFloat(payment) + parseFloat(tempstylist);
	}
	$("#totalPayment").val(payment);
	$("#totalPaymentLabel").addClass('active');
}
function checkForConflictsPhotographer(connection, startdate, enddate, photoid, callback){
	var sql = "SELECT * FROM tbltrans_photography WHERE ((? >= date_start AND"+
	" ? <= date_end) OR (? >= date_start AND ? <= date_end) "+
	"OR (date_start >= ? AND date_end <= ?)) AND photoID = ?";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate, photoid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			throw err;
		}else{
			if(rows.length>0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function checkForConflictsStylist(connection, startdate, enddate, stylistid, callback){
	var sql = "SELECT * FROM tbltrans_stylist WHERE ((? >= startDate AND"+
	" ? <= endDate) OR (? >= startDate AND ? <= endDate) "+
	"OR (startDate >= ? AND endDate <= ?)) AND stylistID = ?";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate, stylistid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			throw err;
			callback(false);
		}else{
			if(rows.length>0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function promiseExperiment(){
	new Promise((resolve, reject)=>{
		
		console.log("Hrllo");
	}).then((res)=>{
		console.log("hi");
	}).then((res)=> {
		return new Promise((resolve, reject)=>{
			resolve(6);
		});
	}).then((res)=> {
		console.log(5);
	}).catch((res)=>{
		console.log("reject");
	});
		
}
function loadPackages(connection){
	var sql = "SELECT packageName, packageID FROM package";
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			var html = "";
			rows.forEach(function(row){
				html +="<option value = '"+row.packageID+"'>"+row.packageName+"</option>";
			});
			$("#rentpackage").html(html);
			$('#rentpackage').material_select();
		}
	});
}
function getPackagePrice(connection, packageID){
	var sql= "SELECT price FROM package WHERE packageID = ?";
	var insert = [packageID];
	sql =  mysql.format(sql, insert);
	connection.query(sql, function(err, row){
		if(err){
			console.log(err);
		}else{
			$("#packagePrice").val(row[0].price);
			$(".userLabel").addClass('active');
		}
	});
	sql = "SELECT i.itemcode, h.itemQuantity, i.itemcode2 FROM packagehistory "+
	"h INNER JOIN package p ON p.packageID = h.packageID INNER JOIN "+
	"tblinventory i ON i.itemcode = h.inventoryID WHERE p.packageID = ?";
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			new Promise((resolve, reject)=>{
				getItemsForValidation(connection, function(rows){
					//resolve(rows);
					var thiscounter = 0;
					rows.forEach(function(row, index, array){
						getReservedItem(connection, row.itemcode, function(qty){
							rows[index].quantity = returnAbs(rows[index].quantity, qty, rows[index].damagedItems);
							thiscounter++;
							if(thiscounter==array.length){
								resolve(rows);
							}
						});
					});
				});
			}).then((res)=>{
				return new Promise((resolve, reject)=>{
					//console.log(rows);
					var mycounter = 0;
					res.forEach(function(row, index, array){
						rows.forEach(function(item){
							if(row.itemcode==item.itemcode){
								console.log(item.itemQuantity+", "+row.quantity);
								if(item.itemQuantity > row.quantity){
									//console.log(item.itemcode+", "+row.itemcode);
									reject(true);
								}else{
									var html = "<td>"+row.itemcode2+"</td>";
									html += "<td>"+(row.quantity= parseFloat(row.quantity)-parseFloat(item.itemQuantity))+"</td>";
									html += "<td>"+row.price+"</td>";
									html += "<td><a class='waves-effect waves-light btn additem' id = 'additem-"+row.itemcode+"'><i class='material-icons'>add</i></a></td>";
									$("#item-"+row.itemcode).html(html);
								}
							}
						});
						mycounter++;
						if(mycounter==array.length){
							resolve(true);
						}
					});	
				});	
			}).then((res)=>{
				var html = "";
				$("#addItemsHere").html("");
				rows.forEach(function(row){
					getItemPrice(connection, row.itemcode, function(price){
						html = "";
						html += "<tr id = 'addeditem-"+row.itemcode+"'><td>"+row.itemcode2+"</td>"+
						"<td>"+row.itemQuantity+"</td>"+
						"<td>"+(price * row.itemQuantity)+"</td>"+
						"<td><a class='waves-effect waves-light btn removeitem' id = 'removeitem-"+row.itemcode+"'><i class='material-icons'>exposure_neg_1</i></a></td>"
						+"</tr>";
						$("#addItemsHere").prepend(html);
					});
				});
			}).catch((err)=>{
				console.log(err);
				alert("This package have one item that has greater quantity than the actual item on the inventory!");
			});			
		}
	});
}
function getItemPrice(connection, itemcode, callback){
	var sql = "SELECT price FROM tblinventory WHERE itemcode = ?";
	var insert = [itemcode];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
		}else{
			callback(rows[0].price);
		}
	});
}
function checkifmaxtransactsreached(connection, startdate, enddate, callback){
	var sql = "SELECT * FROM tbltrans_location WHERE (? >= "+
	"event_date_start AND ? <= event_date_end) OR "+
	"(? >= event_date_start AND ? <= event_date_end) "+
	"OR (event_date_start >= ? AND event_date_start <= ?)";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(true);
		}else{
			if(rows.length >= 3){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function checkifmaxtransactsreachedononelocation(connection, startdate, enddate, locationid, callback){
	var sql = "SELECT * FROM tbltrans_location WHERE ((? >= "+
	"event_date_start AND ? <= event_date_end) OR "+
	"(? >= event_date_start AND ? <= event_date_end) "+
	"OR (event_date_start >= ? AND event_date_start <= ?)) AND locationID = ?";
	var insert = [startdate, startdate, enddate, enddate, startdate, enddate, locationid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback(true);
		}else{
			if(rows.length > 0){
				callback(true);
			}else{
				callback(false);
			}
		}
	});
}
function getItemsForValidation(connection, callback){
	var sql = "SELECT itemcode, itemcode2, quantity, price, damagedItems FROM tblinventory";
	connection.query(sql, function(err, rows){
		if(err){
			console.log(err);
			callback([]);
		}else{
			callback(rows);
		}
	});
}
function getReservedItem(connection, itemid, callback){
	var sql = "SELECT SUM(r.qty) AS mysum FROM tbltrans_rent r INNER JOIN tbltrans_location l  ON l.trans_locationid ="+
	" r.locationID WHERE r.itemcode = ? AND l.depositReturned <> 'false'";
	var insert = [itemid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err, rows, fields){
		if(err){
			callback(0);
		}else{
			//console.log(sql);
			if(rows[0].mysum == undefined){
				callback(0);
			}else{
				//console.log(rows.mysum);
				callback(rows[0].mysum);
			}
			
		}
	});
}
function returnAbs(firstvalue, secvalue, thirdvalue){
	var myval = (firstvalue-secvalue)-thirdvalue;
	if(myval<0){
		return 0;
	}else{
		return myval;
	}
}
function getGender(){
	if($("#maleRadioButton").prop("checked")){
		return "Male";
	}else if($("#femaleRadioButton").prop("checked")){
		return "Female";
	}else{
		return "";
	}
}
function insertItemToBeReturned(connection, item){
	var sql = "INSERT INTO temporaryitemtable(gender, type, "+
	"category, color, quantity, price, dateToBeReturned, CustomerID)"+
	" VALUES(?,?,?,?,?,?,?,?)";
	var insert = [item.gender, item.type, item.category, item.color, item.quantity, item.price,
	item.dateToBeReturned, item.customerid];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err){
		if(err){
			console.log(err);
			alert("Something went wrong.");
		}else{
			alert("Saved!");
			$("#"+item.id).prop("disabled", true);	
		}
	});
}
function getTailorPrice(){
	var payment=0;
	$('#putTailorMaleHere > tr').each(function(){
		i = 0;
		product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		payment = parseFloat(payment) + parseFloat(product[15]);
	});
	var measurementFemale = [];
	$('#putTailorFemaleHere > tr').each(function(){
		i = 0;
		product = [""];
		$(this).closest('tr').each(function(){
			$(this).find('td').each(function(){
				product[i]=$(this).html();
				i++;
			});
		});
		payment = parseFloat(payment) + parseFloat(product[12]);
	});
	$("#valuePriceTailor").val(payment)
}