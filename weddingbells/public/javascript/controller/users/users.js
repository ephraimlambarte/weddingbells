var totalpages;
var visiblepages;
var pages;
var usertype;
var rowid;
var myuserstatus;
var action;
var myusertype;
$(document).ready(function(){
  w3.includeHTML(function(){
    $("div .dropdown-button").dropdown();
		$(".button-collapse").sideNav();
    $('.mymodal select').material_select();
    $('.modal').modal();
  });
  //alert(sessionStorage.getItem('id'));

  tableLoader("#loadUsersHere", "10");
  connectMysql2(function(connection){
    getMaxRows(function(rows){
        if(rows<=5){
          visiblepages=1;
        }else {
          visiblepages=5;
        }
        if(rows%5!=0){
          totalpages=Math.floor((rows/5))+1;
        }else{
          totalpages=rows/5;
        }
        $('#pagination').twbsPagination({
               totalPages: totalpages,
               visiblePages: visiblepages,
               onPageClick: function (event, page){
                 pages = (5*(page-1));
                   loadTable(pages, connection);
               }
        });
    },"tbluser", "WHERE status <> 'Inactive'");
  });
  $("#loadUsersHere").on('click', '.change',function(){
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    rowid = $(this).prop("id").split("-")[1];
    action="save";
    $("#editUserTitle").text("Edit "+product[0]+" "+product[2]);
    $("#userModal").modal("open");
    $("#userFirstName").val(product[0]);
    $("#userMiddleName").val(product[1]);
    $("#userLastName").val(product[2]);
    $("#userContact").val(product[3]);
    $("#userUserName").val(product[4]);
    $("#userPassword").val(product[5]);
    $("#userType").val(product[6]);
    myusertype = product[6];
    $("#userStatus").val(product[7]);
    myuserstatus = product[7];
    $(".userLabel").addClass("active")
  });
  $("#loadUsersHere").on('click', '.delete',function(){
    var i = 0;
    var product = [""];
    $(this).closest('tr').each(function(){
      $(this).find('td').each(function(){
        product[i]=$(this).html();
        i++;
      });
    });
    $("#deleteTitle").text("Are you sure you want to delete "+product[0]+" "+product[2]+"?");
    $("#deleteModal").modal("open");
    rowid = $(this).prop("id").split("-")[1];
  });
  $("#activateDropdown").on('click', function(){
    $('.dropdown-button').dropdown('open');
  });
  $(".mymodal").on('click', '#saveUserButton',function(e){
    e.preventDefault();
    if(action=="save"){

      var firstname = $("#userFirstName").val();
      var middlename = $("#userMiddleName").val();
      var lastname = $("#userLastName").val();
      var contact = $("#userContact").val();
      var username = $("#userUserName").val();
      var password = $("#userPassword").val();
      var usertype = $("#userType").val();
      var userstatus = $("#userStatus").val();
      if (rowid==sessionStorage.getItem('id')){
        if(myuserstatus != userstatus){
          alert("You cant set yourself to inactive!");
          return;
        }
        if(myusertype!=usertype){
          alert("You cant change your own user type!");
          return;
        }
      }
      if(firstname == "" || lastname == "" || contact == "" || username == "" || password == "" ){
        alert("Please provide all the necessary details!");
        return;
      }
      var user = {firstname:firstname,
                middlename:middlename,
                lastname:lastname,
                contact:contact,
                username:username,
                password:password,
                usertype:usertype,
                userstatus:userstatus};
      connectMysql2(function(connection){
        updateUserTable(user, connection, rowid);
      });
    }else{

      var firstname = $("#userFirstName").val();
      var middlename = $("#userMiddleName").val();
      var lastname = $("#userLastName").val();
      var contact = $("#userContact").val();
      var username = $("#userUserName").val();
      var password = $("#userPassword").val();
      var usertype = $("#userType").val();
      var userstatus = $("#userStatus").val();
      if(firstname == "" || lastname == "" || contact == "" || username == "" || password == "" ){
        alert("Please provide all the necessary details!");
        return;
      }
      var user = {firstname:firstname,
                middlename:middlename,
                lastname:lastname,
                contact:contact,
                username:username,
                password:password,
                usertype:usertype,
                userstatus:userstatus};
      connectMysql2(function(connection){
        insertUserTable(user, connection);
      });
    }
  });
  $("#addUserButton").on('click', function(){
      $("#editUserTitle").text("Add User");
    action="add";
    $("#userModal").modal("open");
    $("#userFirstName").val("");
    $("#userMiddleName").val("");
    $("#userLastName").val("");
    $("#userContact").val("");
    $("#userUserName").val("");
    $("#userPassword").val("");
    $(".userLabel").removeClass("active");
  });
  $(".mymodal").on('click', '#deleteConfirmButton', function(){
    connectMysql2(function(connection){
      deleteRow(rowid, connection);
    });
  });
  $("#searchUser").on('keyup', function(){
    connectMysql2(function(connection){
      searchUsers(connection, $("#searchUser").val());
    });
  });
});
function loadTable(page, connection){
  var sql = "SELECT * FROM tbluser WHERE status <> 'Inactive' LIMIT "+page+", 5";

  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err)
    }else{
      var html;
      rows.forEach(function(row){
        html+="<tr id = 'user-"+row.userID+"'>";
        html+="<td>"+row.firstname+"</td>";
        html+="<td>"+row.middlename+"</td>";
        html+="<td>"+row.lastname+"</td>";
        html+="<td>"+row.contact+"</td>";
        html+="<td>"+row.username+"</td>";
        html+="<td>"+row.password+"</td>";
        html+="<td>"+row.type+"</td>";
        html+="<td>"+row.status+"</td>";
        html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.userID+"'>Edit</a></td>";
        html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.userID+"'>Delete</a></td>";
        html+="<tr>";
      });
      $("#loadUsersHere").html(html);
    }
    //connection.end();
  });
}
function updateUserTable(user, connection, id){
  var sql = "UPDATE tbluser SET lastname = ?, firstname = ?, middlename= ?, username= ?, password = ?, contact = ?, type = ?, status = ? WHERE userID = '"+id+"'";
  var insert = [user.lastname, user.firstname, user.middlename, user.username, user.password, user.contact, user.usertype, user.userstatus];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later!");
      return;
    }else{
      alert("Successfully saved!");
      $("#user-"+id).html("<td>"+user.firstname+"</td><td>"+user.middlename
                          +"</td><td>"+user.lastname+"</td><td>"+user.contact
                          +"</td><td>"+user.username+"</td><td>"+user.password
                          +"</td><td>"+user.usertype+"</td><td>"+user.userstatus
                          +"</td><td><a class='waves-effect waves-light btn change' id='edit-"
                          +id+"'>Edit</a></td><td><a class='waves-effect waves-light btn delete' id = 'delete-"
                          +id+"'>Delete</a></td>");

    }
    connection.end();
  });
}
function insertUserTable(user, connection){
  var sql = "INSERT INTO tbluser(lastname, firstname, "+
            "middlename, username, password, contact, type, status) VALUES"+
            "(?,?,?,?,?,?,?,?)";
  var insert = [user.lastname, user.firstname, user.middlename, user.username, user.password, user.contact, user.usertype, user.userstatus];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, result){
    if(err){
      console.log(err);
      alert("Something went wrong. Please try again later!");
      return;
    }

    $("#loadUsersHere").prepend("<tr id='user-"+result.insertId+"'><td>"+user.firstname+"</td><td>"+user.middlename
                        +"</td><td>"+user.lastname+"</td><td>"+user.contact
                        +"</td><td>"+user.username+"</td><td>"+user.password
                        +"</td><td>"+user.usertype+"</td><td>"+user.userstatus
                        +"</td><td><a class='waves-effect waves-light btn change' id='edit-"
                        +result.insertId+"'>Edit</a></td><td><a class='waves-effect waves-light btn delete' id = 'delete-"
                        +result.insertId+"'>Delete</a></td></tr>");
    alert("Successfully saved!");
    connection.end();
  });
}
function deleteRow(rowid, connection){
  var sql = "DELETE FROM tbluser WHERE userID = '"+rowid+"'";
  connection.query(sql, function(err){
    if(err){
      alert("This user cannot be deleted because it has existing transactions!");
    }else{
      $("#user-"+rowid).remove();
      alert("Successfully deleted!");
    }
  });
}
function searchUsers(connection, search){
  var sql = "SELECT * FROM tbluser WHERE (lastname LIKE ? OR firstname LIKE ? "+
  "OR middlename LIKE ? OR username LIKE ? OR status LIKE ?) AND (status <> 'Inactive')";
  var insert = ['%'+search+'%','%'+search+'%','%'+search+'%','%'+search+'%','%'+search+'%'];
  sql = mysql.format(sql, insert);
  connection.query(sql, function(err, rows, fields){
    if(err){
      console.log(err)
    }else{
      var html="";
      rows.forEach(function(row){
        html+="<tr id = 'user-"+row.userID+"'>";
        html+="<td>"+row.firstname+"</td>";
        html+="<td>"+row.middlename+"</td>";
        html+="<td>"+row.lastname+"</td>";
        html+="<td>"+row.contact+"</td>";
        html+="<td>"+row.username+"</td>";
        html+="<td>"+row.password+"</td>";
        html+="<td>"+row.type+"</td>";
        html+="<td>"+row.status+"</td>";
        html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.userID+"'>Edit</a></td>";
        html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.userID+"'>Delete</a></td>";
        html+="<tr>";
      });
      $("#loadUsersHere").html(html);
    }
    //connection.end();
  });

}
