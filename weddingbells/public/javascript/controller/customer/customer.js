var totalpages;
var visiblepages;
var mypage;
var action = "";
var rowid;
$(document).ready(function(){
		w3.includeHTML(function(){
			$("div .dropdown-button").dropdown();
			$(".button-collapse").sideNav();
			$('select').material_select();
			$('.modal').modal();

		});
	 tableLoader("#loadCustomerHere", "8");
	 $("#addCustomerButton").on('click', function(){
		 $("#editCustomerTitle").text("Add Customer");
		 $("#customerModal").modal("open");
		 $(".userLabel").removeClass("active");
		 $("#custFirstName").val("");
		 $("#custMiddleName").val("");
		 $("#custLastName").val("");
		 $("#custAddress").val("");
		 $("#custContact").val("");

		 action = "add";
	 });
	 connectMysql2(function(connection){
		 getUserType(connection, sessionStorage.getItem('id'), function(usertype){
			 if(usertype != 'Admin'){
				 $("#userPage").remove();
			 }
		 });
		 getLocation(connection);
		 getMaxRows(function(rows){
			 if(rows<=5){
				 visiblepages=1;
			 }else {
				 visiblepages=5;
			 }
			 if(rows%5!=0){
				 totalpages=Math.floor((rows/5))+1;
			 }else{
				 totalpages=rows/5;
			 }
			 if(rows==0){
				 $("#loadCustomerHere").html("");
				 return;
			 }
			 $('#customerPagination').twbsPagination({
							 totalPages: totalpages,
							 visiblePages: visiblepages,
							 onPageClick: function (event, page){
								 mypage = (5*(page-1));
									 loadCustomersTable(mypage, connection);
							 }
				});
		 }, "tblcustomer", "");
	 });
	 $(".mymodal").on('click', '#custRadioMale', function(){
		 $("#custGenderMale").prop("checked", true);
	 });
	 $(".mymodal").on('click', '#custRadioFemale', function(){
		 $("#custGenderFemale").prop("checked", true);
	 });
	 $(".mymodal").on('click', '#saveCustomerButton',function(){
		 var custfirstname =  $("#custFirstName").val();
		 var custlastname = $("#custLastName").val();
		 var custmiddlename = $("#custMiddleName").val();
		 var custaddress =$("#custAddress").val();
		 var custcontact =$("#custContact").val();
		 var gender = getGender();
		 //alert(gender)
		 if(custfirstname == ""|| custlastname=="" || custaddress == "" || custcontact == "" || gender == ""){
			 alert("Please provide all the necessary details!");
			 return;
		 }
		 var customer = {
			 firstname:custfirstname,
			 lastname:custlastname,
			 middlename:custmiddlename,
			 address:custaddress,
			 contact:custcontact,
			 gender:gender
		 }
		 if(action=="add"){
				connectMysql2(function(connection){
					getLocationId(connection, custaddress, function(locationid){
						insertCustomer(connection, customer, locationid);
					});
				});
		 }else{
			 connectMysql2(function(connection){
				 updateCustomer(connection, customer, rowid);
			 });
		 }
	 });
	 $("#loadCustomerHere").on('click', '.change', function(){
		 rowid = $(this).prop("id").split("-")[1];
		 var i = 0;
     var product = [""];
     $(this).closest('tr').each(function(){
       $(this).find('td').each(function(){
         product[i]=$(this).html();
         i++;
       });
     });
		 action = "edit"
		 $("#custFirstName").val(product[0]);
		 $("#custMiddleName").val(product[1]);
		 $("#custLastName").val(product[2]);
		 $("#custAddress").val(product[5]);
		 $("#custContact").val(product[4]);
		 setGender(product[3]);
		 $("#editCustomerTitle").text("Edit Customer "+product[0]+" "+product[2]);
		 $("#customerModal").modal("open");
		 $(".userLabel").addClass("active");
	 });
	 $("#loadCustomerHere").on('click', '.delete', function(){

		 rowid = $(this).prop("id").split("-")[1];
		 var i = 0;
		 var product = [""];
		 $(this).closest('tr').each(function(){
			 $(this).find('td').each(function(){
				 product[i]=$(this).html();
				 i++;
			 });
		 });
		 $("#deleteTitle").text("Are you sure you want to delete "+product[0]+" "+product[1]);
		 $("#deleteModal").modal("open");
	 });
	 $(".mymodal").on('click', "#deleteConfirmButton", function(){
		 connectMysql2(function(connection){
			 deleteCustomer(connection, rowid);
		 });
	 });
	 $("#searchCustomer").on('keyup', function(){
		 connectMysql2(function(connection){
			 searchCustomers(connection, $("#searchCustomer").val());
		 });
	 });
});
function loadCustomersTable(page, connection){
	var sql = "SELECT c.*, l.locationname "+
							"FROM tblcustomer c "+
							"INNER JOIN customer_location cl "+
    					"ON c.custID = cl.customerID "+
        			"AND cl.customerLocationID = ( "+
            	"SELECT MAX(cl2.customerLocationID) "+
            	"FROM customer_location cl2 "+
            	"WHERE cl2.customerID = c.custID "+
        ") INNER JOIN tbllocation l ON l.locationID = cl.locationID LIMIT "+page+", 5";

	connection.query(sql,function(err, rows, fields){
		if(err){
			console.log(err);
		}else{
			var html;
			rows.forEach(function(row){
				html+="<tr id = 'cust-"+row.custID+"'>";
					html+="<td>"+row.firstname+"</td>";
					html+="<td>"+row.middlename+"</td>";
					html+="<td>"+row.lastname+"</td>";
					html+="<td>"+row.gender+"</td>";
					html+="<td>"+row.contact+"</td>";
					html+="<td>"+row.locationname+"</td>";
					html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.custID+"'>Edit</a></td>";
					html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.custID+"'>Delete</a></td>";
				html+="</tr>";
			});
			//console.log(html);
			$("#loadCustomerHere").html(html);
		}
		//connection.end();
	});
}
function searchCustomers(connection, search){
	var sql = "SELECT custID, lastname, firstname, middlename, gender, contact, address FROM tblcustomer"+
	" WHERE lastname LIKE ? OR firstname LIKE ? OR middlename LIKE ? OR gender LIKE ? OR contact LIKE ? "+
	"OR address LIKE ?";
	var insert  = ['%'+search+'%', '%'+search+'%', '%'+search+'%', '%'+search+'%', '%'+search+'%', '%'+search+'%'];
	sql = mysql.format(sql, insert);
	connection.query(sql,function(err, rows, fields){
		if(err){
			console.log(err);
		}else{
			var html;
			rows.forEach(function(row){
				html+="<tr id = 'cust-"+row.custID+"'>";
					html+="<td>"+row.firstname+"</td>";
					html+="<td>"+row.middlename+"</td>";
					html+="<td>"+row.lastname+"</td>";
					html+="<td>"+row.gender+"</td>";
					html+="<td>"+row.contact+"</td>";
					html+="<td>"+row.address+"</td>";
					html+="<td><a class='waves-effect waves-light btn change' id='edit-"+row.custID+"'>Edit</a></td>";
					html+="<td><a class='waves-effect waves-light btn delete' id = 'delete-"+row.custID+"'>Delete</a></td>";
				html+="</tr>";
			});
			//console.log(html);
			$("#loadCustomerHere").html(html);
		}
		//connection.end();
	});
}
function getGender(){
	if($("#custGenderMale").prop("checked")){
		return "Male";
	}else{
		return "Female";
	}
}
function insertCustomer(connection, customer, locationid){
	if(locationid == 0){
		alert("Something went wrong. Please try again later!");
	}else{
		var sql = "INSERT INTO tblcustomer(lastname, firstname, middlename, gender, contact, address) VALUES(?,?,?,?,?,?)";
		var insert = [customer.lastname, customer.firstname, customer.middlename, customer.gender, customer.contact, customer.address];
		sql= mysql.format(sql, insert);
		connection.query(sql, function(err, result){
			if(err){
				console.log(err);
				alert("Something went wrong. please try again later!");
				connection.rollback(function(){
					throw err;
					connection.commit(function(err){
						if(err){
							connection.rollback(function(){
								throw err;
								connection.end();
							});
						}else{
							connection.end();
						}
					});
				});
			}else{
				sql = "INSERT INTO customer_location(customerID, locationID, dateAssigned) VALUES(?, ?, CURDATE())";
				var myinsert = [result.insertId, locationid];
				sql = mysql.format(sql, myinsert);
				//console.log(sql);
				connection.query(sql, function(err, result){
					if(err){
						console.log(err);
						connection.rollback(function(){
							throw err;
							connection.commit(function(err){
								connection.rollback(function(){
									throw err;
								});
							});
						});
						alert("Something went wrong. Please try again later!");
					}else{
						var html = "<tr id = 'cust-"+result.insertId+"'>";
									html += "<td>"+customer.firstname+"</td>";
									html += "<td>"+customer.middlename+"</td>";
									html += "<td>"+customer.lastname+"</td>";
									html += "<td>"+customer.gender+"</td>";
									html += "<td>"+customer.contact+"</td>";
									html += "<td>"+customer.address+"</td>";
									html += "<td><a class='waves-effect waves-light btn change' id='edit-"+result.insertId+"'>Edit</a></td>";
									html += "<td><a class='waves-effect waves-light btn delete' id='delete-"+result.insertId+"'>Delete</a></td>";
								html += "</tr>";
						$("#loadCustomerHere").prepend(html);
						connection.commit(function(err){
							if(err){
								console.log(err);
								alert("Something went wrong. Please try again later!");
								connection.rollback(function(){
									throw err;
									connection.end();
								});
							}else{
								connection.end();
								alert("Successfully saved!");
							}
						});
					}
				});
			}
		});
	}
}
function getLocationId(connection, locationname, callback){
	var sql = "SELECT locationID FROM tbllocation WHERE locationname = ?";
	var insert = [locationname];
	sql = mysql.format(sql, insert);
	connection.beginTransaction(function(err){
		if(err){
			console.log(err);
			alert("Something went wrong. Please try again later!");
		}else{
			connection.query(sql, function(err, rows){
				if(err){
					console.log(err);
				}else{
					if(rows.length == 0){
						var sql = "INSERT INTO tbllocation (locationname) VALUES(?)";
						sql = mysql.format(sql, insert);
						connection.query(sql, function(err, result){
							if(err){
								console.log(err);
								connection.rollback(function(){
									throw err;
								});
								callback(0);
							}else{
								callback(result.insertId);
							}
						});
					}else{
						callback(rows[0].locationID);
					}
				}
			});
		}
	});

}
function updateCustomer(connection, customer, rowid){
	var sql = "UPDATE tblcustomer SET lastname= ?, firstname= ?, middlename=?, gender=?, contact=?, address=? WHERE custID = '"+rowid+"'";
	var insert = [customer.lastname, customer.firstname, customer.middlename, customer.gender, customer.contact, customer.address];
	sql = mysql.format(sql, insert);
	connection.query(sql, function(err){
		if(err){
			console.log(err);
			alert("Something went wrong, please try again later!");
		}else{
			var html;
			html += "<td>"+customer.firstname+"</td>";
			html += "<td>"+customer.middlename+"</td>";
			html += "<td>"+customer.lastname+"</td>";
			html += "<td>"+customer.gender+"</td>";
			html += "<td>"+customer.contact+"</td>";
			html += "<td>"+customer.address+"</td>";
			html += "<td><a class='waves-effect waves-light btn change' id='edit-"+rowid+"'>Edit</a></td>";
			html += "<td><a class='waves-effect waves-light btn change' id='delete-"+rowid+"'>Delete</a></td>";
			$("#cust-"+rowid).html(html);
			alert("Successfully saved!");
		}
		connection.end();
	});
}
function setGender(gender){
	if(gender=="Male"){
		$("#custGenderMale").prop("checked", true);
	}else{
		$("#custGenderFemale").prop("checked", true);
	}
}
function deleteCustomer(connection, rowid){
	var sql  = "DELETE FROM tblcustomer WHERE custID = '"+rowid+"'";
	connection.query(sql, function(err){
		if(err){
			console.log(connection);
			alert("This customer have existing transactions and cannot be deleted!");
		}else{
			alert("Successfully deleted!");
			$("#cust-"+rowid).remove()
		}
		connection.end();
	});
}
function getLocation(connection){
	var data1=[];
	console.log(connection);
		var sql = "SELECT locationID, locationname FROM tbllocation";
		connection.query(sql, function(err, rows){
			if(err){
				console.log(err);

			}else{
				rows.forEach(function(row){
					data1[row.locationname] = '';
				});

				$(".autocomplete").autocomplete({
					data:data1
				});
			}
		});

}
